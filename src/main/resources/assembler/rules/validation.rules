#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier:  GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

@prefix : <https://www.datatourisme.gouv.fr/ontology/core#>.
@prefix schema: <http://schema.org/>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
@prefix dc: <http://purl.org/dc/elements/1.1/>.

[POISpecificClass: (?C rdfs:subClassOf :PointOfInterest), notEqual(?C, :PointOfInterest), (?P rdf:type ?C)  -> (?P rdf:type all(rdf:type, :PointOfInterest)) ]

#------------------------------------------------------------------
# Tous les POI doivent avoir un type spécifique, un identifiant, un label, une localisation, un créateur, une date de dernière mise à jour
#------------------------------------------------------------------
[POI: (?P rb:validation on()), (?P rdf:type :PointOfInterest) ->
    [specificClass: (?P rb:violation error('missing specific class', 'Le POI n\'a pas de type spécifique', rdf:type)) <- noValue(?P rdf:type all(rdf:type, :PointOfInterest)) ]
    [requiredId: (?P rb:violation error('missing required property', 'Le POI n\'a pas d\'identifiant', dc:identifier)) <- noValue(?P dc:identifier) ]
    [requiredLabel: (?P rb:violation error('missing required property', 'Le POI n\'a pas de libellé', rdfs:label)) <- noValue(?P rdfs:label) ]
    [requiredLocatedAt: (?P rb:violation error('missing required property', 'Le POI n\'a pas de lieu associé', :isLocatedAt)) <- noValue(?P :isLocatedAt) ]
    [requiredHasBeenCreatedBy: (?P rb:violation error('missing required property', 'Le POI n\'a pas de créateur associé', :hasBeenCreatedBy)) <- noValue(?P :hasBeenCreatedBy) ]
    [requiredLastUpdate: (?P rb:violation error('missing required property', 'Le POI n\'a pas de date de dernière mise à jour', :lastUpdate)) <- noValue(?P :lastUpdate) ]
]

#------------------------------------------------------------------
# Tous les POI doivent avoir des latitude/longitude
# Tous les POI doivent avoir une commune et un code postal
#------------------------------------------------------------------
[LocatedAt: (?P rb:validation on()), (?P rdf:type :PointOfInterest), (?P :isLocatedAt ?L) ->
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas de coordonnées géographiques associées', schema:geo)) <- noValue(?L schema:geo) ]
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas d\'adresse postale associée', schema:address)) <- noValue(?L schema:address) ]
]
[GeoLoc: (?P rb:validation on()), (?P rdf:type :PointOfInterest), (?P :isLocatedAt ?L), (?L schema:geo ?G) ->
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas de latitude associée', schema:latitude)) <- noValue(?G schema:latitude) ]
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas de longitude associée', schema:longitude)) <- noValue(?G schema:longitude) ]
]
[Address: (?P rb:validation on()), (?P rdf:type :PointOfInterest), (?P :isLocatedAt ?L), (?L schema:address ?A) ->
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas de commune associée', :hasAddressCity)) <- noValue(?A :hasAddressCity) ]
    [(?P rb:violation error('missing required property', 'Le lieu du POI n\'a pas de code postal associé', schema:postalCode)) <- noValue(?A schema:postalCode) ]
]

#------------------------------------------------------------------
# Tous les événements doivent avoir une période définie avec une startDate
#------------------------------------------------------------------
[Event: (?P rb:validation on()), (?P rdf:type :EntertainmentAndEvent) ->
    [(?P rb:violation error('missing required property', 'Le POI n\'a pas de période associée', :takesPlaceAt)) <- noValue(?P :takesPlaceAt) ]
]
[Date: (?P rb:validation on()), (?P rdf:type :EntertainmentAndEvent), (?P :takesPlaceAt ?D) ->
    [(?P rb:violation error('missing required property', 'La période du POI n\'a pas de date de début associée', :startDate)) <- noValue(?D :startDate) ]
]

#------------------------------------------------------------------
# Divers validation par regex
#------------------------------------------------------------------
[email: (?x schema:email ?email), regexNeg(?email, '^([a-zA-Z0-9_\\.-]+)@([\\da-z\\.-]+)\.([a-z\\.]{2,6})$') ->
    (?x rb:violation warn('invalid email', 'L\'adresse mail "$1" n\'est pas valide', schema:email, ?email))
]

#------------------------------------------------------------------
# 12/2019 "au moins un mail ou un téléphone ou une URL dans A COMME CONTACT/Agent ou A COMME CONTACT RESERVATION/Agent"
# 01/2020 "SAUF les aire de camping car"
#------------------------------------------------------------------
[(?P rdf:type :PointOfInterest),
    makeList(?l, :EntertainmentAndEvent, :Product, :TastingProvider, :Accommodation, :MedicalPlace, :ServiceProvider, :FoodEstablishment, :TouristInformationCenter, :Museum, :InterpretationCentre, :BusinessPlace),
    (?P rdf:type ?t), listContains(?l, ?t) -> (?P rdf:type contactRequired())
]

# handle exceptions
[(?P rdf:type contactRequired()),
    makeList(?l, :CamperVanArea),
    (?P rdf:type ?t), listContains(?l, ?t) -> remove(0)
]

[(?P rdf:type contactRequired()), (?P :hasContact ?c), (?c schema:telephone ?v) -> (?P rdf:type hasContact())]
[(?P rdf:type contactRequired()), (?P :hasContact ?c), (?c schema:email ?v) -> (?P rdf:type hasContact())]
[(?P rdf:type contactRequired()), (?P :hasContact ?c), (?c foaf:homepage ?v) -> (?P rdf:type hasContact())]

[(?P rdf:type contactRequired()), (?P :hasBookingContact ?c), (?c schema:telephone ?v) -> (?P rdf:type hasContact())]
[(?P rdf:type contactRequired()), (?P :hasBookingContact ?c), (?c schema:email ?v) -> (?P rdf:type hasContact())]
[(?P rdf:type contactRequired()), (?P :hasBookingContact ?c), (?c foaf:homepage ?v) -> (?P rdf:type hasContact())]

[(?P rb:validation on()), (?P rdf:type contactRequired()) ->
    [(?P rb:violation error('missing contact info', 'Le POI n\'a pas de contact renseigné')) <- noValue(?P rdf:type hasContact())]
]