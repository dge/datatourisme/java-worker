/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle;

import fr.datatourisme.bundle.builder.ResourceBundleBuilderFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BundleConfiguration {
    @Bean @Lazy
    public ResourceStore createResourceStore(@Value( "${datatourisme.bundlestore.path}" ) String storePath) {
        return new ResourceStore(storePath);
    }
    @Bean @Lazy
    public ResourceBundleBuilderFactory createBuilderFactory(ResourceStore resourceStore) {
        return new ResourceBundleBuilderFactory(resourceStore);
    }
}
