/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.TripletIterator;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.bundle.serializer.Context;
import fr.datatourisme.bundle.serializer.ContextBuilder;
import fr.datatourisme.bundle.serializer.JsonLdSerializer;
import fr.datatourisme.bundle.serializer.XmlSerializer;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeData;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.vocabulary.Schema;
import org.apache.commons.io.FileUtils;
import org.apache.jena.util.SplitIRI;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Build the resource store
 */
public class ResourceStoreBuilder {
    private static final Logger logger = Logger.getLogger(ResourceStoreBuilder.class);

    private final GraphStore graphStore;
    private final Ontology ontology;

    /**
     * @param graphStore
     */
    public ResourceStoreBuilder(GraphStore graphStore, Ontology ontology) {
        this.graphStore = graphStore;
        this.ontology = ontology;
    }

    /**
     * Build the store
     *
     * @throws GraphStoreException
     */
    public void build(String path) throws GraphStoreException, IOException {
        File directory = new File(path);

        // prepare context
        Context context = buildContext(ontology);

        // clean output dir
        logger.info("Clean output directory...");
        clean(directory);

        // write context json
        logger.info("Build context file...");
        File contextFile = new File(directory, "context.jsonld");
        BufferedWriter writer = new BufferedWriter(new FileWriter(contextFile));
        writer.write(context.toJson());
        writer.close();

        // write poi files
        logger.info("Prepare objects file...");
        JsonLdSerializer jsonSerializer = new JsonLdSerializer(graphStore, context);
        XmlSerializer xmlSerializer = new XmlSerializer(graphStore, context);
        ResourceStore resourceStore = new ResourceStore(directory.getPath());

        TripletIterator iter = graphStore.querySPO();
        iter.stream()
            .filter(triplet -> triplet.getPredicate().getLabel().equals(RDF.type.getURI()) && triplet.getObject().getLabel().equals(Datatourisme.PointOfInterest.getURI()))
            .parallel().forEach(triplet -> {
                try {
                    String uri = triplet.getSubject().getLabel();
                    logger.info("Object : " + uri);

                    // prepare directory
                    resourceStore.getResourceDirectory(uri).mkdirs();

                    // JSON
                    File json = resourceStore.getResource(uri, ResourceStore.Format.json);
                    jsonSerializer.serialize(triplet.getSubject(), json, directory);

                    // XML
                    File xml = resourceStore.getResource(uri, ResourceStore.Format.xml);
                    xmlSerializer.serialize(triplet.getSubject(), xml, directory);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
    }

    /**
     * @return
     */
    public static Context buildContext(Ontology ontology) {
        ContextBuilder ctxBuilder = new ContextBuilder(ontology, Datatourisme.NS);
        return ctxBuilder
            // remove namespace from prefixing
            .removeNamespace(DatatourismeData.NS)
            // exclude some namespaces
            .excludeNamespace(DatatourismeMetadata.NS)
            .excludeNamespace("http://www.bigdata.com/rdf/geospatial/literals/v1")
            // exclude some properties
            .exclude(Schema.offers)
            .exclude(RDFS.subClassOf)
            .exclude(Datatourisme.latlon)
            .exclude(DC.date)
            .exclude(Datatourisme.property("isEquippedWith"))
            // owl : exclude all except sameAs property
            .exclude(uri -> SplitIRI.namespace(uri).equals(OWL.NS) && !uri.equals(OWL.sameAs.getURI()))

            .build();
    }

    /**
     * clean directory
     */
    private void clean(File directory) throws IOException {
        if (directory.exists()) {
            FileUtils.deleteDirectory(directory);
        }
        directory.mkdirs();
    }

}
