/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle;

import java.io.File;
import java.util.LinkedList;

public class ResourceStore {
    public enum Format { json, xml }

    /**
     * File path
     */
    File path;

    /**
     * @param path
     */
    public ResourceStore(String path) {
        this.path = new File(path);
    }

    /**
     * Retrieve a resource file for a given URI & format
     *
     * @param uri
     * @param format
     */
    public File getResource(String uri, Format format) {
        return getFile(getResourcePath(uri, format));
    }


    /**
     * Retrieve a resource index entry for a given URI & format
     *
     * @param uri
     * @param format
     */
    public File getResourceIndexEntry(String uri, Format format) {
        return getFile(getResourcePath(uri, format) + ".idx");
    }

    /**
     * Retrieve a resource directory
     *
     * @param uri
     * @return
     */
    public File getResourceDirectory(String uri) {
        return getFile(getResourcePath(uri, null));
    }

    /**
     * Retrieve a file
     */
    public File getFile(String path) {
        return new File(this.path, path);
    }

    /**
     * @param uri
     *
     * @return
     */
    private static String getUuid(String uri) {
        return uri.substring(uri.lastIndexOf('/') + 1);
    }

    /**
     * @param uri
     *
     * @return
     */
    private static String getProducerIdentifier(String uri) {
        String tmp = uri.substring(0, uri.lastIndexOf('/'));
        return tmp.substring(tmp.lastIndexOf('/') + 1);
    }

    /**
     * @return
     */
    public static String getResourcePath(String uri, Format format) {
        String uuid = getUuid(uri);
        String producerIdentifier = getProducerIdentifier(uri);
        LinkedList<String> list = new LinkedList<>();
        int depth = 2;
        while (depth > 0) {
            list.addFirst(uuid.substring(0,depth));
            depth--;
        }
        if (format != null) {
            list.add(producerIdentifier + "-" + uuid + "." + format);
        }
        return String.join(File.separator, list);
    }

}
