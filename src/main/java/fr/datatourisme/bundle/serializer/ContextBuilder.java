/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.serializer;

import fr.datatourisme.ontology.Ontology;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.util.SplitIRI;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * ContextBuilder
 */
public class ContextBuilder {
    Ontology ontology;
    String defaultNamespace;
    List<Function<String, Boolean>> excludeCallbacks;
    PrefixMapping prefixMapping;

    /**
     * @param defaultNamespace
     */
    public ContextBuilder(Ontology ontology, String defaultNamespace) {
        this.ontology = ontology;
        this.defaultNamespace = defaultNamespace;
        this.excludeCallbacks = new ArrayList<>();

        Map<String, String> mapping = ontology.getPrefixMapping().getNsPrefixMap().entrySet().stream()
            .filter(es -> !es.getValue().equals(defaultNamespace))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        this.prefixMapping = PrefixMapping.Factory.create().setNsPrefixes(mapping);
    }

    /**
     * @param callback
     */
    public ContextBuilder exclude(Function<String, Boolean> callback) {
        excludeCallbacks.add(callback);
        return this;
    }

    /**
     * @param uri
     */
    public ContextBuilder exclude(String uri) {
        return exclude(s -> s.equals(uri));
    }

    /**
     * @param res
     */
    public ContextBuilder exclude(Resource res) {
        return exclude(res.getURI());
    }

    /**
     * @param namespace
     */
    public ContextBuilder excludeNamespace(String namespace) {
        exclude(uri -> SplitIRI.namespace(uri).equals(namespace));
        removeNamespace(namespace);
        return this;
    }

    /**
     * remove a namespace
     *
     * @param namespace
     * @return
     */
    public ContextBuilder removeNamespace(String namespace) {
        String prefix = prefixMapping.getNsURIPrefix(namespace);
        if (prefix != null) {
            prefixMapping.removeNsPrefix(prefix);
        }
        return this;
    }

    /**
     * build
     * @return
     */
    public Context build() {
        return new Context(ontology, defaultNamespace, prefixMapping, excludeCallbacks);
    }
}
