/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.serializer;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.Triplet;
import com.conjecto.graphstore.iterator.PredicateIterator;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.sparql.util.PrefixMapping2;
import org.apache.jena.util.SplitIRI;
import org.semanticweb.yars.nx.BNode;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.Resource;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * XmlSerializer
 */
public class XmlSerializer extends Serializer {
    private final Set<String> additionalPrefixes;
    private PrefixMapping prefixMapping;

    /**
     * Constructor
     *
     * @param store
     * @param context
     */
    public XmlSerializer(GraphStore store, Context context) {
        super(store, context);
        this.additionalPrefixes = new HashSet<>();
    }

    /**
     * @param node
     * @param out
     * @throws IOException
     */
    public void serialize(Node node, OutputStream out) throws IOException {
        this.prefixMapping = new PrefixMapping2(context.getPrefixMapping());
        OutputStreamWriter writer = new OutputStreamWriter(out);
        writeHeader(writer);
        writeResource(writer, node, 1);
        writeFooter(writer);
        writer.flush();
    }

    /**
     * @param writer
     * @throws IOException
     */
    private void writeHeader(OutputStreamWriter writer) throws IOException {
        writer.write("<?xml version=\"1.0\"?>\n");
        writer.write("<rdf:RDF \n\txmlns=\"" + context.getDefaultNamespace() + "\" \n\txmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"");
        // add default namespaces
        for(Map.Entry<String, String> entry : prefixMapping.getNsPrefixMap().entrySet()) {
            if(!entry.getKey().equals("rdf")) {
                writer.write("\n\txmlns:" + entry.getKey() + "=\"" + entry.getValue() + "\"");
            }
        }
        writer.write(">\n");
    }

    /**
     * @param writer
     * @param node
     */
    private void writeResource(OutputStreamWriter writer, Node node, int i) throws IOException {
        writer.write(indent("<rdf:Description", i));
        if(node instanceof BNode) {
            writer.write(" rdf:nodeID=\"" + generateBnodeId(node.getLabel()) + "\"");
        } else {
            writer.write(" rdf:about=\"" + node.getLabel() + "\"");
        }
        writer.write(">\n");

        PredicateIterator iterator = new PredicateIterator(store.querySPO(node.toString()));
        while(iterator.hasNext()) {
            writePredicate(writer, iterator.next(), i+1);
        }
        writer.write(indent("</rdf:Description>\n", i));
    }

    /**
     * @param writer
     * @param iteration
     */
    private void writePredicate(OutputStreamWriter writer, PredicateIterator.PredicateIteration iteration, int i) throws IOException {
        Resource predicate = iteration.predicate;
        boolean isFunctional = context.isFunctional(predicate.getLabel());

        String tag = getXmlTag(predicate.getLabel());
        if (tag == null) {
            // excluded predicate, return
            return;
        }

        while(iteration.iterator.hasNext()) {
            Triplet triplet = iteration.iterator.next();
            if (context.isExclude(triplet)) {
                // if the triplet contain excluded namespace, ignore it
                continue;
            }
            Node object = triplet.getObject();
            writer.write(indent("<" + tag, i));
            if(object instanceof Resource || object instanceof BNode) {
                if (context.isEmbeddable(triplet)) {
                    writer.write(">\n");
                    writeResource(writer, object, i+1);
                    writer.write(indent("</"  + tag.split(" ")[0] + ">\n", i));
                } else {
                    writer.write(" rdf:resource=\"" + object.getLabel() + "\" />\n");
                }
            } else {
                if(object instanceof Literal) {
                    Literal literal = (Literal) object;
                    if(literal.getLanguageTag() != null) {
                        writer.write(" xml:lang=\"" + literal.getLanguageTag() + "\"");
                    }
                    if(literal.getDatatype() != null) {
                        writer.write(" rdf:datatype=\"" + literal.getDatatype().getLabel() + "\"");
                    }
                }
                writer.write(">" + escapeText(object.getLabel()));
                writer.write("</"  + tag.split(" ")[0] + ">\n");
            }

            // if the property is functional, stop now
            if (isFunctional) {
                return;
            }
        }
    }

    /**
     * @param count
     * @return
     */
    public String indent(String line, int count) {
        return StringUtils.repeat("\t", count) + line;
    }

    /**
     * @param uri
     * @return
     */
    protected String getXmlTag(String uri) {
        if (context.isExclude(uri)) {
            return null;
        }
        String namespace = SplitIRI.namespace(uri);

        // #1 : test default namespace
        if(context.getDefaultNamespace() != null) {
            if (context.getDefaultNamespace().equals(namespace)) {
                return uri.substring(context.getDefaultNamespace().length());
            }
        }

        // #2 else, use prefix mapping
        String prefix = prefixMapping.getNsURIPrefix(namespace);
        if (prefix == null) {
            prefix = "ns" + (prefixMapping.numPrefixes());
            synchronized (prefixMapping) {
                prefixMapping.setNsPrefix(prefix, namespace);
            }
            additionalPrefixes.add(prefix);
        }

        String tag = prefixMapping.shortForm(uri);
        if(additionalPrefixes.contains(prefix)) {
            tag += " xmlns:" + prefix + "=\"" + namespace + "\"";
        }

        return tag;
    }

    /**
     * @param writer
     * @throws IOException
     */
    private void writeFooter(OutputStreamWriter writer) throws IOException {
        writer.write("</rdf:RDF>");
    }

    /**
     * @param iri
     * @throws IOException
     */
    private String generateBnodeId(String iri) throws IOException {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        md.update(iri.getBytes());
        byte[] digest = md.digest();
        String hash = DatatypeConverter.printHexBinary(digest).toLowerCase();

        return "n" + hash;
    }

    /**
     * @param t
     * @return
     */
    private String escapeText(String t) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < t.length(); i++){
            char c = t.charAt(i);
            switch(c){
                case '<': sb.append("&lt;"); break;
                case '>': sb.append("&gt;"); break;
                case '&': sb.append("&amp;"); break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * @param node
     * @param out
     * @throws IOException
     */
    public void serializeIndexEntry(Node node, OutputStream out, Path filePath) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(out);
        writer.write(indent("<object>\n", 1));

        // uuid
//        String uri = node.getLabel();
//        String uuid = uri.substring(uri.lastIndexOf('/') + 1);
//        writer.write(indent("<uuid>" + uuid + "</uuid>\n", 2));

        // label
        String label = getFirstValue(node, "http://www.w3.org/2000/01/rdf-schema#label");
        if (label != null) {
            writer.write(indent("<label>" + escapeText(label) + "</label>\n", 2));
        }

        // lastUpdate
        String lastUpdateDatatourisme = getFirstValue(node, Datatourisme.lastUpdateDatatourisme.getURI());
        if (lastUpdateDatatourisme != null) {
            writer.write(indent("<lastUpdateDatatourisme>" + escapeText(lastUpdateDatatourisme) + "</lastUpdateDatatourisme>\n", 2));
        }

        // file
        writer.write(indent("<file>" + filePath.toString() + "</file>\n", 2));

        writer.write(indent("</object>", 1));
        writer.flush();
    }
}
