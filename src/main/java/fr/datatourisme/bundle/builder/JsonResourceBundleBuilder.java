/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.builder;

import fr.datatourisme.bundle.ResourceBundle;
import fr.datatourisme.bundle.ResourceStore;

import java.io.FileInputStream;
import java.io.IOException;

public class JsonResourceBundleBuilder extends ResourceBundleBuilder {
    public JsonResourceBundleBuilder(ResourceStore resourceStore) throws IOException {
        super(resourceStore);
    }

    @Override
    public ResourceStore.Format getFormat() {
        return ResourceStore.Format.json;
    }

    @Override
    public ResourceBundle build() throws IOException {
        // add context.jsonld
        writeZipEntry("context.jsonld", resourceStore.getFile("context.jsonld"));
        return super.build();
    }

    @Override
    protected void writeIndexHeader() throws IOException {
        indexOutputStream.write("[".getBytes());
    }

    @Override
    protected void writeIndexFooter() throws IOException {
        indexOutputStream.write("\n]\n".getBytes());
    }

    @Override
    protected void writeIndexEntry(FileInputStream input) throws IOException {
        if (resources.size() > 0) {
            indexOutputStream.write(",".getBytes());
        }
        super.writeIndexEntry(input);
    }
}
