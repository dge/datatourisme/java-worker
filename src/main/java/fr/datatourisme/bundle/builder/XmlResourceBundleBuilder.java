/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.builder;

import fr.datatourisme.bundle.ResourceStore;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class XmlResourceBundleBuilder extends ResourceBundleBuilder {
    public XmlResourceBundleBuilder(ResourceStore resourceStore) throws IOException {
        super(resourceStore);
    }

    @Override
    public ResourceStore.Format getFormat() {
        return ResourceStore.Format.xml;
    }

    @Override
    protected void writeIndexHeader() throws IOException {
        IOUtils.write("<?xml version=\"1.0\"?>\n", indexOutputStream);
        IOUtils.write("<objects>", indexOutputStream);
    }

    @Override
    protected void writeIndexFooter() throws IOException {
        IOUtils.write("\n</objects>\n", indexOutputStream);
    }
}
