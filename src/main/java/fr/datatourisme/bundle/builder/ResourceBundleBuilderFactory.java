/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.builder;

import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.bundle.ResourceStore;

import java.io.IOException;

/**
 * ResourceBundleFactory
 */
public class ResourceBundleBuilderFactory {
    ResourceStore resourceStore;

    /**
     * @param resourceStore
     */
    public ResourceBundleBuilderFactory(ResourceStore resourceStore) {
        this.resourceStore = resourceStore;
    }

    /**
     * @return
     * @throws IOException
     * @throws GraphStoreException
     */
    public ResourceBundleBuilder create(ResourceStore.Format format) throws IOException, GraphStoreException {
        switch (format) {
            case json:
                return new JsonResourceBundleBuilder(resourceStore);
            case xml:
                return new XmlResourceBundleBuilder(resourceStore);
            default:
                throw new RuntimeException("This format is not supported : " + format);
        }
    }
}
