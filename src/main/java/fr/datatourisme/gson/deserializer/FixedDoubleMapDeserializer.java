/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.gson.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class FixedDoubleMapDeserializer implements JsonDeserializer<Map> {
    @Override
    public Map deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        Map<String, Object> map = new HashMap<>();
        jsonElement.getAsJsonObject().entrySet().forEach(es -> {
            Object obj;
            if(es.getValue().isJsonObject()) {
                obj = context.deserialize(es.getValue(), Map.class);
            } else {
                obj = context.deserialize(es.getValue(), Object.class);
                if(obj instanceof Double) {
                    obj = ((Double) obj).intValue();
                }
            }
            map.put(es.getKey(), obj);
        });
        return map;
    }
}
