/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.Tube;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/{tubeName}/{fluxId}/run-task")
public class RunTaskController extends AbstractController {

    @Autowired
    ApplicationContext context;

    static class Request {
        public String task;
        public String endTask;
        public Map<String, Object> payload;
    }

    @ModelAttribute("tube")
    protected Tube getTube(@PathVariable String tubeName) {
        return context.getBean(tubeName, Tube.class);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@ModelAttribute("tube") Tube tube, @PathVariable Integer fluxId, @RequestBody Request request) throws IOException, JobException {
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};

//        Tube tube = context.getBean(tubeName, Tube.class);
//        if(tube == null) {
//            return response("Le tube \"" + tubeName + "\" n'existe pas", HttpStatus.BAD_REQUEST);
//        }

        if(request.task == null) {
            return error("Il manque le paramètre \"task\"", HttpStatus.BAD_REQUEST);
        }

        // test end task
        if(request.endTask != null) {
            try {
                AbstractTask endTask = (AbstractTask) context.getBean(request.endTask);
            } catch(Exception e) {
                return error("La tâche \""+request.endTask+"\" n'existe pas", HttpStatus.BAD_REQUEST);
            }
        }

        // convert payload
        Payload payload = Payload.fromJson(mapper.writeValueAsString(request.payload));
        payload.set("flux", fluxId);

        List<Map<String, Object>> reports = new ArrayList<>();

        String nextTask = request.task;
        while(nextTask != null) {
            AbstractTask task = (AbstractTask) context.getBean(nextTask);
            if(task == null) {
                return error("La tâche \"\" n'existe pas", HttpStatus.BAD_REQUEST);
            }
            task.setStorage(tube.getStorage().dir(fluxId));
            task.initFromPayload(payload);
            ReportEntry report = task.run();
            reports.add(mapper.readValue(report.toJson(), typeRef));

            if(request.endTask == null || nextTask.equals(request.endTask)) {
                nextTask = null;
            } else {
                String[] next = context.getBeanNamesForType(task.getNextTask());
                if(next.length > 0 && !next[0].equals("persist_rdf")) {
                    nextTask = next[0];
                } else {
                    nextTask = null;
                }
            }
        }

        return response(reports);
    }
}