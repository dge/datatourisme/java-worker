/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractController {
    protected Logger logger = Logger.getLogger(getClass());

    protected ResponseEntity<?> response(Object body, HttpStatus status) {
        return new ResponseEntity<>(body, status);
    }
    protected ResponseEntity<?> response(Object body) {
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    protected ResponseEntity<?> error(String message, HttpStatus status) {
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", message);
        errorMap.put("code", status.value());
        return response(Collections.singletonMap("error", errorMap), status);
    }

    protected ResponseEntity<StreamingResponseBody> errorAsStream(String message, HttpStatus status) {
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("message", message);
        errorMap.put("code", status.value());
        return ResponseEntity
            .status(status)
            .header("Content-type", "application/json; charset=utf-8")
            .body(outputStream -> outputStream.write(new ObjectMapper().writeValueAsBytes(Collections.singletonMap("error", errorMap))));
    }

    protected ResponseEntity<?> error(Exception e, HttpStatus status) {
        return error(e.getMessage(), status);
    }

    protected ResponseEntity<?> error(String message) {
        return error(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    protected ResponseEntity<?> error(Exception e) {
        return error(e.getMessage());
    }

    protected ResponseEntity<?> responseJson(String json, HttpStatus status) throws IOException {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-type", "application/json; charset=utf-8");
        return new ResponseEntity<>(json, responseHeaders, status);
    }

    protected ResponseEntity<?> responseJson(String json) throws IOException {
        return responseJson(json, HttpStatus.OK);
    }

    protected ResponseEntity<?> responseJsonContent(File file, HttpStatus status) throws IOException {
        byte[] bytes = Files.readAllBytes(file.toPath());
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Content-type", "application/json; charset=utf-8");
        return new ResponseEntity<>(bytes, responseHeaders, HttpStatus.OK);
    }

    protected ResponseEntity<?> responseJsonContent(File file) throws IOException {
        return responseJsonContent(file, HttpStatus.OK);
    }

    @ExceptionHandler(Exception.class)
    public @ResponseBody
    ResponseEntity<?> handleException(Exception e) {
        logger.error(e);
        return error(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
