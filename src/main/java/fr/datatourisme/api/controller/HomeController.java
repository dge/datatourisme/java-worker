/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

@RestController
@RequestMapping("")
public class HomeController extends AbstractController {

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> get() throws IOException {
        URL banner = HomeController.class.getClassLoader().getResource("banner.txt");

        File file = new File(banner.getFile());
        byte[] encoded = Files.readAllBytes(file.toPath());
        String content = new String(encoded);
        return response("<pre>" + content + "</pre>" + "<pre>Rien d'intéressant ici...</pre>");
    }

}