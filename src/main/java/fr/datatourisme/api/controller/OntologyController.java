/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.api.controller;

import fr.datatourisme.assembler.Assembler;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.serializer.OntologySerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/ontology")
public class OntologyController extends AbstractController {

    @Autowired
    Assembler assembler;

    Map<String, File> cacheFiles = new HashMap<>();

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> get(@RequestParam(required = false) String lang) throws IOException {
       return get(null, lang);
    }

    @GetMapping("/{name}")
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable String name, @RequestParam(required = false) String lang) throws IOException {
        File jsonFile = getOntologyCacheFile(name, lang);
        return responseJsonContent(jsonFile);
    }

    /**
     * @param type
     * @return
     * @throws IOException
     */
    private File getOntologyCacheFile(String type, String lang) throws IOException {
        lang = lang != null ? lang : Ontology.defaultLanguage;
        String key = (type != null ? "ontology_" + type : "ontology") + "_" + lang;

        File file = cacheFiles.get(key);
        if(file != null && file.exists()) {
            return file;
        }

        file = File.createTempFile(key, ".tmp");
        String uri = ":ontology" + (type != null ? type.substring(0, 1).toUpperCase()+ type.substring(1).toLowerCase() : "");
        Ontology ontology = assembler.createOntology(uri);

        FileWriter writer = new FileWriter(file);
        writer.write(OntologySerializer.serialize(ontology, lang));
        writer.close();

        cacheFiles.put(key, file);
        return file;
    }

}