/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import fr.datatourisme.command.*;
import org.nanocom.console.input.ArgsInput;

/**
 * Main console application
 */
public class Application extends org.nanocom.console.Application
{
    public Application() {
        setName("DATAtourisme Java Worker");
        setVersion("0.1");
        setAutoExit(true);
        setCatchExceptions(false);
    }

    public static void main(String[] args)
    {
        Application console = new Application();

        // add commands
        console.add(new HelloWorld());
        console.add(new StartWorker());
        console.add(new StartServer());
        console.add(new SerializeOntology());
        console.add(new XpathSchema());
        console.add(new BenchProcess());
        console.add(new BuildBundleResourceStore());
        console.add(new BuildGraphStore());

        // run !
        console.run(new ArgsInput(args));
    }
}