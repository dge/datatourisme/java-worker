/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.logger;

import fr.datatourisme.worker.Job;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Priority;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JobLogger extends AbstractJobLogger {

    Priority minLogLevel = org.apache.log4j.Level.INFO;

    public static JobLogger getLogger(Job job) {
        JobLogger reporter = new JobLogger();
        reporter.setFile(job.getStorage().dir("jobs").file(job.getUuid() + ".log"));
        return reporter;
    }

    /**
     * @param level
     * @param message
     */
    public void log(Priority level, Object message, String stage) {

        if(message instanceof Throwable) {
            message = ((Throwable) message).getMessage();
        }

        logger.log(level, stage + " - " + message);

        if(file != null && level.isGreaterOrEqual(minLogLevel))  {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String msgString = String.format("%s - %s - %s - %s", formater.format(new Date()), level.toString(), stage, message );
            try {
                FileUtils.write(file, msgString + "\n", true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void log(Priority level, Object message) {
        this.log(level, message, this.defaultStage);
    }

    /*
    *//**
     This method overrides {@link org.apache.log4j.Logger#getLogger} by supplying
     its own factory type as a parameter.
     *//*
    public static Logger getLogger(Job job) throws JobException {
        String loggerId = String.join(".", Job.class.getName(), "Job.logger", job.getUuid());
        Logger logger = Logger.getLogger(loggerId, factory);
        if(logger.getAppender("reportFile") == null) {
            FileAppender appender = new FileAppender();
            appender.setName("reportFile");
            appender.setFile(job.getStorage().dir("jobs").file(job.getUuid() + ".log").getAbsolutePath());
            appender.setLayout(new PatternLayout("%d{HH:mm:ss} - %p - %X{task-name} - %m%n"));
            //appender.setThreshold(Level.INFO);
            appender.setAppend(true);
            appender.activateOptions();
            logger.addAppender(appender);
        }
        return logger;
    }*/
}
