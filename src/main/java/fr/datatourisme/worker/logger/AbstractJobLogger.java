/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.logger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import java.io.File;

abstract public class AbstractJobLogger {

    protected Logger logger = Logger.getLogger(this.getClass().getName());
    protected File file;
    protected String defaultStage;

    abstract public void log(Priority level, Object message);

    public void debug(Object message) {
        log(Level.DEBUG, message);
    }

    public void info(Object message) {
        log(Level.INFO, message);
    }

    public void fatal(Object message) {
        log(Level.FATAL, message);
    }

    public void warn(Object message) {
        log(Level.WARN, message);
    }

    public void error(Object message) {
        log(Level.ERROR, message);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public AbstractJobLogger setDefaultStage(String defaultStage) {
        this.defaultStage = defaultStage;
        return this;
    }
}
