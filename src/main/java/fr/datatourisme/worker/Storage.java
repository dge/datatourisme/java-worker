/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import java.io.File;

public class Storage {
    private File root;

    public Storage(File root, Boolean create) {
        this.root = root;
        if(create && !this.root.exists()) {
            this.root.mkdir();
        }
    }

    public Storage(File root) {
        this(root, true);
    }

    public Storage dir(String name, Boolean create) {
        File subStorage = new File(root, name);
        return new Storage(subStorage, create);
    }

    public Storage dir(String subDir) {
        return dir(subDir, true);
    }

    public Storage dir(Object name) {
        return dir(name.toString(), true);
    }

    public File file(String name) {
        return new File(root, name);
    }

    public File toFile() {
        return root;
    }
}
