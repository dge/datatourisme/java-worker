/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.exception;

public class MissingRequiredPayloadException extends InvalidPayloadException {
    private static final String msg = "Il manque une clé obligatoire dans le payload du job : %s";
    public MissingRequiredPayloadException(String key) {
        super(String.format(msg, key));
    }
}
