/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.exception;

public class TemporaryJobException extends JobException {
    private int delay;
    private int maxTries;
    public TemporaryJobException(String message) {
        super(message);
    }
    public TemporaryJobException(String message, Throwable cause) {
        super(message, cause);
    }

    public int getDelay() {
        return delay;
    }

    /**
     * @param delay (seconds)
     * @return
     */
    public TemporaryJobException setDelay(int delay) {
        this.delay = delay;
        return this;
    }

    /**
     * @return
     */
    public int getMaxTries() {
        return maxTries;
    }

    /**
     * @param maxTries
     * @return
     */
    public TemporaryJobException setMaxTries(int maxTries) {
        this.maxTries = maxTries;
        return this;
    }
}
