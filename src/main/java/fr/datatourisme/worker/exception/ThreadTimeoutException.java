/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.exception;

import java.util.concurrent.CancellationException;

/**
 * fr.datatourisme.worker.exception
 */
public class ThreadTimeoutException extends Exception {
    private static final String msg = "The thread has been cancelled due to timeout";
    public ThreadTimeoutException(CancellationException e) {
        super(msg, e);
    }
}
