/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import fr.datatourisme.vocabulary.DatatourismeData;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;
import org.apache.jena.shared.JenaException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;


/**
 * Bind a URI node to the first argument.
 * For any given combination of the remaining arguments
 * the same URI will be returned.
 */
public class MakeDataResource extends BaseBuiltin {

    /**
     * Return a name for this builtin, normally this will be the name of the
     * functor that will be used to invoke it.
     */
    @Override
    public String getName() {
        return "makeDataResource";
    }

    /**
     * This method is invoked when the builtin is called in a rule body.
     * @param args the array of argument values for the builtin, this is an array
     * of Nodes, some of which may be Node_RuleVariables.
     * @param length the length of the argument list, may be less than the length of the args array
     * for some rule engines
     * @param context an execution context giving access to other relevant data
     * @return return true if the buildin predicate is deemed to have succeeded in
     * the current environment
     */
    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        StringBuilder key = new StringBuilder();
        for (int i = 1; i < length; i++) {
            Node n = getArg(i, args, context);
            if (n.isBlank()) {
                key.append("B"); key.append(n.getBlankNodeLabel());
            } else if (n.isURI()) {
                key.append("U"); key.append(n.getURI());
            } else if (n.isLiteral()) {
                key.append("L"); key.append(n.getLiteralLexicalForm());
                if (n.getLiteralLanguage() != null) key.append("@" + n.getLiteralLanguage());
                if (n.getLiteralDatatypeURI() != null) key.append("^^" + n.getLiteralDatatypeURI());
            } else {
                key.append("O"); key.append(n.toString());
            }
        }

        try {
            MessageDigest digester = MessageDigest.getInstance("MD5");
            digester.reset();
            byte[] digest = digester.digest(key.toString().getBytes());
            String uuid = UUID.nameUUIDFromBytes(digest).toString();
            Node skolem = NodeFactory.createURI(DatatourismeData.resource(uuid).getURI());
            return context.getEnv().bind(args[0], skolem);
        } catch (NoSuchAlgorithmException e) {
            throw new JenaException(e);
        }
    }
}
