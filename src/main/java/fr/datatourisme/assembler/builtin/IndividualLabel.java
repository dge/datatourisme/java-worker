/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyIndividual;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.impl.LiteralLabelFactory;
import org.apache.jena.reasoner.rulesys.BindingEnvironment;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

/**
 * Bind a variable to the label of the given individual
 */
public class IndividualLabel extends BaseBuiltin {
    Ontology ontology;

    public IndividualLabel(Ontology ontology) {
        this.ontology = ontology;
    }

    /**
     * Return a name for this builtin, normally this will be the name of the
     * functor that will be used to invoke it.
     */
    @Override
    public String getName() {
        return "individualLabel";
    }

    /**
     * Return the expected number of arguments for this functor or 0 if the number is flexible.
     */
    @Override
    public int getArgLength() {
        return 2;
    }

    /**
     * This method is invoked when the builtin is called in a rule body.
     * @param args the array of argument values for the builtin, this is an array
     * of Nodes, some of which may be Node_RuleVariables.
     * @param context an execution context giving access to other relevant data
     * @return return true if the buildin predicate is deemed to have succeeded in
     * the current environment
     */
    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        checkArgs(length, context);
        BindingEnvironment env = context.getEnv();
        Node node = args[0];
        OntologyIndividual individual = ontology.getIndividual(node.getURI());
        if(individual != null && individual.getLabel() != null) {
            Node now = NodeFactory.createLiteral( LiteralLabelFactory.createTypedLiteral(individual.getLabel()));
            return env.bind(args[length-1], now);
        }
        return false;
    }
}
