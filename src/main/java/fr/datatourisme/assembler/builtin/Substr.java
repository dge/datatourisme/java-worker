/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.reasoner.rulesys.BuiltinException;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

/**
 * fr.datatourisme.assembler.builtin
 */
public class Substr extends BaseBuiltin {

    /**
     * Return a name for this builtin, normally this will be the name of the
     * functor that will be used to invoke it.
     */
    @Override
    public String getName() {
        return "substr";
    }

    /**
     * Return the expected number of arguments for this functor or 0 if the number is flexible.
     */
    @Override
    public int getArgLength() {
        return 4;
    }

    /**
     * This method is invoked when the builtin is called in a rule body.
     * @param args the array of argument values for the builtin, this is an array
     * of Nodes, some of which may be Node_RuleVariables.
     * @param length the length of the argument list, may be less than the length of the args array
     * for some rule engines
     * @param context an execution context giving access to other relevant data
     * @return return true if the buildin predicate is deemed to have succeeded in
     * the current environment
     */
    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {

        String value = lex(getArg(0, args, context), context);

        Node beginIndexNode = getArg(1, args, context);
        if(!beginIndexNode.isLiteral() || !(beginIndexNode.getLiteralValue() instanceof Integer)) {
            throw new BuiltinException(this, context, "Illegal node type: " + 1);
        }
        Integer beginIndex = (Integer) beginIndexNode.getLiteralValue();

        Node endIndexNode = getArg(2, args, context);
        if(!endIndexNode.isLiteral() || !(endIndexNode.getLiteralValue() instanceof Integer)) {
            throw new BuiltinException(this, context, "Illegal node type: " + 1);
        }
        Integer endIndex = (Integer) endIndexNode.getLiteralValue();

        Node result = NodeFactory.createLiteral(value.substring(beginIndex, endIndex));

        return context.getEnv().bind(args[length-1], result);
    }

    /**
     * Return the appropriate lexical form of a node
     */
    protected String lex(Node n, RuleContext context) {
        if (n.isBlank()) {
            return n.getBlankNodeLabel();
        } else if (n.isURI()) {
            return n.getURI();
        } else if (n.isLiteral()) {
            return n.getLiteralLexicalForm();
        } else {
            throw new BuiltinException(this, context, "Illegal node type: " + n);
        }
    }
}
