/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.reasoner.InfGraph;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;
import org.apache.jena.vocabulary.RDF;

import java.util.HashSet;
import java.util.Set;

/**
 * Remove an element from a list
 */
public class ListDrop extends BaseBuiltin {
    public ListDrop() {
    }

    public String getName() {
        return "listDrop";
    }

    @Override
    public int getArgLength() {
        return 2;
    }

    public void headAction(Node[] args, int length, RuleContext context) {
        this.checkArgs(length, context);
        Node n0 = this.getArg(0, args, context);
        Node n1 = this.getArg(1, args, context);
        listDrop(n0, n1, context);
    }

    protected void listDrop(Node list, Node element, RuleContext context) {
        InfGraph inf = context.getGraph();
        Graph raw = inf.getRawGraph();
        Graph deductions = inf.getDeductionsGraph();

        // get list triples
        Set<Triple> triples = listTriples(list, context);

        // find target elmt
        Node targetElmt = null;
        for(Triple t: triples) {
            if(t.getObject().equals(element)) {
                targetElmt = t.getSubject();
                break;
            }
        }

        if(targetElmt == null) {
            return;
        }

        // if found
        Node targetRest = null;
        for(Triple t: triples) {
            if(t.getSubject().equals(targetElmt) && t.getPredicate().hasURI(RDF.rest.getURI())) {
                targetRest = t.getObject();
                break;
            }
        }

        if(targetRest == null) {
            return;
        }

        for(Triple t: triples) {
            if(t.getSubject().equals(targetElmt) || t.getSubject().equals(targetRest)) {
                raw.delete(t);
                deductions.delete(t);
            }
            if(t.getSubject().equals(targetRest)) {
                raw.add(new Triple(targetElmt, t.getPredicate(), t.getObject()));
                deductions.add(new Triple(targetElmt, t.getPredicate(), t.getObject()));
            }
        }
    }

    /**
     * Get all triple of a list
     *
     * @param list
     * @param context
     * @return
     */
    protected Set<Triple> listTriples(Node list, RuleContext context) {
        Set<Triple> triples = new HashSet<>();
        if(list != null && !list.equals(RDF.Nodes.nil)) {
            context.find(list, null, null).forEachRemaining(t -> {
                triples.add(t);
                if(t.getPredicate().hasURI(RDF.rest.getURI())) {
                    triples.addAll(listTriples(t.getObject(), context));
                }
            });
        }
        return triples;
    }

    public boolean isMonotonic() {
        return false;
    }
}
