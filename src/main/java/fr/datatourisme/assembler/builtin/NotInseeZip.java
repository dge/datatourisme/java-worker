/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.graph.Node;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

import java.util.HashMap;
import java.util.Map;

public class NotInseeZip extends BaseBuiltin {

    private static Map<String, String> EXCEPTIONS;

    static {
        EXCEPTIONS = new HashMap<>();
        EXCEPTIONS.put("86042", "37160");
        EXCEPTIONS.put("15105", "43450");
        EXCEPTIONS.put("04234", "05130");
        EXCEPTIONS.put("07136", "48250");
        EXCEPTIONS.put("04058", "05110");
        EXCEPTIONS.put("04150", "05130");
        EXCEPTIONS.put("24335", "33220");
        EXCEPTIONS.put("04194", "06260");
        EXCEPTIONS.put("39274", "01410");
        EXCEPTIONS.put("39283", "01590");
        EXCEPTIONS.put("03250", "42620");
        EXCEPTIONS.put("04066", "05110");
        EXCEPTIONS.put("04154", "05160");
        EXCEPTIONS.put("39102", "01590");
        EXCEPTIONS.put("71085", "21340");
        EXCEPTIONS.put("24189", "33220");
        EXCEPTIONS.put("74109", "01200");
        EXCEPTIONS.put("99138", "98000");
        EXCEPTIONS.put("83105", "13780");
        EXCEPTIONS.put("80756", "62760");
        EXCEPTIONS.put("91479", "94390");
        EXCEPTIONS.put("04170", "06260");
        EXCEPTIONS.put("51522", "52100");
        EXCEPTIONS.put("51478", "52100");
        EXCEPTIONS.put("26374", "05700");

    }

    /**
     * Return a name for this builtin, normally this will be the name of the
     * functor that will be used to invoke it.
     */
    @Override
    public String getName() {
        return "notInseeZip";
    }

    /**
     * Return the expected number of arguments for this functor or 0 if the number is flexible.
     */
    @Override
    public int getArgLength() {
        return 2;
    }

    /**
     * This method is invoked when the builtin is called in a rule body.
     * @param args the array of argument values for the builtin, this is an array
     * of Nodes, some of which may be Node_RuleVariables.
     * @param length the length of the argument list, may be less than the length of the args array
     * for some rule engines
     * @param context an execution context giving access to other relevant data
     * @return return true if the buildin predicate is deemed to have succeeded in
     * the current environment
     */
    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        checkArgs(length, context);
        String insee = getArg(0, args, context).getLiteral().toString().toUpperCase();
        String zip = getArg(1, args, context).getLiteral().toString().toUpperCase();

        if(EXCEPTIONS.containsKey(insee) && EXCEPTIONS.get(insee).equals(zip)) {
            return false;
        }

        String subInsee = insee.substring(0, 2);
        String subZip = zip.substring(0, 2);

        if(subInsee.equals("2A") || subInsee.equals("2B")) {
            return !subZip.equals("20");
        }

        return !subInsee.equals(subZip);
    }

}
