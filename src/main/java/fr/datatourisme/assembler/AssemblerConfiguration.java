/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler;

import fr.datatourisme.ontology.Ontology;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class AssemblerConfiguration {

    @Value("${datatourisme.ontology.path}")
    private String ontologyPath;

    @Value("${datatourisme.ontology-extra.path}")
    private String ontologyExtraPath;

    /**
     * General assembler
     */
    @Bean(name="assembler") @Lazy
    public Assembler getAssembler() {
        ImmutableMap map = ImmutableMap.of(
            "ontology", ontologyPath.trim(),
            "ontology-extra", ontologyExtraPath.trim()
        );
        return new Assembler(map);
    }

    @Bean(name = "ontology") @Lazy
    public Ontology getOntology(Assembler assembler) {
        return assembler.createOntology(":ontology");
    }

    @Bean(name = "ontologyLite") @Lazy
    public Ontology getOntologyLite(Assembler assembler) {
        return assembler.createOntology(":ontologyLite");
    }

    @Bean(name = "ontologyAlignment") @Lazy
    public Ontology getOntologyAlignment(Assembler assembler) {
        return assembler.createOntology(":ontologyAlignment");
    }
}
