/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.utils;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * fr.datatourisme.utils
 */
public class StreamUtils {

    /**
     * Batch stream
     *
     * @param list
     * @param batchSize
     * @param <T>
     * @return
     */
    public static <T> Stream<List<T>> batchStream(List<T> list, int batchSize) {
        return IntStream.range(0, (list.size()+batchSize-1)/batchSize)
            .mapToObj(i -> list.subList(i*batchSize, Math.min(list.size(), (i+1)*batchSize)));
    }

}
