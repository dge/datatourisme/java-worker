/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.utils;

import org.jdom2.*;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.InputSource;

import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import java.io.StringReader;

/**
 * 
 */
public class JDOM2Utils {

    /**
     * Get a node value based on the type
     *
     * @param obj
     * @return
     */
    public static String getNodeValue(Object obj) {
        if(obj instanceof Attribute) {
            return ((Attribute) obj).getValue();
        } else if(obj instanceof Element) {
            return ((Element) obj).getValue();
        } else if(obj instanceof Text) {
            return ((Text) obj).getText();
        } else if (obj instanceof String) {
            return (String) obj;
        } else if (obj instanceof Comment) {
            return ((Comment) obj).getText();
        } else if (obj instanceof ProcessingInstruction) {
            return ((ProcessingInstruction) obj).getData();
        } else if (obj instanceof Namespace) {
            return ((Namespace) obj).getURI();
        } else {
            return obj.toString();
        }
    }

    /**
     * @return
     */
    public static String asXML(Element obj) {
        XMLOutputter outp = new XMLOutputter();
        return outp.outputString(obj);
    }

    /**
     * @return
     */
    public static Source asXMLSource(Element obj) {
        return new SAXSource(new InputSource(new StringReader(JDOM2Utils.asXML(obj))));
    }
}
