/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.task;

import fr.datatourisme.webapp.api.message.JobReportMessage;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;

public class ReportingTask extends AbstractTask {

    public ReportingTask() {
    }

    public static int defaultPriority = 0;

    @Override
    public String getMachineName() {
        return "reporting";
    }

    @Override
    protected String getLocalizedName() {
        return "Envoi du rapport";
    }

    @Override
    public ReportEntry execute() throws Exception {
        logger.info("Envoi du rapport de traitement");
        job.callWebappApi("status/success", JobReportMessage.class);
        return null;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        // nothing to do
    }
}
