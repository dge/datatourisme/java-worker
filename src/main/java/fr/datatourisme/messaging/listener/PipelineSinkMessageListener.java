/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.messaging.listener;

import fr.datatourisme.dataset.BlazegraphRestClient;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.shared.impl.PrefixMappingImpl;
import org.apache.jena.sparql.util.FmtUtils;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;
import org.apache.log4j.Logger;
import org.seaborne.patch.RDFPatch;
import org.seaborne.patch.RDFPatchOps;
import org.seaborne.patch.changes.PatchSummary;
import org.seaborne.patch.changes.RDFChangesBase;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.List;

/**
 * This message listener execute a RDFPatch on the extra dataset
 */
public class PipelineSinkMessageListener implements MessageListener {
    private static final Logger logger = Logger.getLogger(PipelineSinkMessageListener.class);

    @Autowired
    private BlazegraphRestClient datasetRestClient;

    @Autowired
    private BlazegraphRestClient extraDatasetRestClient;

    @Override
    public void onMessage(Message message) {
        InputStream inputStream = new ByteArrayInputStream(message.getBody());
        RDFPatch rdfPatch = RDFPatchOps.read(inputStream);
        if (rdfPatch != null) {
            // log
            PatchSummary summary = RDFPatchOps.summary(rdfPatch);
            logger.info(String.format("Received patch in pipeline sink : %d ADD, %d DELETE", summary.countAddData, summary.countDeleteData));
            // apply
            rdfPatch.apply(new RDFPatchExecute());
        }
    }

    @Override
    public void containerAckMode(AcknowledgeMode mode) {
        // todo ?
    }

    @Override
    public void onMessageBatch(List<Message> messages) {
        messages.forEach(this::onMessage);
    }

    /**
     * This RDFChanges execute the patch on the SparqlQueryExecutionService
     */
    private class RDFPatchExecute extends RDFChangesBase {
        private final UpdateRequest tripleRequest = UpdateFactory.create();
        private final UpdateRequest quadRequest = UpdateFactory.create();
        private final PrefixMapping prefixMapping = new PrefixMappingImpl();

        @Override
        public void add(Node g, Node s, Node p, Node o) {
            Triple triple = new Triple(s, p, o);
            tripleRequest.add(String.format("INSERT DATA { %s }", FmtUtils.stringForTriple(triple, prefixMapping)));
            quadRequest.add(String.format("INSERT DATA { GRAPH %s { %s } }", FmtUtils.stringForNode(g), FmtUtils.stringForTriple(triple, prefixMapping)));
        }

        @Override
        public void delete(Node g, Node s, Node p, Node o) {
            Triple triple = new Triple(s, p, o);
            tripleRequest.add(String.format("DELETE DATA { %s }", FmtUtils.stringForTriple(triple, prefixMapping)));
            quadRequest.add(String.format("DELETE DATA { GRAPH %s { %s } }", FmtUtils.stringForNode(g), FmtUtils.stringForTriple(triple, prefixMapping)));
        }

        @Override
        public void txnCommit() {
            try {
                datasetRestClient.update(tripleRequest);
                extraDatasetRestClient.update(quadRequest);
            } catch (IOException | URISyntaxException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }
}
