/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.messaging;

import fr.datatourisme.messaging.listener.PipelineSinkMessageListener;
import fr.datatourisme.messaging.listener.PipelineSourceMessageListener;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @see "https://docs.spring.io/spring-amqp/reference/html/#introduction-2"
 */
@Configuration
public class MessagingConfiguration {

    @Bean
    public ConnectionFactory connectionFactory(@Value( "${rabbitmq.host}" ) String host, @Value( "${rabbitmq.port:5672}" ) int port, @Value( "${rabbitmq.username:#{null}}" ) String username, @Value( "${rabbitmq.password:#{null}}" ) String password) {
        CachingConnectionFactory factory = new CachingConnectionFactory(host, port);
        if (username != null) {
            factory.setUsername(username);
        }
        if (password != null) {
            factory.setPassword(password);
        }
        return factory;
    }

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    @Profile({"server", "worker"})
    Exchange pipelineSourceExchange() {
        return ExchangeBuilder.fanoutExchange("datatourisme.pipeline.source").durable(true).build();
    }

    @Bean
    @Profile({"server"})
    Queue pipelineSourceQueue() {
        return QueueBuilder.nonDurable().exclusive().autoDelete().build();
    }

    @Bean
    @Profile({"server"})
    Binding pipelineSourceBinding(Queue pipelineSourceQueue, FanoutExchange pipelineSourceExchange) {
        return BindingBuilder.bind(pipelineSourceQueue).to(pipelineSourceExchange);
    }

    @Bean
    @Profile({"server"})
    public PipelineSourceMessageListener pipelineSourceMessageListener() {
        return new PipelineSourceMessageListener();
    }

    @Bean
    @Profile({"server"})
    SimpleMessageListenerContainer pipelineSourceListenerContainer(ConnectionFactory connectionFactory, Queue pipelineSourceQueue, PipelineSourceMessageListener messageListener) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener(messageListener);
        container.setQueues(pipelineSourceQueue);
        container.setDefaultRequeueRejected(false);
        container.setAutoStartup(false); // important !!
        return container;
    }

    @Bean
    @Profile({"server"})
    Queue pipelineSinkQueue() {
        return QueueBuilder.durable("datatourisme.pipeline.sink").build();
    }

    @Bean
    @Profile({"server"})
    public PipelineSinkMessageListener pipelineSinkMessageListener() {
        return new PipelineSinkMessageListener();
    }

    @Bean
    @Profile({"server"})
    SimpleMessageListenerContainer pipelineSinkListenerContainer(ConnectionFactory connectionFactory, Queue pipelineSinkQueue, PipelineSinkMessageListener messageListener) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setMessageListener(messageListener);
        container.setQueues(pipelineSinkQueue);
        container.setAutoStartup(false); // important !!
        return container;
    }
}
