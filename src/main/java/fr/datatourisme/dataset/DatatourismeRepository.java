/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset;

import fr.datatourisme.dataset.query.AbstractQueryExecutionService;
import fr.datatourisme.utils.StreamUtils;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeData;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.vocabulary.Schema;
import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.graph.FrontsNode;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.shared.Lock;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.lang.sparql_11.ParseException;
import org.apache.jena.sparql.util.FmtUtils;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * fr.datatourisme.dataset
 */
public class DatatourismeRepository {
    AbstractQueryExecutionService queryExecutionService;

    public DatatourismeRepository(AbstractQueryExecutionService queryExecutionService) {
        this.queryExecutionService = queryExecutionService;
    }

    public QueryExecution queryExecution(Query query) {
        return queryExecutionService.queryExecution(query);
    }

    /**
     * Return a map of resources and list of POI using it
     *
     * @return Map
     */
    public Map<Resource, Set<Resource>> selectResourcesUsages(Set<Resource> resources, Map<Resource, Set<Resource>> visited) {

        Map<Resource, Set<Resource>> poiMap = Collections.synchronizedMap(new HashMap<>());

        // prepare list
        List<Node> nodes = resources.stream().map(FrontsNode::asNode).collect(Collectors.toList());

        // execute by batch (1000 nodes)
        StreamUtils.batchStream(nodes, 1000).parallel().forEach(batch -> {
            try {
                /*
                SELECT DISTINCT ?resource ?subject ?isPoi WHERE {
                    ?subject ?p ?resource.
                    BIND(EXISTS{?subject a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest>} AS ?isPoi)
                } VALUES ?resource { ... }
                */

                Set<Resource> nexResList = new HashSet<>();
                Map<Resource, Set<Resource>> resourceMap = new HashMap<>();

                SelectBuilder sb = new SelectBuilder()
                    .addVar("?resource").addVar("?subject").addVar("?isPoi")
                    .setDistinct(true)
                    .addWhere(new Triple(Var.alloc("subject"), Var.alloc("p"), Var.alloc("resource")))
                    .addBind("EXISTS {?subject a " + FmtUtils.stringForResource(Datatourisme.PointOfInterest) + " }", "?isPoi")
                    .addValueVar("resource", batch.toArray());

                ResultSet rs = queryExecution(sb.build()).execSelect();

                rs.forEachRemaining(qs -> {
                    Resource resource = qs.getResource("resource");
                    Resource subject = qs.getResource("subject");
                    Literal isPoi = qs.getLiteral("isPoi");

                    if(isPoi.getBoolean()) {
                        synchronized (poiMap) {
                            poiMap.computeIfAbsent(resource, k -> new HashSet<>()).add(subject);
                        }
                        synchronized (visited) {
                            visited.computeIfAbsent(resource, k -> new HashSet<>()).add(subject);
                        }
                    } else {
                        synchronized (visited) {
                            if(visited.get(subject) != null) {
                                poiMap.computeIfAbsent(resource, k -> new HashSet<>()).addAll(visited.get(subject));
                                return;
                            }
                        }
                        resourceMap.computeIfAbsent(resource, k -> new HashSet<>()).add(subject);
                        nexResList.add(subject);
                    }
                });

                if(nexResList.size() > 0) {
                    Map<Resource, Set<Resource>> subMap = selectResourcesUsages(nexResList, visited);
                    subMap.forEach((res, subjects) -> {
                        resourceMap.entrySet().stream()
                            .filter(es -> es.getValue().contains(res))
                            .map(Map.Entry::getKey)
                            .forEach(resource -> {
                                synchronized (poiMap) {
                                    poiMap.computeIfAbsent(resource, k -> new HashSet<>()).addAll(subjects);
                                }
                                synchronized (visited) {
                                    visited.computeIfAbsent(resource, k -> new HashSet<>()).addAll(subjects);
                                }
                            });
                    });
                }

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        });

        return poiMap;
    }

    /**
     * @param resources
     * @return
     */
    public Map<Resource, Set<Resource>> selectResourcesUsages(Set<Resource> resources) {
        Map<Resource, Set<Resource>> visited = Collections.synchronizedMap(new HashMap<>());
        return selectResourcesUsages(resources, visited);
    }

    /**
     * Return all the POI for a given flux id and org id
     *
     * @return
     */
    public Map<String, Resource> findAllPOI(int fluxIdentifier) {
        /*
        SELECT DISTINCT ?id ?s WHERE {
          ?s a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> .
          ?s <https://www.datatourisme.gouv.fr/resource/metadata#hasFluxIdentifier> "1"^^<http://www.w3.org/2001/XMLSchema#int> .
          ?s dc:identifier ?id
        }
        */
        SelectBuilder sb = new SelectBuilder()
            .addVar("?id").addVar("?s")
            .setDistinct(true)
            .addWhere("?s", "a", Datatourisme.PointOfInterest)
            .addWhere("?s", DatatourismeMetadata.hasFluxIdentifier, fluxIdentifier)
            .addWhere("?s", DC.identifier, "?id");

        Map<String, Resource> list = new HashMap<>();
        ResultSet rs = queryExecution(sb.build()).execSelect();
        rs.forEachRemaining(qs -> {
            list.put(qs.getLiteral("id").getString(), qs.getResource("s"));
        });

        return list;
    }

    /**
     * Return an existing POI
     *
     * @return
     */
    public Resource findPOI(String uri) {
        // uri validation
        try {
            new URI(uri);
        } catch (URISyntaxException ex) { return null ; }

        /*
        SELECT ?s WHERE { ?s a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> } VALUES ?s { <...> }
        */
        SelectBuilder sb = new SelectBuilder()
            .addVar("?s")
            .addWhere("?s", "a", Datatourisme.PointOfInterest)
            .addValueVar("s", NodeFactory.createURI(uri));

        ResultSet rs = queryExecution(sb.build()).execSelect();
        if (rs.hasNext()) {
            return rs.next().getResource("s");
        }

        return null;
    }

    /**
     * @param subjects
     * @return
     */
    public Model constructPOIHierarchy(Set<Resource> subjects) {
        /*
        CONSTRUCT WHERE { ?s  ?p  ?o } VALUES ?s { ... }
        */
        Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
        ConstructBuilder cb = new ConstructBuilder().addWhere(triple);
        Set<Resource> visited = Collections.synchronizedSet(new HashSet<>());
        return constructHierarchy(subjects, cb, visited);
    }

    /**
     * @param subjects
     * @param visited
     *
     * @return
     */
    private Model constructResourceHierarchy(Set<Resource> subjects, Set<Resource> visited) {
        /*
        CONSTRUCT WHERE {
            ?s  ?p  ?o
            FILTER NOT EXISTS { ?s  a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> }
        } VALUES ?s { ... }
        */
        try {
            ConstructBuilder cb = new ConstructBuilder();
            Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
            cb.addConstruct(triple);
            cb.addWhere(triple);
            cb.addFilter("NOT EXISTS { ?s a " + FmtUtils.stringForResource(Datatourisme.PointOfInterest) + " }");
            return constructHierarchy(subjects, cb, visited);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public Model constructResourceHierarchy(Set<Resource> list) {
        Set<Resource> visited = Collections.synchronizedSet(new HashSet<>());
        return constructResourceHierarchy(list, visited);
    }

    /**
     * @param subjects
     * @param constructBuilder
     * @param visited
     * @return
     */
    private Model constructHierarchy(Set<Resource> subjects, ConstructBuilder constructBuilder, Set<Resource> visited) {
        Model model = ModelFactory.createDefaultModel();
        if(subjects.size() == 0) {
            return model;
        }

        // prepare list
        List<Node> nodes = subjects.stream().map(FrontsNode::asNode).collect(Collectors.toList());

        // execute by batch (500 nodes)
        StreamUtils.batchStream(nodes, 1000).parallel().forEach(batch -> {

            // prepare query
            ConstructBuilder cb = constructBuilder.clone()
                .addValueVar("s", batch.toArray());

            // execute query
            Model subModel = queryExecution(cb.build()).execConstruct();

            // list objects to find all sub-resources
            Set<Resource> resList = subModel.listObjects()
                .filterKeep(RDFNode::isResource)
                .mapWith(RDFNode::asResource)
                .filterKeep(res -> res.getURI().startsWith(DatatourismeData.getURI()))
                .filterKeep(res -> {
                    synchronized (visited) {
                        if(!visited.contains(res)) {
                            visited.add(res);
                            return true;
                        }
                        return false;
                    }
                })
                .toSet();

            if(resList.size() > 0) {
                subModel.add(constructResourceHierarchy(resList, visited));
            }

            model.enterCriticalSection(Lock.WRITE);
            try {
                model.add(subModel);
            } finally {
                model.leaveCriticalSection();
            }
        });

        return model;
    }

    /**
     * Return all POI stats from a given fluxIdentifier/organizationIdentifier
     *
     * @return
     */
    public ResultSet selectPOIStats(int fluxIdentifier) {

        // prepare filter
        List<String> typesList = new ArrayList<>();
        typesList.add(FmtUtils.stringForNode(Datatourisme.EntertainmentAndEvent.asNode()));
        typesList.add(FmtUtils.stringForNode(Datatourisme.PlaceOfInterest.asNode()));
        typesList.add(FmtUtils.stringForNode(Datatourisme.Product.asNode()));
        typesList.add(FmtUtils.stringForNode(Datatourisme.Tour.asNode()));

        try {
            /*
            SELECT ?uri (SAMPLE(?id) as ?id) WHERE {
                ?uri a <https://www.datatourisme.gouv.fr/ontology/core#PointOfInterest> .
                ?uri <https://www.datatourisme.gouv.fr/resource/metadata#hasFluxIdentifier> "1"^^xsd:int.
                ?uri <http://purl.org/dc/elements/1.1/identifier> ?id.

            } GROUP BY ?uri
            */

            SelectBuilder sb = new SelectBuilder();
            //sb.setDistinct(true);

            sb.addVar("?uri");
            sb.addGroupBy("?uri");
            sb.addWhere("?uri", "a", Datatourisme.PointOfInterest);
            sb.addWhere("?uri", DatatourismeMetadata.hasFluxIdentifier, fluxIdentifier);

            // id
            sb.addVar("SAMPLE(?id)", "?id");
            sb.addWhere("?uri", DC.identifier, "?id");

            // principal type
            sb.addVar("SAMPLE(?type)", "?type");
            sb.addOptional(new SelectBuilder()
                .addWhere("?uri", RDF.type, "?type")
                .addFilter("?type IN (" + String.join(", ", typesList) + ")")
            );

            // secondary type
            sb.addVar("SAMPLE(?type2)", "?type2");
            sb.addOptional(new SelectBuilder()
                .addWhere("?uri", RDF.type, "?type2")
                .addWhere("?type2", RDFS.subClassOf, "?type")
            );

            // label
            sb.addVar("SAMPLE(?label)", "?label");
            sb.addOptional(new SelectBuilder()
                .addWhere("?uri", RDFS.label, "?label")
                //.addFilter("lang(?label) = 'fr'")
            );

            // city
            sb.addVar("SAMPLE(?city)", "?city");
            sb.addOptional(new SelectBuilder()
                .addWhere("?uri", Datatourisme.isLocatedAt, "?place")
                .addWhere("?place", Schema.address, "?address")
                .addWhere("?address", Datatourisme.hasAddressCity, "?c")
                .addWhere("?c", RDFS.label, "?city")
                .addFilter("lang(?city) = 'fr'")
            );

            // creator
            sb.addVar("SAMPLE(?creator)", "?creator");
            sb.addOptional(new SelectBuilder()
                .addWhere("?uri", Datatourisme.hasBeenCreatedBy, "?creator_agent")
                .addWhere("?creator_agent", Schema.legalName, "?creator")
            );

            return queryExecution(sb.build()).execSelect();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
