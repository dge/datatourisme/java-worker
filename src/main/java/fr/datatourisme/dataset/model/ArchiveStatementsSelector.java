/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset.model;

import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

/**
 * fr.datatourisme.dataset
 */
public class ArchiveStatementsSelector implements Selector {
    Resource subject;

    public ArchiveStatementsSelector(Resource subject) {
        this.subject = subject;
    }

    @Override
    public boolean isSimple() {
        return false;
    }

    @Override
    public Resource getSubject() {
        return this.subject;
    }

    @Override
    public Property getPredicate() {
        return null;
    }

    @Override
    public RDFNode getObject() {
        return null;
    }

    @Override
    public boolean test(Statement statement) {
        return statement.getPredicate().equals(RDF.type)
            || statement.getPredicate().equals(DC.identifier)
            || statement.getPredicate().equals(RDFS.label)
            || statement.getPredicate().equals(RDFS.comment);
    }
}
