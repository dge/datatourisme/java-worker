/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset.query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;

import java.util.Iterator;

/**
 * fr.datatourisme.dataset.query
 */
abstract public class AbstractQueryExecutionService {
    private final Log logger = LogFactory.getLog(this.getClass());

    abstract public QueryExecution queryExecution(Query query);

    abstract public UpdateProcessor updateExecution(UpdateRequest request);

    public QueryExecution queryExecution(String queryStr) {
        Query query = QueryFactory.create(queryStr) ;
        return queryExecution(query);
    }

    public UpdateProcessor updateExecution(String queryStr) {
        UpdateRequest request = UpdateFactory.create(queryStr) ;
        return updateExecution(request);
    }

    /**
     * SELECT
     *
     * @param query
     * @return
     */
    public ResultSet select(Query query) {
        logger.debug("SPARQL Select : " + query.serialize());
        return queryExecution(query).execSelect();
    }

    /**
     * CONSTRUCT
     *
     * @param query
     * @return
     */
    public Model construct(Query query, Model model) {
        logger.debug("SPARQL Construct : " + query.serialize());
        return queryExecution(query).execConstruct(model);
    }

    public Model construct(Query query) {
        logger.debug("SPARQL Construct : " + query.serialize());
        return queryExecution(query).execConstruct();
    }

    /**
     * CONSTRUCT TRIPLES
     *
     * @param query
     * @return
     */
    public Iterator<Triple> constructTriples(Query query) {
        logger.debug("SPARQL Construct : " + query.serialize());
        return queryExecution(query).execConstructTriples();
    }

    /**
     * ASK
     *
     * @param query
     * @return
     */
    public Boolean ask(Query query) {
        logger.debug("SPARQL Query : " + query);
        return queryExecution(query).execAsk();
    }
}
