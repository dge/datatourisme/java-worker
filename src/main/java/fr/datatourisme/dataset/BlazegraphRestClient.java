/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.jena.query.Query;
import org.apache.jena.update.UpdateRequest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 */
public class BlazegraphRestClient {

    /**
     * enpoint url
     */
    String endpoint;

    Pattern exceptionPattern = Pattern.compile("Exception:\\s((?:[^\\n](?!Exception))+)\\n\\s*at");

    /**
     * default timeout
     */
    int queryTimeout = 0;

    public BlazegraphRestClient(String endpoint) {
        this.endpoint = endpoint;
    }

    public void setQueryTimeout(int queryTimeout) {
        this.queryTimeout = queryTimeout;
    }

    /**
     * Launch a query
     *
     * @param query
     * @return
     * @throws IOException
     * @throws URISyntaxException
     */
    public InputStream query(Query query, String mimeType, int queryTimeout) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(endpoint);
        Request request = Request.Post(builder.build());
        request.bodyString(query.serialize(), ContentType.create("application/sparql-query", "UTF-8"));
        request.addHeader("Accept", mimeType);
        if(queryTimeout > 0) {
            request.addHeader("X-BIGDATA-MAX-QUERY-MILLIS", String.valueOf(queryTimeout));
        }

        HttpResponse response = request.execute().returnResponse();
        StatusLine statusLine = response.getStatusLine();
        HttpEntity entity = response.getEntity();
        if(statusLine.getStatusCode() >= 300) {
            throw new HttpResponseException(statusLine.getStatusCode(), extractExceptionMessage(response));
        }

        return entity == null ? null : entity.getContent();
    }

    /**
     * @param query
     * @param mimeType
     * @return
     * @throws IOException
     * @throws URISyntaxException
     */
    public InputStream query(Query query, String mimeType) throws IOException, URISyntaxException {
        return query(query, mimeType, queryTimeout);
    }

    /**
     * @param query
     * @return
     * @throws IOException
     * @throws URISyntaxException
     */
    public InputStream query(Query query) throws IOException, URISyntaxException {
        return query(query, "text/plain", queryTimeout);
    }

    /**
     * GETSTMTS (POST)
     */
    public CloseableHttpResponse getStmts() throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(endpoint);
        builder.addParameter("GETSTMTS", null);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(builder.build());
        return httpclient.execute(httpPost);
    }

    /**
     * SPARQL Update 1.1 Query
     *
     * @param query
     */
    public void update(UpdateRequest query) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(endpoint);
        Request request = Request.Post(builder.build());
        request.bodyString(query.toString(), ContentType.create("application/sparql-update", "UTF-8"));
        request.execute().handleResponse(response -> {
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() >= 300) {
                throw new HttpResponseException(statusLine.getStatusCode(), extractExceptionMessage(response));
            }
            return null;
        });
    }

    /**
     * UPDATE (POST with Multi-Part Request Body)
     */
    public void deleteInsert(File deleteFile, File insertFile) throws URISyntaxException, IOException {
        URIBuilder builder = new URIBuilder(endpoint);
        builder.setParameter("updatePost", null);
        Request request = Request.Post(builder.build());

        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.addBinaryBody("remove", deleteFile, ContentType.TEXT_PLAIN, "remove.nt");
        entityBuilder.addBinaryBody("add", insertFile, ContentType.TEXT_PLAIN, "add.nt");
        HttpEntity multipart = entityBuilder.build();
        request.body(multipart);

        request.execute().handleResponse(response -> {
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() >= 300) {
                throw new HttpResponseException(statusLine.getStatusCode(), extractExceptionMessage(response));
            }
            return null;
        });
    }

    /**
     * @param response
     *
     * @return
     */
    private String extractExceptionMessage(HttpResponse response) throws IOException {
        StatusLine statusLine = response.getStatusLine();
        HttpEntity entity = response.getEntity();
        String body = EntityUtils.toString(entity);
        Matcher matcher = exceptionPattern.matcher(body);
        if(matcher.find()) {
            return matcher.group(1);
        }
        return statusLine.getReasonPhrase();
    }
}
