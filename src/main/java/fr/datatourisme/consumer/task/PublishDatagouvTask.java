/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import com.mashape.unirest.http.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;
import fr.datatourisme.consumer.processor.FileFormat;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.task.ReportingTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Level;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * fr.datatourisme.consumer.task
 */
public class PublishDatagouvTask extends AbstractTask {
    private String apiKey;
    private String dataset;
    private int fluxIdentifier;

    static String DATA_GOUV_API_BASE_URL = "https://www.data.gouv.fr/api/1";

    @Override
    public String getMachineName() {
        return "publish_datagouv";
    }

    @Override
    protected String getLocalizedName() {
        return "Publication data.gouv.fr";
    }

    @Override
    protected ReportEntry execute() throws Exception {
        ReportEntry reportEntry = new ReportEntry();
        if(apiKey == null | dataset == null) {
            return reportEntry;
        }

        Storage outputStorage = storage.dir("output");
        File[] files = outputStorage.toFile().listFiles();
        if(files.length == 0) {
            throw new JobException("Aucun fichier de donnée à publier");
        }
        File outputFile = files[0];
        String extension = FilenameUtils.getExtension(outputFile.getAbsolutePath());
        String basename = "datatourisme-flux-" + fluxIdentifier;
        String filename = basename + "." + extension + ".gz";

        try {
            String rid = null;

            // search for existing resource
            HttpResponse<JsonNode> response = Unirest.get(DATA_GOUV_API_BASE_URL + "/datasets/" + dataset)
                .header("accept", "application/json")
                .header("X-API-KEY", apiKey)
                .asJson();

            JSONArray resources = response.getBody().getObject().getJSONArray("resources");
            for (int i = 0; i < resources.length(); i++) {
                JSONObject resource = resources.getJSONObject(i);
                String title = resource.getString("title");
                if(FilenameUtils.getBaseName(title.replace(".gz", "")).equals(basename)) {
                    rid = resource.getString("id");
                    break;
                }
            }

            // determine url to use
            String url = DATA_GOUV_API_BASE_URL + "/datasets/" + dataset;
            if(rid != null) {
                url += "/resources/" + rid + "/upload/";
            } else {
                url += "/upload/";
            }

            InputStream inputStream = new FileInputStream(outputFile);
            byte[] bytes = IOUtils.toByteArray(inputStream);
            FileFormat fileFormat = FileFormat.getByExtension(extension);
            ContentType contentType = ContentType.create(fileFormat.getMimeType());
            //ContentType contentType = ContentType.create(fileFormat.getMimeType());

            HttpResponse<String> test = new CustomHttpRequestWithBody(HttpMethod.POST, url)
                    .header("accept", "application/json")
                    .header("X-API-KEY", apiKey)
                    .field("file", bytes, contentType, filename)
                    .asString();

            if(response.getStatus() >= 300) {
                // error
                reportEntry.setLevel(Level.WARN).setMessage(response.getStatusText());
            } else {
                // success
                reportEntry.setLevel(Level.INFO);
            }
            //System.out.println(response.getRawBody());
        } catch (Exception e) {
            e.printStackTrace();
            reportEntry.setLevel(Level.WARN).exception(e);
        }

        return reportEntry;
    }

    public PublishDatagouvTask setApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public PublishDatagouvTask setDataset(String dataset) {
        this.dataset = dataset;
        return this;
    }

    public PublishDatagouvTask setFluxIdentifier(int fluxIdentifier) {
        this.fluxIdentifier = fluxIdentifier;
        return this;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setFluxIdentifier((Integer) payload.getRequired("flux"));
        Map<String, String> map = (Map) payload.get("datagouv").orElse(new HashMap<>());
        setApiKey(map.getOrDefault("api", null));
        setDataset(map.getOrDefault("dataset", null));
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return ReportingTask.class;
    }

    /**
     *
     */
    class CustomHttpRequestWithBody extends HttpRequestWithBody {
        public CustomHttpRequestWithBody(HttpMethod method, String url) {
            super(method, url);
        }

        public MultipartBody field(String name, byte[] bytes, ContentType contentType, String fileName) {
            MultipartBody body = (new MultipartBody(this)).field(name, bytes, contentType, fileName);
            this.body = body;
            return body;
        }

        public CustomHttpRequestWithBody header(String name, String value) {
            return (CustomHttpRequestWithBody)super.header(name, value);
        }
    }
}
