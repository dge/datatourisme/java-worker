/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import fr.datatourisme.consumer.processor.tuple.ExtractPOIUrisHandler;
import fr.datatourisme.consumer.processor.tuple.TupleResultWriterFactory;
import fr.datatourisme.consumer.processor.tuple.TupleResultWriterFactoryRegistry;
import fr.datatourisme.consumer.processor.tuple.TupleResultWriterWrapper;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.ThreadTimeoutException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.io.CountingOutputStream;
import org.openrdf.query.QueryResultHandlerException;
import org.openrdf.query.resultio.QueryResultParseException;
import org.openrdf.query.resultio.binary.BinaryQueryResultParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.zip.GZIPOutputStream;

/**
 * fr.datatourisme.consumer.task
 */
public class ProcessTupleResultTask extends AbstractProcessResultTask {

    @Override
    public String getMachineName() {
        return "process_tuple_result";
    }

    @Override
    protected String getLocalizedName() {
        return "Traitement du résultat";
    }

    @Override
    protected ReportEntry execute() throws Exception {
        ReportEntry.Duration duration = reportEntry.getDuration();
        duration.start();

        File resultFile = storage.file("_tuples.bin");
        if(!resultFile.exists()) {
            throw new JobException("Enable to find the query result file");
        }

        // #1 : get the poi list
        if(!partial) {
            logger.info("Localisation des POI");
            duration.startProcess("find_pois");
            BinaryQueryResultParser parser = new BinaryQueryResultParser();
            ExtractPOIUrisHandler extractHandler = new ExtractPOIUrisHandler();
            parser.setQueryResultHandler(extractHandler);
            parser.parse(new FileInputStream(resultFile));
            reportEntry.data("resources", extractHandler.getList());
            duration.end();
        }

        // #2 : serialize
        logger.info("Sérialisation des résultats");
        duration = reportEntry.getDuration().startProcess("serialization");
        executeSerialize(resultFile, format);
        duration.end();

        FileUtils.forceDelete(resultFile);

        return reportEntry;
    }

    /**
     * @param inputFile
     * @param format
     */
    public void executeSerialize(File inputFile, String format) throws IOException, InterruptedException, ExecutionException, ThreadTimeoutException, JobException, QueryResultParseException, QueryResultHandlerException {
        Storage inputDir = storage.dir("output");
        String type = partial ? "partial" : "complete";

        // prepare outputstream
        File temp = File.createTempFile("producer-export", ".tmp");
        FileOutputStream fileOutputStream = new FileOutputStream(temp, false);
        GZIPOutputStream outputStream = new GZIPOutputStream(fileOutputStream, true);
        CountingOutputStream output = new CountingOutputStream(outputStream);
        logger.debug("write to " + temp.getAbsolutePath());

        // prepare writer
        TupleResultWriterFactory writerFactory = TupleResultWriterFactoryRegistry.get(format);
        TupleResultWriterWrapper writer = new TupleResultWriterWrapper(writerFactory.create(output));
        writer.setLanguages(languages);

        // prepare parser
        BinaryQueryResultParser parser = new BinaryQueryResultParser();
        parser.setQueryResultHandler(writer);

        // go !
        parser.parseQueryResult(new FileInputStream(inputFile));

        // close streams
        outputStream.finish();
        output.close();

        // remove file from this type
        File[] files = inputDir.toFile().listFiles((d, name) -> name.startsWith("out." + type + "."));
        Arrays.stream(files).forEach(File::delete);

        // move the temp file
        String filename = "out." + (partial ? "partial" : "complete") + "." + writerFactory.getFormat().getFileExtension();
        File target = inputDir.file(filename);
        Files.move(temp.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);

        reportEntry.data("filename", filename)
            .data("mimetype", writerFactory.getFormat().getMimeType())
            .data("size", output.getCount());
    }
}
