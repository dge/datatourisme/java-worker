/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.PrefixMapping;
import com.conjecto.graphstore.Triplet;
import com.conjecto.graphstore.exception.GraphStoreException;
import com.conjecto.graphstore.exception.GraphStoreLockException;
import fr.datatourisme.bundle.ResourceBundle;
import fr.datatourisme.bundle.ResourceStore;
import fr.datatourisme.bundle.builder.ResourceBundleBuilder;
import fr.datatourisme.bundle.builder.ResourceBundleBuilderFactory;
import fr.datatourisme.consumer.processor.graph.FilteredGraphStore;
import fr.datatourisme.consumer.processor.graph.GraphResultSerializer;
import fr.datatourisme.consumer.processor.graph.GraphResultSerializerRegistry;
import fr.datatourisme.consumer.processor.graph.GraphStoreHierarchyCompleter;
import fr.datatourisme.dataset.GraphStoreManager;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.exception.ThreadTimeoutException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.io.CountingOutputStream;
import org.apache.jena.vocabulary.RDF;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.zip.GZIPOutputStream;

/**
 * fr.datatourisme.consumer.task
 */
public class ProcessGraphResultTask extends AbstractProcessResultTask {

    @Autowired
    GraphStoreManager datasetStoreManager;

    @Autowired
    ResourceBundleBuilderFactory resourceBundleBuilderFactory;

    @Autowired
    Ontology ontology;

    @Override
    public String getMachineName() {
        return "process_graph_result";
    }

    @Override
    protected String getLocalizedName() {
        return "Traitement du résultat";
    }

    @Override
    protected ReportEntry execute() throws Exception {

        reportEntry.getDuration().start();

        File resultFile = storage.dir("_graph", false).toFile();
        if(!resultFile.exists()) {
            throw new JobException("Enable to find the query result file");
        }

        // special case : new zip formats
        if (format.startsWith("bundle/")) {
            executeResourceBundle(resultFile, ResourceStore.Format.valueOf(format.substring(7)));
            FileUtils.forceDelete(resultFile);
            return reportEntry;
        }

        FilteredGraphStore store = FilteredGraphStore.open(resultFile.getPath());
        store.setLanguages(languages);
        try {

            // #1 : complete the model with whole hierarchy of <urn:resource>
            logger.info("Ajout des triplets complémentaires");
            ReportEntry.Duration duration = reportEntry.getDuration().startProcess("complete_hierarchy");
            executeCompleteStore(store);
            duration.end();

            // #2 : get the poi list
            if(!partial) {
                Set<String> uris = new HashSet<>();
                logger.info("Localisation des POI");
                duration = reportEntry.getDuration().startProcess("find_pois");
                Iterator<Triplet> iter = store.queryPOS("<"+ RDF.type.getURI() +">", "<"+ Datatourisme.PointOfInterest.getURI() +">");
                while(iter.hasNext()) {
                    uris.add(iter.next().getSubject().getLabel());
                }
                reportEntry.data("resources", uris);
                duration.end();
            }

            // #3 serialize !
            logger.info("Sérialisation du graphe");
            duration = reportEntry.getDuration().startProcess("serialization");
            executeSerialize(store, format);
            duration.end();

            FileUtils.forceDelete(resultFile);
        } finally {
            store.close();
        }

        return reportEntry;
    }

    /**
     *
     * @throws TemporaryJobException
     * @throws GraphStoreException
     */
    public void executeCompleteStore(GraphStore store) throws JobException, GraphStoreException, InterruptedException, ExecutionException, ThreadTimeoutException {
        GraphStore datasetStore;
        try {
            datasetStore = datasetStoreManager.open(30000); // do not open in read only, we need to use lock security !!
        } catch (GraphStoreLockException e) {
            throw new TemporaryJobException(e.getMessage());
        }
        try {
            Iterator<Triplet> iter = store.queryPOS("<"+ RDF.type.getURI() +">", "<urn:resource>");
            GraphStoreHierarchyCompleter completer = new GraphStoreHierarchyCompleter(store, datasetStore);
            completer.process(iter);
            store.compact();
        } finally {
            datasetStore.close();
        }
    }

    /**
     * @param store
     */
    public void executeSerialize(GraphStore store, String format) throws IOException, InterruptedException, ExecutionException, ThreadTimeoutException, JobException {
        Storage inputDir = storage.dir("output");
        String type = partial ? "partial" : "complete";

        // prepare outputstream
        File temp = File.createTempFile("producer-export", ".tmp");
        OutputStream fileOutputStream = new FileOutputStream(temp, false);
        GZIPOutputStream outputStream = new GZIPOutputStream(fileOutputStream, true);
        CountingOutputStream output = new CountingOutputStream(outputStream);
        logger.debug("write to " + temp.getAbsolutePath());

        // prepare serializer
        GraphResultSerializer serializer = GraphResultSerializerRegistry.get(format);
        PrefixMapping prefixMapping = (new PrefixMapping()).set(ontology.getPrefixMapping().getNsPrefixMap());

        // go !
        serializer.serialize(output, store, prefixMapping);

        // close streams
        outputStream.finish();
        output.close();

        // remove file from this type
        File[] files = inputDir.toFile().listFiles((d, name) -> name.startsWith("out." + type + "."));
        Arrays.stream(files).forEach(File::delete);

        // move the temp file
        String filename = "out." + type + "." + serializer.getFormat().getFileExtension();
        File target = inputDir.file(filename);
        Files.move(temp.toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);

        reportEntry.data("filename", filename)
            .data("mimetype", serializer.getFormat().getMimeType())
            .data("size", output.getCount());
    }

    /**
     * New format : list of POI files into a zip file
     *
     * @return
     */
    private void executeResourceBundle(File resultFile, ResourceStore.Format format) throws IOException, GraphStoreException {
        Storage inputDir = storage.dir("output");
        String type = partial ? "partial" : "complete";
        ResourceBundleBuilder builder = resourceBundleBuilderFactory.create(format);

        // add resources to the bundle
        GraphStore store = GraphStore.openReadOnly(resultFile.getPath());
        Iterator<Triplet> iter = store.queryPOS("<"+ RDF.type.getURI() +">", "<urn:resource>");
        while (iter.hasNext()) {
            String uri = iter.next().getSubject().getLabel();
            builder.add(uri);
        }
        store.close();

        // build the bundle file
        ResourceBundle bundle = builder.build();
        long size = bundle.getFile().length();

        // remove file from this type
        File[] files = inputDir.toFile().listFiles((d, name) -> name.startsWith("out." + type + "."));
        Arrays.stream(files).forEach(File::delete);

        // move the temp file
        String filename = "out." + type + ".zip";
        File target = inputDir.file(filename);
        bundle.moveToGzip(target.toPath());

        // build report entry
        reportEntry
            .data("filename", filename)
            .data("resources", bundle.getResources())
            .data("mimetype", "application/zip")
            .data("size", size);
    }
}
