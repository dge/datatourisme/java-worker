/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import com.conjecto.graphstore.GraphLoader;
import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.dataset.BlazegraphRestClient;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.ThreadTimeoutException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

/**
 * fr.datatourisme.consumer.task
 */
public class ExecuteQueryTask extends AbstractTask {
    private String queryStr;
    private Boolean partial = false;

    Class<? extends AbstractTask> nextTask;

    @Autowired
    BlazegraphRestClient datasetRestClient;

    @Override
    public String getMachineName() {
        return "execute_query";
    }

    @Override
    protected String getLocalizedName() {
        return "Exécution de la requête";
    }

    @Override
    protected ReportEntry execute() throws Exception {
        ReportEntry reportEntry = new ReportEntry();
        reportEntry.getDuration().start();

        // prepare result files
        File selectResultFile = storage.file("_tuples.bin");
        File constructResultFile = storage.dir("_graph", false).toFile();

        // clean old versions
        if(selectResultFile.exists()) {
            FileUtils.forceDelete(selectResultFile);
        }
        if(constructResultFile.exists()) {
            FileUtils.forceDelete(constructResultFile);
        }

        // parse de la requête
        Query query = QueryFactory.create(queryStr);
        if(query.isConstructType() || query.isDescribeType()) {

            logger.debug("Exécution de la requête CONSTRUCT");
            if(partial) {
                query.setLimit(10);
            }
            executeConstruct(query, constructResultFile);
            nextTask = ProcessGraphResultTask.class;

        } else if(query.isSelectType()) {

            if(partial) {
                query.setLimit(1000);
            }
            logger.debug("Exécution de la requête SELECT");
            executeSelect(query, selectResultFile);
            nextTask = ProcessTupleResultTask.class;

        } else {
            throw new IllegalArgumentException("Ce type de requête n'est pas supporté");
        }

        return reportEntry;
    }

    /**
     * Execute CONSTRUCT query and load the temp GraphStore
     *
     * @return
     */
    private void executeConstruct(Query query, File resultFile) throws IOException, URISyntaxException, GraphStoreException, InterruptedException, ExecutionException, ThreadTimeoutException {
        GraphStoreOptions options = (new GraphStoreOptions()).setCreateIfMissing(true);
        GraphStore store = GraphStore.open(resultFile.getPath(), options);
        try {
            GraphLoader loader = new GraphLoader(store, "nt");
            InputStream inputStream = datasetRestClient.query(query, "text/plain");
            loader.load(inputStream);
            store.compact();
            inputStream.close();
        } finally {
            store.close();
        }
    }

    /**
     * Execute SELECT query
     *
     * @return
     */
    private void executeSelect(Query query, File resultFile) throws IOException, URISyntaxException, InterruptedException, ExecutionException, ThreadTimeoutException {
        InputStream inputStream = datasetRestClient.query(query, "application/x-binary-rdf-results-table");
        OutputStream outputStream = new FileOutputStream(resultFile);
        IOUtils.copy(inputStream, outputStream);
        inputStream.close();
        outputStream.close();
    }

    public ExecuteQueryTask setQuery(String queryStr) {
        this.queryStr = queryStr;
        return this;
    }

    public ExecuteQueryTask setPartial(Boolean partial) {
        this.partial = partial;
        return this;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setQuery(payload.getRequired("query").toString());
        setPartial((Boolean) payload.get("partial").orElse(false));
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return nextTask;
    }
}
