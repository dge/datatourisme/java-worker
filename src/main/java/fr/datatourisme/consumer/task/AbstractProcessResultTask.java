/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * fr.datatourisme.consumer.task
 */
abstract public class AbstractProcessResultTask extends AbstractTask {
    protected Boolean partial = false;
    protected String format;
    protected Set<String> languages = new HashSet<>();

    ReportEntry reportEntry = new ReportEntry();

    public void setPartial(Boolean partial) {
        this.partial = partial;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setPartial((Boolean) payload.get("partial").orElse(false));
        setFormat(payload.getRequired("format").toString());
        List<String> languages = (List<String>)payload.get("languages").orElse(null);
        if(languages != null) {
            setLanguages(new HashSet<>(languages));
        }
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return PublishDatagouvTask.class;
    }
}
