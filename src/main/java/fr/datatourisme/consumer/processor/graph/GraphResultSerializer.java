/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.PrefixMapping;
import com.conjecto.graphstore.Serializer;
import fr.datatourisme.consumer.processor.FileFormat;

import java.io.IOException;
import java.io.OutputStream;

/**
 * fr.datatourisme.consumer.processor.graph
 */
public class GraphResultSerializer {
    private FileFormat format;
    private Serializer serializer;

    public GraphResultSerializer(FileFormat format, Serializer serializer) {
        this.format = format;
        this.serializer = serializer;
    }

    public Serializer getSerializer() {
        return serializer;
    }

    public FileFormat getFormat() {
        return format;
    }

    public void serialize(OutputStream output, GraphStore store, PrefixMapping prefixMapping) throws IOException {
        serializer.serialize(output, store, prefixMapping);
    }
}
