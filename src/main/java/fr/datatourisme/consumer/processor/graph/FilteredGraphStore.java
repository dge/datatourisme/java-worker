/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.conjecto.graphstore.Triplet;
import com.conjecto.graphstore.TripletIterator;
import com.conjecto.graphstore.exception.GraphStoreException;
import org.rocksdb.ColumnFamilyHandle;
import org.rocksdb.RocksIterator;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Node;

import java.util.Set;

/**
 * A special GraphStore to filter triplets during query. As language.
 */
public class FilteredGraphStore extends GraphStore  {

    protected Set<String> languages;

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public FilteredGraphStore(String dbDir, GraphStoreOptions options, Boolean readOnly) throws GraphStoreException {
        super(dbDir, options, readOnly);
    }

    public static FilteredGraphStore open(String dbDir, GraphStoreOptions options) throws GraphStoreException {
        return new FilteredGraphStore(dbDir, options, false);
    }

    public static FilteredGraphStore open(String dbDir) throws GraphStoreException {
        GraphStoreOptions options = new GraphStoreOptions();
        return open(dbDir, options);
    }

    @Override
    protected TripletIterator query(ColumnFamilyHandle cfHandle, String key1, String key2, String key3) {
        String prefix = "";
        if(key1 != null) {
            prefix += key1;
        }
        if(key2 != null) {
            prefix += key2;
        }
        if(key3 != null) {
            prefix += key3;
        }

        if(languages != null && languages.size() > 0) {
            return new FilteredTripletIterator(db.newIterator(cfHandle), prefix);
        } else {
            return new TripletIterator(db.newIterator(cfHandle), prefix);
        }
    }

    /**
     * Custom triplet iterator
     */
    class FilteredTripletIterator extends TripletIterator {

        FilteredTripletIterator(RocksIterator iterator, String prefix) {
            super(iterator, prefix);
        }

        @Override
        public boolean hasNext() {
            while (super.hasNext()) {
                Triplet triplet = Triplet.parse(new String(iterator.value()));
                if(isValid(triplet)) {
                    return true;
                } else {
                    iterator.next();
                }
            }
            return false;
        }

        /**
         * @param triplet
         *
         * @return
         */
        private boolean isValid(Triplet triplet) {
            Node object = triplet.getObject();
            if(object instanceof Literal && ((Literal) object).getLanguageTag() != null) {
               return languages.contains(((Literal) object).getLanguageTag());
            }
            return true;
        }
    }
}
