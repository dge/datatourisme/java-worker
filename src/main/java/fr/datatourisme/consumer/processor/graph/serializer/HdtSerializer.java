/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph.serializer;

import com.conjecto.graphstore.*;
import fr.datatourisme.vocabulary.Datatourisme;
import org.rdfhdt.hdt.enums.ResultEstimationType;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManagerImpl;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdt.triples.IteratorTripleString;
import org.rdfhdt.hdt.triples.TripleString;
import org.semanticweb.yars.nx.Literal;

import java.io.IOException;
import java.io.OutputStream;

/**
 * fr.datatourisme.consumer.processor.graph.serializer
 */
public class HdtSerializer implements Serializer {

    @Override
    public void serialize(OutputStream out, GraphStore store, PrefixMapping prefixMapping) throws IOException {
        HDTManagerImpl hdtManager = new HDTManagerImpl();
        GraphStoreIteratorTripleString iteratorTripleString = new GraphStoreIteratorTripleString(store.querySPO());
        HDT hdt = hdtManager.doGenerateHDT(iteratorTripleString, Datatourisme.getURI(), new HDTSpecification(), null);
        // Save generated HDT to a file
        hdt.saveToHDT(out, null);
        out.flush();
    }

    /**
     * Implements IteratorTripleString
     */
    class GraphStoreIteratorTripleString implements IteratorTripleString {

        private TripletIterator iterator;

        public GraphStoreIteratorTripleString(TripletIterator iterator) {
            this.iterator = iterator;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public TripleString next() {
            Triplet triplet = iterator.next();
            TripleString tripleString = new TripleString();
            tripleString.setSubject(triplet.getSubject().getLabel());
            tripleString.setPredicate(triplet.getPredicate().getLabel());
            tripleString.setObject(triplet.getObject() instanceof Literal ? triplet.getObject().toString() : triplet.getObject().getLabel());
            return tripleString;
        }

        @Override
        public void goToStart() {

        }

        @Override
        public long estimatedNumResults() {
            return 0;
        }

        @Override
        public ResultEstimationType numResultEstimation() {
            return null;
        }
    }
}
