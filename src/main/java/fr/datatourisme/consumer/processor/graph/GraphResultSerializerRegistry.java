/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph;

import com.conjecto.graphstore.serializer.*;
import fr.datatourisme.consumer.processor.FileFormat;
import fr.datatourisme.consumer.processor.graph.serializer.HdtSerializer;
import fr.datatourisme.vocabulary.Datatourisme;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * fr.datatourisme.consumer.processor.graph
 */
public class GraphResultSerializerRegistry {
    private static Map<String, GraphResultSerializer> serializers = new LinkedHashMap<>();

    // list supported format
    static {
        register("xml", new GraphResultSerializer(FileFormat.RDFXML, new XmlSerializer()));
        register("ttl", new GraphResultSerializer(FileFormat.TTL, new TurtleSerializer()));
        register("nt", new GraphResultSerializer(FileFormat.NT, new NTriplesSerializer()));
        register("csv", new GraphResultSerializer(FileFormat.CSV, new CsvSerializer()));
        register("hdt", new GraphResultSerializer(FileFormat.HDT, new HdtSerializer()) );

        // JSON LD
        Map<String, String> jsonLdContext = new HashMap<>();
        jsonLdContext.put("@vocab", Datatourisme.getURI());
        register("jsonld", new GraphResultSerializer(FileFormat.JSONLD, new JsonLDSerializer().setContext(jsonLdContext)));
        register("jsonld-frame", new GraphResultSerializer(FileFormat.JSONLD, new JsonLDFrameSerializer().setContext(jsonLdContext)));
    }

    private static void register(String name, GraphResultSerializer writer) {
        serializers.put(name, writer);
    }

    public static GraphResultSerializer get(String name) throws IndexOutOfBoundsException {
        if(!serializers.containsKey(name)) {
            throw new IndexOutOfBoundsException("The format " + name + " is not registered");
        }
        return serializers.get(name);
    }
}
