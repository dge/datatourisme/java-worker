/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * fr.datatourisme.consumer.processor
 */
public class FileFormat {

    private static List<FileFormat> fileFormats = new ArrayList<>();

    public static FileFormat ZIP = new FileFormat("zip", "application/zip", StandardCharsets.UTF_8);

    public static FileFormat JSON = new FileFormat("json", "application/json", StandardCharsets.UTF_8);

    public static FileFormat SRX = new FileFormat("srx", "application/sparql-results+xml", StandardCharsets.UTF_8);
    public static FileFormat CSV = new FileFormat("csv", "text/csv", StandardCharsets.UTF_8);
    public static FileFormat TSV = new FileFormat("tsv", "text/tab-separated-values", StandardCharsets.UTF_8);
    public static FileFormat SRJ = new FileFormat("srj", "application/sparql-results+json", StandardCharsets.UTF_8);

    public static FileFormat RDFXML = new FileFormat("rdf", "application/rdf+xml", StandardCharsets.UTF_8);
    public static FileFormat NT = new FileFormat("nt", "text/plain", StandardCharsets.UTF_8);
    public static FileFormat TTL = new FileFormat("ttl", "application/x-turtle", StandardCharsets.UTF_8);
    public static FileFormat JSONLD = new FileFormat("jsonld", "application/ld+json", StandardCharsets.UTF_8);
    public static FileFormat HDT = new FileFormat("hdt", "application/vnd.hdt", StandardCharsets.UTF_8);

    
    private final String fileExtension;
    private final String mimeType;
    private final Charset charset;

    public FileFormat(String fileExtension, String mimeType, Charset charset) {
        assert mimeType != null : "mimeType must not be null";
        this.fileExtension = fileExtension;
        this.mimeType = mimeType;
        this.charset = charset;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Charset getCharset() {
        return charset;
    }

    static public void register(FileFormat fileFormat) {
        fileFormats.add(fileFormat);
    }

    static public FileFormat getByExtension(String fileExtension) {
        for (FileFormat fileFormat : fileFormats) {
            if (fileFormat.getFileExtension().equals(fileExtension)) {
                return fileFormat;
            }
        }
        return null;
    }

    static {
        register(ZIP);
        register(JSON);

        register(SRX);
        register(CSV);
        register(TSV);
        register(SRJ);

        register(RDFXML);
        register(NT);
        register(TTL);
        register(JSONLD);
        register(HDT);
    }
}
