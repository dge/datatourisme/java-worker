/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.tuple;

import fr.datatourisme.vocabulary.DatatourismeData;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * fr.datatourisme.consumer.processor.tuple
 */
public class ExtractPOIUrisHandler implements TupleQueryResultHandler {
    Set<String> list = new HashSet<>();

    public Set<String> getList() {
        return list;
    }

    @Override
    public void handleBoolean(boolean value) throws QueryResultHandlerException {

    }

    @Override
    public void handleLinks(List<String> linkUrls) throws QueryResultHandlerException {

    }

    @Override
    public void startQueryResult(List<String> bindingNames) throws TupleQueryResultHandlerException {

    }

    @Override
    public void endQueryResult() throws TupleQueryResultHandlerException {

    }

    @Override
    public void handleSolution(BindingSet bindingSet) throws TupleQueryResultHandlerException {
        Iterator<Binding> iter = bindingSet.iterator();
        while(iter.hasNext()) {
            Value value = iter.next().getValue();
            if(value instanceof URI
                && value.stringValue().startsWith(DatatourismeData.getURI())
                && value.stringValue().split("/").length > 4) { // ex : https://data.datatourisme.gouv.fr/15/9eb7ce65-393a-30d2-8540-0c52ef4c0720
                    list.add(value.stringValue());
            }
        }
    }
}
