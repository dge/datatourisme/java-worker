/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.tuple;

import fr.datatourisme.consumer.processor.FileFormat;
import org.openrdf.query.resultio.TupleQueryResultWriter;

import java.io.OutputStream;

/**
 * fr.datatourisme.consumer.processor.graph
 */
public class TupleResultWriterFactory {
    private FileFormat format;
    private Class<? extends TupleQueryResultWriter> writerClass;

    public TupleResultWriterFactory(FileFormat format, Class<? extends TupleQueryResultWriter> writerClass) {
        this.format = format;
        this.writerClass = writerClass;
    }

    public FileFormat getFormat() {
        return format;
    }

    public TupleQueryResultWriter create(OutputStream output){
        try {
            return writerClass.getConstructor(OutputStream.class).newInstance(output);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
