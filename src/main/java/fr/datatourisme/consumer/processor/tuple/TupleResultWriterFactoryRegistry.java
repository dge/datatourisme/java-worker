/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.tuple;

import fr.datatourisme.consumer.processor.FileFormat;
import org.openrdf.query.resultio.sparqljson.SPARQLResultsJSONWriter;
import org.openrdf.query.resultio.sparqlxml.SPARQLResultsXMLWriter;
import org.openrdf.query.resultio.text.csv.SPARQLResultsCSVWriter;
import org.openrdf.query.resultio.text.tsv.SPARQLResultsTSVWriter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * fr.datatourisme.consumer.processor.graph
 */
public class TupleResultWriterFactoryRegistry {
    private static Map<String, TupleResultWriterFactory> serializers = new LinkedHashMap<>();

    // list supported format
    static {
        register("srx", new TupleResultWriterFactory(FileFormat.SRX, SPARQLResultsXMLWriter.class));
        register("csv", new TupleResultWriterFactory(FileFormat.CSV, SPARQLResultsCSVWriter.class));
        register("tsv", new TupleResultWriterFactory(FileFormat.TSV, SPARQLResultsTSVWriter.class));
        register("srj", new TupleResultWriterFactory(FileFormat.SRJ, SPARQLResultsJSONWriter.class));
    }

    private static void register(String name, TupleResultWriterFactory writer) {
        serializers.put(name, writer);
    }

    public static TupleResultWriterFactory get(String name) throws IndexOutOfBoundsException {
        if(!serializers.containsKey(name)) {
            throw new IndexOutOfBoundsException("The format " + name + " is not registered");
        }
        return serializers.get(name);
    }
}
