/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.api.controller;

import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.consumer.processor.FileFormat;
import fr.datatourisme.worker.Storage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
@RequestMapping("/consumer/{fluxId}")
public class FluxController extends AbstractController {

    @Autowired
    @Qualifier("consumer.storage")
    Storage consumerStorage;

    @GetMapping("/download/{type}")
    public ResponseEntity<StreamingResponseBody> getDownload(@PathVariable Integer fluxId, @PathVariable String type) throws IOException {
        Storage outputStorage = consumerStorage.dir(fluxId).dir("output");
        File[] files = outputStorage.toFile().listFiles((d, name) -> name.startsWith("out." + type + "."));
        if(files.length == 0) {
            return errorAsStream("Le contenu du flux n'est pas encore disponible.", HttpStatus.SERVICE_UNAVAILABLE);
        }

        File outputFile = files[0];
        String extension = FilenameUtils.getExtension(outputFile.getAbsolutePath());
        FileFormat fileFormat = FileFormat.getByExtension(extension);
        if(fileFormat == null) {
            return errorAsStream("Le format du flux n'est pas reconnu.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String filename = outputFile.getName();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(fileFormat.getMimeType() + ";charset=UTF-8"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("no-cache, post-check=0, pre-check=0");
        headers.setLastModified(outputFile.lastModified());
        headers.setContentLength(outputFile.length());
        // do not pass gzip, webapp-consumer will handle that
        // headers.set("Content-Encoding", "gzip");

        FileInputStream inputStream = new FileInputStream(outputFile);
        StreamingResponseBody stream = outputStream -> {
            int numberOfBytesToWrite;
            byte[] data = new byte[1024];
            while ((numberOfBytesToWrite = inputStream.read(data, 0, data.length)) != -1) {
                outputStream.write(data, 0, numberOfBytesToWrite);
            }
            inputStream.close();
        };

        return ResponseEntity.ok().headers(headers).body(stream);
    }

    @DeleteMapping()
    @ResponseBody
    public ResponseEntity<?> delete(@PathVariable Integer fluxId) throws IOException {
        Storage storage = consumerStorage.dir(fluxId);
        FileUtils.deleteDirectory(storage.toFile());
        return response("ok");
    }
}