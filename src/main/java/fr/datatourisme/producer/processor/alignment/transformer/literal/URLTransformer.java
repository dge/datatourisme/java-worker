/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;
import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;
import io.mola.galimatias.GalimatiasParseException;
import io.mola.galimatias.URL;
import org.apache.commons.validator.routines.UrlValidator;

/**
 * Transform an url
 */
public class URLTransformer extends AbstractTransformer<String> {

    @Override
    public String transform(Object value) throws TransformException {
        try {
            String url = value.toString();
            if (!url.toLowerCase().matches("^\\w+://.*")) {
                url = "http://" + url;
            }
            url = URL.parse(url).toString();

            String[] schemes = {"http","https"};
            UrlValidator urlValidator = new UrlValidator(schemes);
            if (!urlValidator.isValid(url)) {
                throw new TransformException(value, "adresse URL");
            }

            return URL.parse(url).toString();
        } catch (GalimatiasParseException e) {
            throw new TransformException(value, "adresse URL", e);
        }
    }
}
