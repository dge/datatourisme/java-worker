/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DC;

import java.util.Map;

public class POIReferenceRule extends AbstractRule {
    @Override
    public void doProcess(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {
       checkFunctional(evaluate(ctx), resource).forEach(identifier -> {
            Model model = resource.getModel();
            Resource anon = model.createResource();
            anon.addProperty(DC.identifier, identifier);
            Resource res = alignment.getUriGenerator().identify(anon);
            model.add(resource, model.createProperty(property.getURI()), res);
        });
    }
}
