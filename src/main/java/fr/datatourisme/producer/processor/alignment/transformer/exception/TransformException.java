/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.exception;

import org.apache.commons.lang3.StringUtils;

public class TransformException extends Exception {
    public TransformException() {
    }

    public TransformException(Object value, String expected, Throwable throwable) {
        super("Impossible de convertir la valeur \"" + StringUtils.abbreviate(value.toString(), 100) + (expected != null ? "\" en " + expected : "\""), throwable);
    }

    public TransformException(Object value, String expected) {
        this(value, expected, null);
    }

    public TransformException(String s) {
        super(s);
    }

    public TransformException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TransformException(Throwable throwable) {
        super(throwable);
    }

    public TransformException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
