/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;
import org.pojava.datetime.DateTime;
import org.pojava.datetime.DateTimeConfig;
import org.pojava.datetime.DateTimeConfigBuilder;
import org.pojava.datetime.IDateTimeConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateTransformer extends AbstractTransformer<String> {
    IDateTimeConfig dateTimeConfig;
    static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public DateTransformer() {
        DateTimeConfigBuilder builder = DateTimeConfigBuilder.newInstance();
        builder.setDmyOrder(true);
        builder.setInputTimeZone(TimeZone.getTimeZone("Europe/Paris")); // Used by parser
        builder.setOutputTimeZone(TimeZone.getTimeZone("Europe/Paris")); // Used by formatter
        dateTimeConfig = DateTimeConfig.fromBuilder(builder);
    }

    @Override
    public String transform(Object value) throws TransformException {
        try {
            DateTime dt = new DateTime(value.toString(), dateTimeConfig);
            return dateFormat.format(dt.toDate());
        } catch(Exception e) {
            throw new TransformException(value, "date", e);
        }
    }
}
