/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.reasoner.ValidationReasoner;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

abstract public class AbstractRule {
    protected Alignment alignment;
    protected OntologyProperty property;
    protected AtomicChain atomicChain;

    public enum Type {
        constant,
        single,
        xpath,
        xquery
    }

    protected Type type;

    protected Object value;

    protected String expr;

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public OntologyProperty getProperty() {
        return property;
    }

    public void setProperty(OntologyProperty property) {
        this.property = property;
    }

    public AtomicChain getAtomicChain() {
        return atomicChain;
    }

    public void setAtomicChain(AtomicChain atomicChain) {
        this.atomicChain = atomicChain;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getExpr() {
        return expr;
    }

    public void setExpr(String expr) {
        this.expr = expr;
    }

    /**
     * Process the given resource with the given XmlContext
     *
     * @param ctx
     * @param resource
     */
    public abstract void doProcess(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException;

    /**
     * Process the given resource with the given XmlContext
     *
     * @param ctx
     * @param resource
     */
    public void process(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {
        try {
            doProcess(ctx, i18nCtx, resource);
        } catch (QueryExpressionException e) {
            addWarn(resource, "Erreur d'extraction : " + e.getMessage());
        }
    }

    /**
     * Process the given resource with the given XmlContext
     *
     * @param ctx
     * @param resource
     */
    public void process(XmlContext ctx, Resource resource) throws QueryExpressionException {
        process(ctx, null, resource);
    }

    /**
     * @param stream
     * @param <T>
     * @return
     */
    public <T> Stream<T> checkFunctional(Stream<T> stream, Resource resource) {
        if(property.isFunctional()) {
            List<T> list = stream.collect(Collectors.toList());
            if(list.size() > 1) {
                addWarn(resource, "La propriété \"" + property.getLabel() + "\" ne peut pas avoir plusieurs valeurs");
            }
            // limit the stream to the first element
            return list.stream().limit(1);
        }
        return stream;
    }

    /**
     * Make a stream with values
     * values can be a unique item or a collection
     *
     * @param values
     * @return
     */
    static Stream<String> streamOfValues(Object values) {
        return Stream.of(values)
            .flatMap(o -> {
                if(o instanceof Collection) {
                    return ((List) o).stream();
                }
                return Stream.of(o);
            })
            .map(Object::toString);
    }

    /**
     * Apply the atomic chain to the given stream
     *
     * @param stream
     * @return
     */
    Stream<String> applyAtomicChain(Stream<String> stream) {
        if(atomicChain == null) {
            return (new AtomicChain()).processStream(stream);
        }
        return atomicChain.processStream(stream);
    }

    /**
     * Evaluate current rule on a given context
     *
     * @param ctx
     * @return
     */
    public Stream<String> evaluate(XmlContext ctx, Resource resource) throws QueryExpressionException {
        if(type == Type.constant && value != null) {
            return streamOfValues(value);
        } else if((type == Type.xpath || type == Type.xquery) && expr != null) {
            return applyAtomicChain(ctx.evaluateValues(expr));
        }
        return Stream.empty();
    }

    /**
     * Evaluate current rule on a given context
     *
     * @param ctx
     * @return
     */
    public Stream<String> evaluate(XmlContext ctx) throws QueryExpressionException {
        return evaluate(ctx, null);
    }

    /**
     * Add a violation to a given resource
     */
    protected void addViolation(Resource resource, ValidationReasoner.ViolationNature nature, String message) {
        Property property = ResourceFactory.createProperty(getProperty().getURI());
        if(resource != null) {
            ValidationReasoner.addViolation(resource, property, nature, message);
        }
    }

    /**
     * Add an error violation to a given resource
     *
     * @param message
     */
    protected void addError(Resource resource, String message) {
        addViolation(resource, ValidationReasoner.ViolationNature.error, message);
    }

    /**
     * Add a warning violation to a given resource
     *
     * @param message
     */
    protected void addWarn(Resource resource, String message) {
        addViolation(resource, ValidationReasoner.ViolationNature.warn, message);
    }
}
