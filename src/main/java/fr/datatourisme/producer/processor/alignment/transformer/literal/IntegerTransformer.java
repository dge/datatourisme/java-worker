/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;

public class IntegerTransformer extends AbstractTransformer<Integer> {

    @Override
    public Integer transform(Object value) throws TransformException {
        try {
            return Integer.parseInt(value.toString());
        } catch(Exception e) {
            throw new TransformException(value, "nombre entier", e);
        }
    }
}
