/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;

import java.math.BigDecimal;

public class DecimalTransformer extends AbstractTransformer<String> {

    @Override
    public String transform(Object value) throws TransformException {
        try {
            return (new BigDecimal(value.toString().replace(",", "."))).toString();
        } catch(Exception e) {
            throw new TransformException(value, "nombre décimal", e);
        }
    }
}
