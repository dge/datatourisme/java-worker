/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.OntologyResource;
import fr.datatourisme.producer.processor.reasoner.ValidationReasoner;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class POIClassRule extends ThesaurusRule {

    /**
     * @param values
     * @return
     */
    protected Stream<String> evaluateConstants(Stream<String> values) {
        return values
            .map(uri -> alignment.getOntology().getClass(uri))
            .filter(ontClass -> (ontClass != null && ontClass.hasSuperClass(Datatourisme.PointOfInterest)))
            .map(OntologyResource::getURI);
    }

    /**
     * @param values
     * @return
     */
    protected Stream<String> evaluateExtractedValues(Stream<String> values, Resource resource) {
        // extract values
        List<String> list = values.collect(Collectors.toList());

        Set<String> set = list.stream()
            .flatMap(value -> alignment.getThesaurusAlignment().resolve(value, Datatourisme.PointOfInterest).stream())
            .map(OntologyResource::getURI)
            .collect(Collectors.toSet());

        if(set.size() == 0) {
            list.forEach(value -> {
                addViolation(resource, ValidationReasoner.ViolationNature.warn,"La valeur \"" + StringUtils.abbreviate(value, 100) + "\" n'a pas de correspondance dans le thésaurus \"Type de POI\"");
            });
            // add default value : PointOfInterest
            set.add(alignment.getOntology().getClass(Datatourisme.PointOfInterest).getURI());
        }

        return set.stream();
    }
}
