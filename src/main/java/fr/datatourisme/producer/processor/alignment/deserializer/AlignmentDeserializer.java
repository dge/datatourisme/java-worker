/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.deserializer;

import com.google.gson.*;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.ontology.OntologyResource;
import fr.datatourisme.producer.processor.URIGenerator;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.rule.*;
import fr.datatourisme.producer.processor.alignment.transformer.LiteralTransformer;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class AlignmentDeserializer implements JsonDeserializer<Alignment> {
    private Ontology ontology;
    private ThesaurusAlignment thesaurusAlignment;

    public AlignmentDeserializer(Ontology ontology, ThesaurusAlignment thesaurusAlignment) {
        this.ontology = ontology;
        this.thesaurusAlignment = thesaurusAlignment;
    }

    @Override
    public Alignment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
    {
        JsonObject root = json.getAsJsonObject();
        return new AlignmentDeserializerBuilder(root, context).create();
    }

    /**
     * AlignmentDeserializerBuilder
     */
    private class AlignmentDeserializerBuilder {
        PrefixMapping prefixMapping = PrefixMapping.Factory.create().setNsPrefixes(ontology.getPrefixMapping());
        JsonDeserializationContext context;
        JsonObject root;

        /**
         * @param root
         */
        public AlignmentDeserializerBuilder(JsonObject root, JsonDeserializationContext context) {

            // populare prefix mapping
            JsonElement _context = root.get("@context");
            if(_context != null) {
                _context.getAsJsonObject().entrySet().forEach(es -> {
                    String prefix;
                    while((prefix = prefixMapping.getNsURIPrefix(es.getValue().getAsString())) != null) {
                        prefixMapping.removeNsPrefix(prefix);
                    }
                    prefixMapping.setNsPrefix(es.getKey(), es.getValue().getAsString());
                });
                root.remove("@context");
            }

            this.root = root;
            this.context = context;
        }

        /**
         * Create alignment
         *
         * @return
         */
        public Alignment create() {
            Alignment alignment = new Alignment(ontology, thesaurusAlignment, new URIGenerator());

            // set xpath
            if(root.has("contextExpr")) {
                alignment.setContextExpr(root.get("contextExpr").getAsString());
            }

            // rules
            if(root.has("rules")) {
                JsonObject rules = root.get("rules").getAsJsonObject();
                OntologyClass ontClass = ontology.getClass(Datatourisme.PointOfInterest);
                alignment.setRules(buildRules(rules, new HashSet<>(Collections.singletonList(ontClass)), alignment));
            }

            return alignment;
        }

        /**
         * Build a set of rules
         *
         * @return
         */
        public Set<AbstractRule> buildRules(JsonObject rules, Set<OntologyClass> rangeClasses, Alignment alignment) {
            Set<AbstractRule> ruleSet = new HashSet<>();

            Set<OntologyProperty> properties = rangeClasses.stream()
                .flatMap(OntologyClass::listCandidateProperties)
                .collect(Collectors.toSet());

            BiConsumer<OntologyProperty, JsonElement> addRule = (property, definition) -> {
                AbstractRule rule = buildRule(property, definition, alignment);
                if(rule != null) {
                    ruleSet.add(rule);
                }
            };

            for(OntologyProperty property: properties) {
                String propUri = prefixMapping.shortForm(ontology.expandPrefix(property.getURI()));
                JsonElement definition = rules.get(propUri);
                if(definition != null) {
                    if(definition.isJsonArray()) {
                        definition.getAsJsonArray().iterator().forEachRemaining(def -> addRule.accept(property, def));
                    } else {
                        addRule.accept(property, definition);
                    }
                }
            }

            return ruleSet;
        }

        /**
         * Build  single rule
         *
         * @return
         */
        AbstractRule buildRule(OntologyProperty property, JsonElement definition, Alignment alignment) {
            AbstractRule rule;
            OntologyResource res = property.getRange();

            // test language
            if(property.isDatatype()) {
                Boolean isLangString = (res != null) && res.hasURI(RDF.langString.getURI());
                if(!isLangString && definition.getAsJsonObject().has("lang")
                        && !definition.getAsJsonObject().get("lang").getAsString().equals(LiteralTransformer.defaultLang)) {
                    // if not langString and has a custom lang, ignore it
                    return null;
                }
            }

            if(res instanceof OntologyClass && res.hasURI(Datatourisme.PointOfInterestClass.getURI())) {
                rule = context.deserialize(definition, POIClassRule.class);
            } else if(res instanceof OntologyClass && ((OntologyClass) res).hasSuperClass(OWL2.NamedIndividual.getURI())) {
                rule = context.deserialize(definition, ThesaurusRule.class);
            } else if(property.isDatatype()) {
                rule = context.deserialize(definition, LiteralRule.class);
            } else if(res instanceof OntologyClass && ((OntologyClass) res).hasSuperClass(Datatourisme.PointOfInterest.getURI())) {
                rule = context.deserialize(definition, POIReferenceRule.class);
            } else {
                rule = context.deserialize(definition, ResourceRule.class);
            }

            // set some properties
            rule.setProperty(property);
            rule.setAlignment(alignment);

            // add atomic chain
            JsonElement atomic = definition.getAsJsonObject().get("atomic");
            if(atomic != null) {
                try {
                    rule.setAtomicChain(AtomicChain.fromJson(atomic.toString()));
                } catch (JsonSyntaxException ignored) {}
            }

            // handle resource rules
            if(rule instanceof ResourceRule && definition.getAsJsonObject().has("rules")) {
                JsonObject nextRules =  definition.getAsJsonObject().getAsJsonObject("rules");
                Set<OntologyClass> nextRangeClasses = property.listRanges().collect(Collectors.toSet());
                ((ResourceRule) rule).setRules(buildRules(nextRules, nextRangeClasses, alignment));
            }

            return rule;
        }
    }

}
