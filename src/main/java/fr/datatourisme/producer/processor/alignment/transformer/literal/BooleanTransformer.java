/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

public class BooleanTransformer extends AbstractTransformer<Boolean> {

    @Override
    public Boolean transform(Object value) {
        String string = value.toString();
        return string.equalsIgnoreCase("true") ||
            string.equalsIgnoreCase("oui") ||
            string.equalsIgnoreCase("vrai");
    }
}
