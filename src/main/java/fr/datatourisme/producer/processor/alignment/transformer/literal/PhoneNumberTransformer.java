/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;

/**
 * Transform a phone number to intl format (+33 2 99 00 00 00)
 */
public class PhoneNumberTransformer extends AbstractTransformer<String> {

    private PhoneNumberUtil phoneUtil;
    private static String defaultRegion = "FR";
    private static PhoneNumberUtil.PhoneNumberFormat defaultFormat = PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL;

    public PhoneNumberTransformer() {
        phoneUtil = PhoneNumberUtil.getInstance();
    }

    @Override
    public String transform(Object value) throws TransformException {
        try {
            Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(value.toString(), defaultRegion);
            return phoneUtil.format(phoneNumberProto, defaultFormat);
        } catch (NumberParseException e) {
            throw new TransformException(value, "numéro de téléphone", e);
        }
    }
}
