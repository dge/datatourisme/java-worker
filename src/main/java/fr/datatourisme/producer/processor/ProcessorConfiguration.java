/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor;

import fr.datatourisme.assembler.Assembler;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.reasoner.InferenceReasoner;
import fr.datatourisme.producer.processor.reasoner.ValidationReasoner;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import fr.datatourisme.producer.processor.xml.XmlProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class ProcessorConfiguration {

    @Bean @Lazy
    public InferenceReasoner getInferenceReasoner(Assembler assembler) {
        return new InferenceReasoner(assembler);
    }

    @Bean @Lazy
    public ValidationReasoner getValidationReasoner(Assembler assembler) {
        return new ValidationReasoner(assembler);
    }

    @Bean @Lazy
    public XmlProcessor getXmlProcessor() {
        return new XmlProcessor();
    }

    @Bean(name = "alignmentFactory") @Lazy
    public AlignmentFactory createAlignmentFactory() {
        return new AlignmentFactory();
    }

    @Bean(name = "thesaurusAlignmentFactory") @Lazy
    public ThesaurusAlignmentFactory createThesaurusAlignmentFactory() {
        return new ThesaurusAlignmentFactory();
    }
}
