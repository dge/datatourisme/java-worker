/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.preview;

import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.processor.report.ResourceReport;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PropertyPreview {
    String uri;
    String label;
    Set<Object> values = new HashSet<>();
    List<PropertyAnomaly> anomalies = new ArrayList<>();
    Boolean mapped = false;
    float priority = 0;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Object> getValues() {
        return values;
    }

    public void addValue(Object value) {
        this.values.add(value);
    }

    public List<PropertyAnomaly> getAnomalies() {
        return anomalies;
    }

    public void addAnomaly(PropertyAnomaly anomaly) {
        this.anomalies.add(anomaly);
    }

    public Boolean isMapped() {
        return mapped;
    }

    public void setMapped(Boolean mapped) {
        this.mapped = mapped;
    }

    public float getPriority() {
        return priority;
    }

    public void setPriority(float priority) {
        this.priority = priority;
    }

    public static PropertyPreview fromProperty(Resource resource, Property property, ResourceReportPreview resourceReportPreview) {
        PropertyPreview preview = new PropertyPreview();
        Model model = property.getModel();

        // set uri
        preview.setUri(model.shortForm(property.getURI()));

        // set label & priority from ontology
        OntologyProperty ontProperty = resourceReportPreview.getOntology().getProperty(property.getURI());
        if(ontProperty != null) {
            preview.setLabel(ontProperty.getLabel());
            if(ontProperty.getOntResource().hasProperty(Datatourisme.hasPriority)) {
                preview.setPriority(ontProperty.getOntResource().getPropertyValue(Datatourisme.hasPriority).asLiteral().getFloat());
            }
        }

        // set anomalies
        List<ResourceReport.Report> reports = resourceReportPreview.getMapReports(resource);
        if(reports != null) {
            reports.forEach(r -> {
                if(r.getTarget() != null && model.expandPrefix(r.getTarget()).equals(property.getURI())) {
                    preview.addAnomaly(new PropertyAnomaly(r));
                    if(preview.getLabel() != null) {
                        r.setTargetLabel(preview.getLabel());
                    }
                }
            });
        }

        return preview;
    }

    static class PropertyAnomaly {
        Boolean error;
        String message;

        public PropertyAnomaly(ResourceReport.Report report) {
            this.error = report.isError();
            this.message = report.getMessage();
        }
    }
}
