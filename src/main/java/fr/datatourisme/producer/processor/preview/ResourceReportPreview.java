/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.preview;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import fr.datatourisme.gson.serializer.CollectionSerializer;
import fr.datatourisme.gson.serializer.MapSerializer;
import fr.datatourisme.gson.serializer.PrefixMappingSerializer;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.report.ResourceReport;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;

import java.util.*;

public class ResourceReportPreview {

    @SerializedName("@context")
    private PrefixMapping prefixMapping = PrefixMapping.Factory.create().setNsPrefixes(PrefixMapping.Standard);

    private ResourcePreview resource;
    private ResourceReport.ValidityReport report;

    private transient Alignment alignment;
    private transient Map<String, List<ResourceReport.Report>> mapReports = new HashMap<>();
    private transient Set<Resource> visited = new HashSet<>();

    public boolean isVisited(Resource resource) {
        return visited.contains(resource);
    }

    public void setVisited(Resource resource) {
        visited.add(resource);
    }

    /**
     * @param resourceReport
     * @param alignment
     */
    public ResourceReportPreview(ResourceReport resourceReport, Alignment alignment) {
        this.alignment = alignment;
        this.report = resourceReport.getReport();

        Resource resource = resourceReport.getResource();
        this.prefixMapping.setNsPrefixes(resource.getModel().getNsPrefixMap());

        this.report.getReports().forEach(r -> {
            List<ResourceReport.Report> descs = mapReports.computeIfAbsent(r.getSubject(), k -> new ArrayList<>());
            if(r.getTarget() != null) {
                r.setTarget(resource.getModel().shortForm(r.getTarget()));
            }
            descs.add(r);
        });

        Set<? extends AbstractRule> rules = alignment.getRules();
        this.resource = ResourcePreview.fromResource(resource, this, rules);
    }

    public ResourcePreview getResource() {
        return resource;
    }

    public void setResource(ResourcePreview resource) {
        this.resource = resource;
    }

    public ResourceReport.ValidityReport getReport() {
        return report;
    }

    public void setReport(ResourceReport.ValidityReport report) {
        this.report = report;
    }

    Ontology getOntology() {
        return alignment.getOntology();
    }

    Map<String, List<ResourceReport.Report>> getMapReports() {
        return mapReports;
    }

    List<ResourceReport.Report> getMapReports(Resource resource) {
        return mapReports.get(resource.getURI());
    }

    public String toJson(Boolean prettyPrinting) {
        GsonBuilder gsonBuilder = new GsonBuilder()
            .registerTypeHierarchyAdapter(Collection.class, new CollectionSerializer())
            .registerTypeHierarchyAdapter(Map.class, new MapSerializer())
            .registerTypeHierarchyAdapter(PrefixMapping.class, new PrefixMappingSerializer());
            //.registerTypeHierarchyAdapter(ValidityReport.class, new ValidityReportSerializer(prefixMapping));
        if(prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }
        return gsonBuilder.create().toJson(this);
    }

    public String toJson() {
        return toJson(false);
    }
}
