/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.preview;

import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.ontology.OntologyIndividual;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.alignment.rule.ResourceRule;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.*;

public class ResourcePreview {
    String uri;
    List<String> types;
    String label;
    String description;
    Boolean visited;
    Boolean obsolete = false;
    List<PropertyPreview> properties;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }

    public void setObsolete(Boolean obsolete) {
        this.obsolete = obsolete;
    }

    public List<PropertyPreview> getProperties() {
        return properties;
    }

    public void setProperties(List<PropertyPreview> properties) {
        this.properties = properties;
    }

    public static ResourcePreview fromResource(Resource resource, ResourceReportPreview resourceReportPreview, Set<? extends AbstractRule> rules) {
        ResourcePreview preview = new ResourcePreview();
        Model model = resource.getModel();

        if(!resource.isAnon()) {
            // set uri
            preview.setUri(model.shortForm(resource.getURI()));
        } else {
            // set uri
            preview.setUri(resource.toString());
        }

        // set label
        Statement label = resource.getProperty(RDFS.label);
        if(label != null) {
            preview.setLabel(label.getString());
        }

        // set types
        List<String> types = new ArrayList<>();
        resource.listProperties(RDF.type).forEachRemaining(stmt -> {
            types.add(model.shortForm(stmt.getObject().asResource().getURI()));
        });
        preview.setTypes(types);

        // check visited resource
        if(resourceReportPreview.isVisited(resource)) {
            preview.setVisited(true);
            return preview;
        }
        resourceReportPreview.setVisited(resource);

        // obsolete ?
        Boolean obsolete = resource.hasProperty(DatatourismeMetadata.isObsolete, ResourceFactory.createTypedLiteral(true));
        preview.setObsolete(obsolete);
        resource.removeAll(DatatourismeMetadata.isObsolete);

        // load direct properties
        Set<String> directProperties = new HashSet<>();
        //Map<String, PropertyPreview> properties = new HashMap<>();
        types.stream().forEach(uri -> {
            OntologyClass ontClass = resourceReportPreview.getOntology().getClass(model.expandPrefix(uri));
            if(ontClass != null) {
                ontClass.listDirectProperties().forEach(p -> {
                    directProperties.add(p.getURI());
                });
            }
        });

        // load values
        Map<String, PropertyPreview> properties = new HashMap<>();
        directProperties.stream().forEach(uri -> {
            PropertyPreview propertyPreview = PropertyPreview.fromProperty(resource, model.getProperty(uri), resourceReportPreview);
            properties.put(uri, propertyPreview);
        });

        // check rules to see all mapped
        if(rules != null) {
            rules.stream()
                .filter(r -> !r.getProperty().hasURI(DatatourismeMetadata.isObsolete.getURI()))   // remove obsolete from preview
                .map(r -> r.getProperty().getURI())
                .forEach(uri -> {
                    PropertyPreview propertyPreview = properties.computeIfAbsent(uri, k -> PropertyPreview.fromProperty(resource, model.createProperty(uri), resourceReportPreview));
                    propertyPreview.setMapped(true);
                });
        }

        // populate all property values
        resource.listProperties().forEachRemaining(stmt -> {
            String propUri = stmt.getPredicate().getURI();
            PropertyPreview propertyPreview = properties.computeIfAbsent(propUri, k -> PropertyPreview.fromProperty(resource, stmt.getPredicate(), resourceReportPreview));

            // find current rule
            AbstractRule rule = null;
            if(rules != null) {
                rule = rules.stream().filter(r -> {
                    return r.getProperty().getURI().equals(propUri);
                }).findFirst().orElse(null);
            }

            RDFNode object = stmt.getObject();
            if(object.isResource()) {
                Resource res = object.asResource();

                // specific case : add label for thesaurus value
                if(!res.isAnon() && resourceReportPreview.getOntology().hasIndividual(res.getURI())) {
                    OntologyIndividual individual = resourceReportPreview.getOntology().getIndividual(res.getURI());
                    propertyPreview.addValue(individual.getLabel() != null ? individual.getLabel() : individual.getURI());
                    return;
                }

                // find next rules in current rule
                Set<? extends AbstractRule> nextRules = null;
                if(rule != null && rule instanceof ResourceRule) {
                    nextRules = ((ResourceRule)rule).getRules();
                }

                propertyPreview.addValue(ResourcePreview.fromResource(res, resourceReportPreview, nextRules));
            } else {
                String value = object.asLiteral().getString();
                if(!object.asLiteral().getLanguage().equals("")) {
                    value += "@" + object.asLiteral().getLanguage();
                }
                propertyPreview.addValue(value);
            }
        });

        preview.setProperties(new ArrayList<>(properties.values()));

        return preview;
    }
}
