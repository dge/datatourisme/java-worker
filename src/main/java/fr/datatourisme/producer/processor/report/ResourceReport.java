/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.report;

import fr.datatourisme.ontology.OntologyClass;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ResourceReport {
    Resource resource;
    ValidityReport report;
    OntologyClass mainClass;

    public ResourceReport(Resource resource, org.apache.jena.reasoner.ValidityReport report) {
        this.resource = resource;
        this.report = new ValidityReport(report);
    }

    public Resource getResource() {
        return resource;
    }

    public Model getModel() {
        return resource.getModel();
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public ValidityReport getReport() {
        return report;
    }

    public void setReport(ValidityReport report) {
        this.report = report;
    }

    public List<Report> getReports() {
        return this.report.getReports();
    }

    public Boolean isValid() {
        return this.report.isValid();
    }

    public Boolean isClean() {
        return this.report.isClean();
    }

    /**
     * ValidityReport
     */
    public class ValidityReport {
        private transient org.apache.jena.reasoner.ValidityReport originalReport;
        private Boolean valid;
        private Boolean clean;
        private List<Report> reports = new ArrayList<>();

        public ValidityReport(org.apache.jena.reasoner.ValidityReport originalReport) {
            this.originalReport = originalReport;
            setValid(originalReport.isValid());
            setClean(originalReport.isClean());
            originalReport.getReports().forEachRemaining(r -> {
                reports.add(new Report(r));
            });
        }

        public org.apache.jena.reasoner.ValidityReport getOriginalReport() {
            return originalReport;
        }

        public Boolean isValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }

        public Boolean isClean() {
            return clean;
        }

        public void setClean(Boolean valid) {
            this.clean = clean;
        }

        public List<Report> getReports() {
            return reports;
        }
    }

    /**
     * Report
     */
    public class Report {
        private transient org.apache.jena.reasoner.ValidityReport.Report originalReport;
        private String message = null;
        private String subject = null;
        private String target = null;
        private String targetLabel = null;
        private Boolean error = null;

        private transient Pattern patt = Pattern.compile("\\$(\\d+)");

        public Report(org.apache.jena.reasoner.ValidityReport.Report originalReport) {
            this.originalReport = originalReport;
            setError(originalReport.isError());
            setSubject(((Resource) originalReport.getExtension()).getURI());

            List<String> list = new ArrayList<>(Arrays.asList(originalReport.getDescription().split("\n")));

            // set message
            String message = list.get(0);
            message = StringEscapeUtils.unescapeJava(message.substring(1, message.length() - 1));
            list.remove(0);


            List<String> args = list.stream()
                .filter(s -> s.startsWith("Implicated node:"))
                .map(s -> s.split(":", 2)[1].trim())
                .map(s -> {
                    if(s.startsWith("<") || s.startsWith("'")) {
                        return s.substring(1, s.length() - 1);
                    }
                    return s;
                })
                .collect(Collectors.toList());

            if(args.size() > 0) {
                // set target
                setTarget(getModel().expandPrefix(args.get(0)));

                // search and replace $1, $2 in message
                Matcher m = patt.matcher(message);
                StringBuffer sb = new StringBuffer(message.length());
                while (m.find()) {
                    int index = Integer.parseInt(m.group(1));
                    if(args.get(index) != null) {
                        m.appendReplacement(sb, Matcher.quoteReplacement(args.get(index)));
                    }
                }
                m.appendTail(sb);
                message = sb.toString();
            }

            setMessage(message);
        }

        public org.apache.jena.reasoner.ValidityReport.Report getOriginalReport() {
            return originalReport;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

        public String getTargetLabel() {
            return targetLabel;
        }

        public void setTargetLabel(String targetLabel) {
            this.targetLabel = targetLabel;
        }

        public Boolean isError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }
    }
}
