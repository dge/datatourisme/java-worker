/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query;

import fr.datatourisme.producer.processor.xml.query.Exception.QueryEvaluationException;
import fr.datatourisme.utils.JDOM2Utils;
import org.jdom2.Element;

import java.util.stream.Stream;

/**
 * 
 */
public abstract class QueryExpression {

    public abstract Stream<Object> evaluate(Element context) throws QueryEvaluationException;

    public abstract Object evaluateFirst(Element context) throws QueryEvaluationException;

    public Stream<String> evaluateValues(Element context) throws QueryEvaluationException {
        return evaluate(context).map(JDOM2Utils::getNodeValue);
    }

    public String evaluateFirstValue(Element context) throws QueryEvaluationException {
        return JDOM2Utils.getNodeValue(evaluateFirst(context));
    }

}
