/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query;

import fr.datatourisme.producer.processor.xml.query.Exception.QueryEvaluationException;
import fr.datatourisme.producer.processor.xml.query.Exception.QuerySyntaxException;
import fr.datatourisme.utils.JDOM2Utils;
import fr.datatourisme.utils.SaxonUtils;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.*;
import net.sf.saxon.type.Type;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.Text;
import org.jdom2.input.SAXBuilder;

import javax.xml.transform.Source;
import java.io.StringReader;
import java.util.Collection;
import java.util.stream.Stream;

/**
 * 
 */
public class XQueryExpression extends QueryExpression {
    private XQueryEvaluator xqueryEvaluator;

    private static Processor processor;

    static {
        processor = new Processor(new Configuration());
    }

    XQueryExpression(String expr, Collection<Namespace> namespaces) throws QuerySyntaxException {
        try {
            XQueryCompiler compiler = processor.newXQueryCompiler();
            for(Namespace ns: namespaces) {
                compiler.declareNamespace(ns.getPrefix(), ns.getURI());
            }
            xqueryEvaluator = compiler.compile(expr).load();
        } catch (SaxonApiException e) {
            throw new QuerySyntaxException(e);
        }
    }

    @Override
    public Stream<Object> evaluate(Element context) throws QueryEvaluationException {
        setContext(context);
        Stream.Builder<Object> streamBuilder = Stream.builder();

        Element newContext = context.clone();
        newContext.removeContent();
        xqueryEvaluator.forEach(xdmItem -> {
            streamBuilder.add(wrap(xdmItem, newContext));
        });

        return streamBuilder.build();
    }

    @Override
    public Object evaluateFirst(Element context) throws QueryEvaluationException {
        setContext(context);

        Element newContext = context.clone();
        newContext.removeContent();
        if(xqueryEvaluator.iterator().hasNext()) {
            return wrap(xqueryEvaluator.iterator().next(), newContext);
        }

        return null;
    }

    /**
     * @param context
     */
    private void setContext(Element context) throws QueryEvaluationException {
        Source source = JDOM2Utils.asXMLSource(context);
        try {
            XdmNode n = processor.newDocumentBuilder().build(source);
            xqueryEvaluator.setContextItem(SaxonUtils.getDocumentElement(n));
        } catch (SaxonApiException e) {
            throw new QueryEvaluationException(e);
        }
    }

    /**
     * @param xdmItem
     * @return
     */
    protected Object wrap(XdmItem xdmItem, Element context) {
        if(xdmItem instanceof XdmNode) {
            switch(((XdmNode) xdmItem).getUnderlyingNode().getNodeKind()) {
                case Type.ELEMENT:
                    try {
                        SAXBuilder saxBuilder = new SAXBuilder();
                        Element rootElement = saxBuilder.build(new StringReader(xdmItem.toString())).getRootElement().detach();
                        context.addContent(rootElement);
                        return rootElement;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                case Type.ATTRIBUTE:
                    return new Attribute(((XdmNode) xdmItem).getNodeName().getEQName(), xdmItem.getStringValue());
                case Type.TEXT:
                    return new Text(xdmItem.getStringValue());
            }
        }
        return new Text(xdmItem.getStringValue());
    }
}
