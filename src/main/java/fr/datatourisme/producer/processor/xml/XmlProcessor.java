/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.reasoner.InferenceReasoner;
import fr.datatourisme.producer.processor.reasoner.ValidationReasoner;
import fr.datatourisme.producer.processor.report.ResourceReport;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeData;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class XmlProcessor {

    @Autowired
    Ontology ontologyAlignment;

    @Autowired
    InferenceReasoner inferenceReasoner;

    @Autowired
    ValidationReasoner validationReasoner;

    /**
     * @param xml
     * @param i18nXmls
     * @return
     */
    public ResourceReport process(Alignment alignment, byte[] xml, Map<String, byte[]> i18nXmls) throws JDOMException, IOException, QueryExpressionException {

        // prepare model
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix("data", DatatourismeData.getURI());
        model.setNsPrefixes(ontologyAlignment.getPrefixMapping());

        // create context
        XmlContext ctx = XmlContext.create(xml);

        // create i18nCtx
        Map<String, XmlContext> i18nCtx = new HashMap<>();
        if(i18nXmls != null) {
            for (Map.Entry<String, byte[]> e : i18nXmls.entrySet()) {
                i18nCtx.put(e.getKey(), XmlContext.create(e.getValue()));
            }
        }

        // prepare resource
        Resource anon = model.createResource();

        // process dc:identifier rule now to generate URI
        AbstractRule identifierRule = alignment.getRule(DC.identifier);
        identifierRule.process(ctx, anon);

        // rename resource
        String uri = alignment.getUriGenerator().generate(anon);
        Resource resource = ResourceUtils.renameResource(anon, uri);

        // process rdf:type and get poi classes
        Set<OntologyClass> poiClasses = processRdfType(resource, ctx, alignment);

        // prepare property list
        Set<OntologyProperty> properties = new HashSet<>();
        poiClasses.forEach(c -> {
            properties.addAll(
                c.listCandidateProperties().collect(Collectors.toSet())
            );
        });

        // process each property
        properties.stream()
            .filter(p -> !p.hasURI(DC.identifier.getURI()) && !p.hasURI(RDF.type.getURI()))  // dc:identifier & rdf:type was already processed
            .forEach(p -> {
                alignment.getRules(p).forEach(rule -> {
                    try {
                        rule.process(ctx, i18nCtx, resource);
                    } catch (QueryExpressionException e) {}
                });
            });

        // inference
        inferenceReasoner.process(model);

        // validate
        ValidityReport validityReport = validationReasoner.validate(resource);

        // return resource & report
        return new ResourceReport(resource, validityReport);
    }

    /**
     * @param xml
     * @return
     */
    public ResourceReport process(Alignment alignment, byte[] xml) throws JDOMException, IOException, QueryExpressionException {
        return process(alignment, xml, null);
    }

    /**
     * Process rdf:type rule and return POI classes
     *
     * @param ctx
     * @return
     */
    private Set<OntologyClass> processRdfType(Resource resource, XmlContext ctx, Alignment alignment) throws QueryExpressionException {
        AbstractRule typeRule = alignment.getRule(RDF.type);
        if(typeRule != null) {
            typeRule.process(ctx, resource);
        }

        // get POI classes
        Set<OntologyClass> classes = resource.listProperties(RDF.type)
            .mapWith(s -> ontologyAlignment.getClass(s.getObject().asResource()))
            .filterKeep(c -> c.hasSuperClass(Datatourisme.PointOfInterest.getURI(), false))
            .toSet();

        // if there is no poi class, add PointOfInterest as default
        if(classes.size() == 0) {
            resource.addProperty(RDF.type, Datatourisme.PointOfInterest);
            classes.add(ontologyAlignment.getClass(Datatourisme.PointOfInterest));
        }

        return classes;
    }
}
