/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor;

import fr.datatourisme.vocabulary.DatatourismeData;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.ResourceUtils;
import org.apache.jena.vocabulary.DC;

import java.util.UUID;

public class URIGenerator {
    String producerIdentifier = null;

    public void setProducerIdentifier(String producerIdentifier) {
        this.producerIdentifier = producerIdentifier;
    }

    public String generate(Resource resource) {

        // if dc:identifier
        if(resource.hasProperty(DC.identifier)) {
            String identifier = resource.getProperty(DC.identifier).getObject().asLiteral().getValue().toString();
            String UUID = java.util.UUID.nameUUIDFromBytes(identifier.getBytes()).toString();

            String qName = (this.producerIdentifier != null ? this.producerIdentifier + "/" : "") +  UUID;
            return DatatourismeData.resource(qName).getURI();
        }

        // else
        String UUID = getResourceUUID(resource);
        return DatatourismeData.resource(UUID).getURI();
    }

    /**
     * Identify a resource : rename anonymous to the final URI
     *
     * @param anon
     * @return
     */
    public Resource identify(Resource anon) {
        return ResourceUtils.renameResource(anon, generate(anon) );
    }

    /**
     * Generate a resource UUID base on properties
     *
     * @param resource
     * @return
     */
    static public String getResourceUUID(Resource resource) {
        String hash = resource.listProperties().toList().stream().map(stmt -> {
            return UUID.nameUUIDFromBytes((stmt.getPredicate().getURI() + "|" + stmt.getObject().toString()).getBytes()).toString();
        }).sorted().reduce((a, b) -> {
            return a + b;
        }).orElse(resource.getURI());
        return UUID.nameUUIDFromBytes(hash.getBytes()).toString();
    }
}
