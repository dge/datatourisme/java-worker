/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.reasoner;

import fr.datatourisme.assembler.Assembler;
import fr.datatourisme.vocabulary.DatatourismeAlignment;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.rulesys.Functor;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.ReasonerVocabulary;

import java.util.ArrayList;
import java.util.List;

public class ValidationReasoner {

    public enum ViolationNature { error, warn }

    private Assembler assembler;

    /**
     * @param assembler
     */
    public ValidationReasoner(Assembler assembler) {
        this.assembler = assembler;
    }

    /**
     * @param resource
     * @return
     */
    public ValidityReport validate(Resource resource) {
        Model model = resource.getModel();
        processCustomViolations(model);

        InfModel validationModel = assembler.createReasonerInfModel(":reasonerValidation", model);

        // add the rb:validation on() triplet on resource to validate
        Graph graph = validationModel.getGraph();
        Triple validateOn = new Triple(resource.asNode(),
            ReasonerVocabulary.RB_VALIDATION.asNode(),
            Functor.makeFunctorNode("on", new Node[] {}));
        graph.add(validateOn);

        // validate
        ValidityReport report = validationModel.validate();

        // clean custom violations
        model.removeAll(null, ReasonerVocabulary.RB_VALIDATION_REPORT, null);

        // remove the rb:validation on() triplet
        graph.delete(validateOn);
        return report;
    }

    /**
     * Static method to easily add violation to a resource
     * Custom violation is necessary to be kept in inference rules step
     *
     * @param resource
     * @param property
     */
    public static void addViolation(Resource resource, Property property, ViolationNature nature, String message) {
        Model model = resource.getModel();
        Resource bnode = model.createResource()
            .addProperty(RDFS.comment, message.replace("\n", " "))
            .addProperty(RDFS.label, nature.toString());
        if(property != null) {
            bnode.addProperty(RDF.predicate, property);
        }
        model.add(resource, DatatourismeAlignment.hasViolation, bnode);
    }

    /**
     * Replace custom violation by Jena violations
     *
     * @param model
     */
    private void processCustomViolations(Model model) {
        List<Statement> stmts = model.listStatements(null, DatatourismeAlignment.hasViolation, (RDFNode) null).toList();
        stmts.forEach(stmt -> {
            Resource bnode = stmt.getObject().asResource();
            Resource resource = stmt.getSubject().asResource();
            Statement predicateStmt = bnode.getProperty(RDF.predicate);

            List<Node> args = new ArrayList<>();
            args.add(model.createLiteral(predicateStmt != null ? "error on property" : "error on resource").asNode());
            args.add(bnode.getProperty(RDFS.comment).getObject().asNode());

            if(predicateStmt != null) {
                args.add(bnode.getProperty(RDF.predicate).getObject().asNode());
            }

            // create jena violation
            String nature = bnode.getProperty(RDFS.label).getString();
            Literal violation = model.createTypedLiteral(new Functor(nature, args.toArray(new Node[]{})), Functor.FunctorDatatype.theFunctorDatatype);
            resource.addProperty(ReasonerVocabulary.RB_VALIDATION_REPORT, violation);

            // remove bnode and stmt
            model.removeAll(bnode, null, null);
            model.remove(stmt);
        });
    }

    /**
     * Clean model from jena violations
     *
     * @param model
     */
    private void cleanViolations(Model model) {
        model.removeAll(null, ReasonerVocabulary.RB_VALIDATION_REPORT, null);
    }
}
