/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.reasoner;

import fr.datatourisme.assembler.Assembler;
import fr.datatourisme.vocabulary.DatatourismeData;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.util.HashMap;
import java.util.Map;

public class InferenceReasoner {
    private Assembler assembler;
    static String[] engines =  {":reasonerInferenceInstance", ":reasonerInferenceBusiness"};
    static private Map<String, Model> originals = new HashMap<>();

    public InferenceReasoner(Assembler assembler) {
        this.assembler = assembler;
        for(String engine: engines) {
            originals.put(engine, ModelFactory.createDefaultModel().add(assembler.createReasonerInfModel(engine)));
        }
    }

    /**
     * @return
     */
    public void process(Model model)  {
        for(String engine: engines) {
            processInfModel(engine, model);
        }
    }

    /**
     * @return
     */
    public void processInfModel(String engine, Model model)  {
        InfModel infModel = assembler.createReasonerInfModel(engine, model);
        Model originalModel = originals.get(engine);

        // model to add
        //Model plus = infModel.difference(originalModel).difference(model);
        Model plus = ModelFactory.createDefaultModel().add(
            infModel.difference(originalModel).difference(model).listStatements()
                .filterKeep(stmt ->
                    !stmt.getSubject().isAnon()
                        && (stmt.getSubject().getURI().startsWith(DatatourismeData.getURI()) || stmt.getSubject().getURI().startsWith("urn:"))
                )
                .toList()
        );

        // model to remove
        // ---> seems to be ineffective due to the fact that the reasoner drop the assertions directly in the underlying graph
        Model minus = model.difference(infModel);
        //minus.listStatements().forEachRemaining(System.out::println);

        model.add(plus).remove(minus);
    }
}