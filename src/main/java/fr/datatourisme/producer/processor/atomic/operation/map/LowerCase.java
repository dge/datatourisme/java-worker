/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;

import java.util.stream.Stream;

public class LowerCase extends AbstractMapOperation {
    @Override
    public Stream<String> process(Stream<String> stream) {
        return stream.map(String::toLowerCase);
    }
}
