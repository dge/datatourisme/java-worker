/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.reduce;

import fr.datatourisme.producer.processor.atomic.operation.AbstractReduceOperation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Index extends AbstractReduceOperation {
    public Index(Object[] args) {
        super(args);
    }

    public String process(Stream<String> stream) {
        Object indexArg = args.length > 0 ? args[0] : 0;
        if(indexArg instanceof Number) {
            int index = ((Number) indexArg).intValue();
            List<String> list = stream.collect(Collectors.toList());
            try {
                return list.get(index);
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }
        return null;
    }
}
