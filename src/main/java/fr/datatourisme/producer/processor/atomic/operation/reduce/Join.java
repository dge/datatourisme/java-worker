/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.reduce;

import fr.datatourisme.producer.processor.atomic.operation.AbstractReduceOperation;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Join extends AbstractReduceOperation {
    public Join(Object[] args) {
        super(args);
    }

    public String process(Stream<String> stream) {
        String glue = args.length > 0 ? args[0].toString() : ",";
        return stream.collect(Collectors.joining(glue));
    }
}
