/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation;

import java.util.stream.Stream;

abstract public class AbstractMapOperation extends AbstractOperation {
    public AbstractMapOperation(Object[] args) {
        super(args);
    }

    public AbstractMapOperation() {
    }

    abstract public Stream<String> process(Stream<String> stream);
}
