/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.datatourisme.producer.processor.atomic.deserializer.AtomicChainDeserializer;
import fr.datatourisme.producer.processor.atomic.operation.AbstractExpandOperation;
import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import fr.datatourisme.producer.processor.atomic.operation.AbstractReduceOperation;
import fr.datatourisme.producer.processor.atomic.operation.map.Concat;
import org.apache.commons.lang3.StringUtils;

import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AtomicChain {
    LinkedList<AbstractOperation> operations = new LinkedList<>();

    public void addOperation(AbstractOperation op) {
        operations.add(op);
    }

    public LinkedList<AbstractOperation> getOperations() {
        return operations;
    }

    /**
     * static create
     *
     * @param ops
     * @return
     */
    public static AtomicChain create(AbstractOperation[] ops) {
        AtomicChain chain = new AtomicChain();
        for (AbstractOperation op : ops) {
            chain.addOperation(op);
        }
        return chain;
    }

    /**
     * Process a list of values
     *
     * @param values
     * @return
     */
    public List<String> process(List<String> values) {
        Stream<String> stream = processStream(values.stream());
        return stream.collect(Collectors.toList());
    }

    /**
     * Process a stream
     *
     * @return
     */
    public Stream<String> processStream(Stream<String> stream) {
        return processStream(stream, operations.size() > 0 ? operations.getFirst() : null)
            .map(String::trim)
            .filter(StringUtils::isNotEmpty);
    }

    /**
     * Process a stream
     *
     * @param stream
     * @param op
     * @return
     */
    private Stream<String> processStream(Stream<String> stream, AbstractOperation op) {
        stream = stream
            .filter(Objects::nonNull)
            .map(String::trim);

        if(op == null) {
            return stream;
        }

        AbstractOperation nextOp = getNextOperation(op);

        // reduce operation
        if(op instanceof AbstractReduceOperation) {
            Stream<String> str = Stream.of(((AbstractReduceOperation) op).process(stream));
            return processStream(str, nextOp);
        }

        // expand operation
        if(op instanceof AbstractExpandOperation) {
            if(nextOp instanceof Concat) {
                // special case : next op is concat, let build concatened stream
                Stream<String> st = stream
                    .map(((AbstractExpandOperation) op)::process)
                    .reduce(Stream::concat).orElseGet(Stream::empty);
                return processStream(st, getNextOperation(nextOp));
            } else {
                // normal case, flat map the next op result
                return stream.flatMap(s -> {
                    Stream<String> st = ((AbstractExpandOperation) op).process(s);
                    return processStream(st, nextOp);
                });
            }
        }

        // map operation
        if(op instanceof AbstractMapOperation) {
            Stream<String> st = ((AbstractMapOperation) op).process(stream);
            return processStream(st, nextOp);
        }

        //return Stream.of(stream, nextStream).flatMap(Function.identity());
        return null;
    }

    /**
     * @param op
     * @return
     */
    private AbstractOperation getNextOperation(AbstractOperation op) {
        int idx = operations.indexOf(op);
        return idx < operations.size()-1 ? operations.get(idx + 1) : null;
    }

    public static AtomicChain fromJson(Reader reader) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(AtomicChain.class, new AtomicChainDeserializer())
                .create();
        return gson.fromJson(reader, AtomicChain.class);
    }

    public static AtomicChain fromJson(String json) {
        Reader reader = new StringReader(json);
        return fromJson(reader);
    }
}
