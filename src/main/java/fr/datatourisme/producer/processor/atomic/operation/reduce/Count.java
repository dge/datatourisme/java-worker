/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.reduce;

import fr.datatourisme.producer.processor.atomic.operation.AbstractReduceOperation;

import java.util.stream.Stream;

public class Count extends AbstractReduceOperation {
    public String process(Stream<String> stream) {
        return Long.toString(stream.count());
    }
}
