/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;

import java.util.stream.Stream;

public class Replace extends AbstractMapOperation {
    public Replace(Object[] args) {
        super(args);
    }

    @Override
    public Stream<String> process(Stream<String> stream) {
        String pattern = args[0].toString();
        String replace = args[1].toString();
        return stream.map(s -> s.replaceAll(pattern, replace));
    }
}
