/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Map values to another values
 */
public class Default extends AbstractMapOperation {
    public Default(Object[] args) {
        super(args);
    }

    @Override
    public Stream<String> process(Stream<String> stream) {
        String _default = args[0].toString();
        List<String> list = stream.collect(Collectors.toList());
        if(list.size() == 0) {
            return Stream.of(_default);
        } else {
            return list.stream();
        }
    }
}
