/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import com.alexkasko.springjdbc.iterable.CloseableIterator;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.xml.datasource.XmlItem;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.producer.xml.datasource.XmlItemIndexer;
import fr.datatourisme.producer.xml.xpath.XPathMapper;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ProcessXmlTask extends AbstractTask {

    private File sourceDir;
    private File db;
    private File schemasOutputDir;

    @Autowired
    private Ontology ontology;

    @Autowired
    private ThesaurusAlignmentFactory thesaurusAlignmentFactory;

    @Autowired
    private AlignmentFactory alignmentFactory;

    // dependencies
    private ThesaurusAlignment thesaurusAlignment;
    private Alignment alignment;

    // reset db ?
    private boolean reset = false;

    private ReportEntry reportEntry = new ReportEntry();

    @Override
    public String getMachineName() {
        return "process_xml";
    }

    @Override
    protected String getLocalizedName() {
        return "Traitement XML";
    }

    @Override
    public ReportEntry execute() throws Exception {
        ReportEntry.Duration duration;
        reportEntry.getDuration().start();

        this.sourceDir = storage.dir("source").toFile();
        this.schemasOutputDir = storage.dir("xpath-schemas").toFile();

        if(alignment == null) {
            throw new JobException("Les informations d'alignement sont absentes");
        }

        if(StringUtils.isBlank(alignment.getContextExpr())
                || alignment.getRule(DC.identifier) == null
                || alignment.getRule(RDFS.label) == null
                || alignment.getRule(RDF.type) == null ) {
            throw new JobException("Les informations d'extraction XPath sont incomplètes");
        }

        XmlItemIndexer indexer = XmlItemIndexer.instanciate(db, alignment);

        // 1 : index all XML files
        logger.info("Indexation des fichiers XML sources");
        duration = reportEntry.getDuration().startProcess("index_source");
        indexSourceFiles(indexer);
        duration.end();

        // 2 : reindex POI types
        logger.info("Indexation des types de POI");
        duration = reportEntry.getDuration().startProcess("index_poi_type");
        reindexTypes(indexer);
        duration.end();

        // 3 : delete all schema but raw.json
        schemasOutputDir.mkdirs();
        Files.newDirectoryStream(Paths.get(schemasOutputDir.getPath())).forEach(p -> {
            if(!p.toFile().getName().equals("raw.json")) {
                p.toFile().delete();
            }
        });

        // 4 : generate poi schema from db
        duration = reportEntry.getDuration().startProcess("xpath_poi");
        logger.info("Génération du schéma XPath POI");
        generatePoiSchema(indexer);
        duration.end();

        // 5 : Generate types schema from db
        duration = reportEntry.getDuration().startProcess("xpath_specifics");
        logger.info("Génération des schémas XPath spécifiques");
        generateTypesSchemas(indexer);
        duration.end();

        // 6 : index linguistic XML files
        logger.info("Indexation des fichiers XML linguistiques");
        duration = reportEntry.getDuration().startProcess("index_i18n_source");
        indexI18nSourceFiles();
        duration.end();

        return reportEntry;
    }

    /**
     * Index all XML files
     */
    private void indexSourceFiles(XmlItemIndexer indexer) throws JobException, IOException, XPathParseException, NavException, XPathEvalException, JDOMException {

        TransactionStatus txStatus = indexer.getTransactionStatus();
        if(reset) {
            logger.debug("Reset poi.db");
            indexer.getDao().reset();
        }

        File[] files = sourceDir.listFiles((d, name) -> name.endsWith(".xml"));
        List<Map<String, Object>> reportFiles = new ArrayList<>();
        XmlItemIndexer.Stats stats = new XmlItemIndexer.Stats();
        //Set<String> identifiers = new HashSet<>();
        try {
            for (File file : files) {
                logger.debug("Indexing " + file.getName());
                try {
                    XmlItemIndexer.ProcessResult result = indexer.process(file.getAbsolutePath());
                    Map<String, Object> reportFile = new HashMap<>();
                    reportFile.put("name", file.getName());
                    reportFile.put("size", file.length());
                    reportFile.put("stats", result.stats);
                    reportFiles.add(reportFile);
                    stats.add(result.stats);
                    //identifiers.addAll(result.identifiers);
                } catch(ParseException e) {
                    throw new JobException("Le fichier XML n'est pas valide : " + file.getName());
                }
            }
            indexer.commit(txStatus);

            // append to report entry
            reportEntry.data("stats", stats);
            //reportEntry.data("identifiers", identifiers);
            reportEntry.data("files", reportFiles);

        } catch (DataAccessException e) {
            indexer.rollback(txStatus);
            throw e;
        }
    }
    /**
     * Index all i18n XML files
     */
    private void indexI18nSourceFiles() throws JobException, XPathEvalException, NavException, XPathParseException, JDOMException, IOException {
        String extension = FilenameUtils.getExtension(db.getName());
        String baseName = FilenameUtils.removeExtension(db.getName());

        // first, delete linguistic poi.db if reset
        if(reset) {
            FileFilter fileFilter = new WildcardFileFilter(baseName + ".*." + extension);
            File[] files = db.getParentFile().listFiles(fileFilter);
            for(final File file : files) {
                file.delete();
            }
        }

        // then, index each directory
        File[] dirs = sourceDir.listFiles(File::isDirectory);
        String basePath = FilenameUtils.removeExtension(db.getAbsolutePath());
        for(final File dir: dirs) {
            String lang = dir.getName();

            // create linguistic db
            String path = basePath + "." + lang + "." + extension;
            XmlItemIndexer indexer = XmlItemIndexer.instanciate(new File(path), alignment);
            TransactionStatus txStatus = indexer.getTransactionStatus();

            File[] files = dir.listFiles((d, name) -> name.endsWith(".xml"));
            try {
                for (File file : files) {
                    logger.debug("Indexing " + file.getName());
                    try {
                        indexer.process(file.getAbsolutePath());
                    } catch(ParseException e) {
                        throw new JobException("Le fichier XML n'est pas valide : " + file.getName());
                    }
                }
                indexer.commit(txStatus);
            } catch (DataAccessException e) {
                indexer.rollback(txStatus);
                throw e;
            }
        }
    }

    /**
     * Reset types index
     */
    private void reindexTypes(XmlItemIndexer indexer) throws JobException {
        AbstractRule rule = alignment.getRule(RDF.type);
        XmlItemDao dao = indexer.getDao();
        TransactionStatus txStatus = dao.getTransactionStatus();
        try {
            dao.findAll().forEachRemaining(item -> {
                Set<String> codes = new HashSet<>();
                try {
                    XmlContext ctx = XmlContext.create(item.getBytes());
                    rule.evaluate(ctx).forEach(uri -> {
                        OntologyClass ontClass = ontology.getClass(uri);
                        streamShortCodes(ontClass).forEach(codes::add);
                    });
                    codes.forEach(code -> dao.insertAttr(item, "type", code));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            dao.commit(txStatus);
        } catch (DataAccessException e) {
            dao.rollback(txStatus);
            throw e;
        }
    }

    /**
     * @param ontClass
     * @return
     */
    private Stream<String> streamShortCodes(OntologyClass ontClass) {
        return Stream.concat(Stream.of(ontClass), ontClass.listSuperClasses(false))
            .filter(c -> c.getURI().startsWith(Datatourisme.getURI()))
            .map(c -> c.getShortURI().split(":")[1]);
    }

    /**
     * Generate poi schema from db
     */
    private void generatePoiSchema(XmlItemIndexer indexer) throws IOException, ParseException, NavException {
        XmlItemDao dao = indexer.getDao();
        XPathMapper mapper = new XPathMapper(true);
        CloseableIterator<XmlItem> iter = dao.findAll();
        try {
            while(iter.hasNext()) {
                mapper.parse(iter.next().getBytes());
            }
        } finally {
            iter.close();
        }
        FileOutputStream fos = new FileOutputStream(new File(schemasOutputDir, "poi.json"));
        fos.write(mapper.toJson().getBytes());
        fos.close();
    }

    /**
     * Generate type schemas from db
     */
    private void generateTypesSchemas(XmlItemIndexer indexer) throws IOException, ParseException, NavException  {
        XmlItemDao dao = indexer.getDao();
        CloseableIterator<String> iter = dao.findDistinctAttrValues("type");
        iter.forEachRemaining(value -> {
            XPathMapper mapper = new XPathMapper(true);
            logger.info("Génération du schéma XPath : " + value);

            dao.findAllByAttr("type", value).forEachRemaining(item -> {
                try {
                    mapper.parse(item.getBytes());
                } catch (Exception e) {
                    logger.error(e);
                }
            });

            // save to file
            try {
                File outputFile = new File(schemasOutputDir, value + ".json");
                FileOutputStream fos = new FileOutputStream(outputFile);
                fos.write(mapper.toJson().getBytes());
                fos.close();
            } catch (IOException e) {
                logger.error(e);
            }
        });
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return ProduceRDFTask.class;
    }

    public ProcessXmlTask setDb(File db) {
        this.db = db;
        return this;
    }

    public ProcessXmlTask setReset(boolean reset) {
        this.reset = reset;
        return this;
    }

    public ProcessXmlTask setThesaurusAlignment(ThesaurusAlignment thesaurusAlignment) {
        this.thesaurusAlignment = thesaurusAlignment;
        return this;
    }

    public ProcessXmlTask setAlignment(Alignment alignment) {
        this.alignment = alignment;
        return this;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setReset((Boolean) payload.get("reset").orElse(false));
        setDb(storage.file("poi.db"));

        // get thesaurus
        Reader reader = payload.getJsonOrStream("thesaurus");
        try { reader = dumpToFile(reader, "thesaurus.json"); } catch (IOException e) { e.printStackTrace(); }
        setThesaurusAlignment(thesaurusAlignmentFactory.createFromJson(reader));

        // get alignment
        reader = payload.getRequiredJsonOrStream("alignment");
        try { reader = dumpToFile(reader, "alignment.json"); } catch (IOException e) { e.printStackTrace(); }
        setAlignment(alignmentFactory.createFromJson(thesaurusAlignment, reader));
    }

    /**
     * @param reader
     * @param filename
     * @throws IOException
     */
    private Reader dumpToFile(Reader reader, String filename) throws IOException {

        int intValueOfChar;
        StringBuilder buffer = new StringBuilder();
        while ((intValueOfChar = reader.read()) != -1) {
            buffer.append((char) intValueOfChar);
        }
        reader.close();

        File file = storage.file(filename);
        file.createNewFile();
        Writer targetFileWriter = new FileWriter(file);
        targetFileWriter.write(buffer.toString());
        targetFileWriter.close();

        return new StringReader(buffer.toString());
    }

}
