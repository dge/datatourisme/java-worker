/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import fr.datatourisme.producer.xml.xpath.XPathMapper;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class PreprocessXmlTask extends AbstractTask {

    private File sourceDir;
    private File schemasOutputDir;

    @Override
    public String getMachineName() {
        return "preprocess_xml";
    }

    @Override
    protected String getLocalizedName() {
        return "Prétraitement XML";
    }

    @Override
    public ReportEntry execute() throws Exception {
        ReportEntry reportEntry = new ReportEntry();

        // todo : xslt
        this.sourceDir = storage.dir("source").toFile();
        this.schemasOutputDir = storage.dir("xpath-schemas").toFile();

        // generate raw schema
        generateRawSchema();

        return reportEntry;
    }

    /**
     * Generate raw schema from source files
     */
    private void generateRawSchema() throws IOException, NavException, JobException {
        schemasOutputDir.mkdirs();

        // #1 : delete old
        File xpathSchema = new File(schemasOutputDir, "raw.json");
        if(xpathSchema.exists()) {
            xpathSchema.delete();
        }
        // #2 : run mapper
        logger.info("Génération du schéma XPath général");
        XPathMapper mapper = new XPathMapper(false);
        //logger.debug("Détermination de la liste des fichiers");
        File[] xmlFiles = sourceDir.listFiles((d, name) -> name.endsWith(".xml"));
        for(File xmlFile : xmlFiles) {
            //logger.debug("Traitement du fichier : " + xmlFile);
            byte[] bytes = IOUtils.toByteArray(new FileInputStream(xmlFile));
            try {
                mapper.parse(bytes);
            } catch (ParseException e) {
                throw new JobException("Le fichier XML " + xmlFile.getName() + " n'est pas valide : " + e.getMessage() );
            }
        }
        //logger.debug("Écriture du schéma sur le disque");
        mapper.writeToFile(xpathSchema);
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return ProcessXmlTask.class;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {

    }
}
