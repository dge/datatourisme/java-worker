/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.dataset.BlazegraphRestClient;
import fr.datatourisme.dataset.DatatourismeRepository;
import fr.datatourisme.dataset.model.ArchiveStatementsSelector;
import fr.datatourisme.dataset.query.ModelQueryExecutionService;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.task.ReportingTask;
import fr.datatourisme.utils.StreamUtils;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.util.FmtUtils;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Level;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

public class PersistRDFTask extends AbstractTask {

    @Autowired
    DatatourismeRepository datasetRepository;

    @Autowired
    BlazegraphRestClient datasetRestClient;

    @Autowired
    BlazegraphRestClient archiveDatasetRestClient;

    @Autowired
    BlazegraphRestClient extraDatasetRestClient;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    Exchange pipelineSourceExchange;

    @Autowired
    Ontology ontology;

    private File db;
    private int fluxIdentifier;

    private ReportEntry reportEntry = new ReportEntry();
    private Stats stats = new Stats();

    @Override
    public String getMachineName() {
        return "persist_rdf";
    }

    @Override
    protected String getLocalizedName() {
        return "Publication des données RDF";
    }

    @Override
    protected ReportEntry execute() throws Exception {
        ReportEntry.Duration duration;
        reportEntry.getDuration().start();

        logger.info("Préparation du dossier de publication");
        File inputDir = storage.dir("rdf").toFile();

        // get all nt files
        File[] ntFilesArr = inputDir.listFiles((d, name) -> name.endsWith(".nt"));

        Model insertModel = ModelFactory.createDefaultModel();
        if(inputDir.exists() && ntFilesArr != null && ntFilesArr.length > 0) {
            // prepare models
            logger.info("Préparation des POI à insérer / mettre à jour");
            duration = reportEntry.getDuration().startProcess("prepare_update_model");
            insertModel = prepareInsertModel(ntFilesArr);
            duration.end();
        }

        logger.info("Préparation des POI à invalider");
        duration = reportEntry.getDuration().startProcess("prepare_delete_model");
        Model deleteModel = prepareDeleteModel(insertModel, duration);
        duration.end();

        // prepare delete/insert files
        logger.info("Préparation de la publication");
        duration = reportEntry.getDuration().startProcess("prepare_publication_models");
        UpdateModels models = finalizeUpdateModels(insertModel, deleteModel);
        duration.end();

        File insertTempFile = File.createTempFile("insert", ".nt");
        RDFDataMgr.write(new FileOutputStream(insertTempFile), models.getInsert(), RDFFormat.NTRIPLES_ASCII);
        //finalInsertModel.listStatements().forEachRemaining(System.out::println);

        File deleteTempFile = File.createTempFile("delete", ".nt");
        RDFDataMgr.write(new FileOutputStream(deleteTempFile), models.getDelete(), RDFFormat.NTRIPLES_ASCII);

        // archive / extra
        logger.info("Archivage des resources");
        processArchive(models);
        purgeExtraDataset(models);

        // publication
        logger.info("Publication dans le dataset");
        duration = reportEntry.getDuration().startProcess("publication");
        datasetRestClient.deleteInsert(deleteTempFile, insertTempFile);
        insertTempFile.delete();
        deleteTempFile.delete();
        duration.end();

        // diffusion au pipeline d'enrichissement
        logger.info("Diffusion dans le pipeline d'enrichissement");
        if (ntFilesArr != null && ntFilesArr.length > 0) {
            sendEnrichmentPipelineMessages(ntFilesArr);
        }

        // add stat reports
        duration = reportEntry.getDuration().startProcess("prepare_stat_reports");
        List<ReportEntry> entries = getStatReportEntries();
        entries.forEach(reportEntry::addReport);
        duration.end();

        // calculate publication evolution
        int newSize = entries.size();
        int oldSize = entries.size() - stats.insert + stats.delete;
        if(oldSize > 0) {
            stats.evolution = ((float) newSize / oldSize) - 1;
        } else {
            stats.evolution = newSize > 0 ? 1 : 0;
        }
        stats.total = newSize;

        // counts report
        reportEntry.data("stats", stats);
        reportEntry.data("rdf_mutations", ImmutableMap.of(
            "insert", models.getInsert().size(),
            "delete", models.getDelete().size()
        ));

        return reportEntry;
    }

    /**
     * Prepare insert model
     *
     */
    private Model prepareInsertModel(File[] filesArr) {
        Model model = ModelFactory.createDefaultModel();
        for(File file: filesArr) {
            logger.debug("Traitement du fichier " + file.getName());
            RDFDataMgr.read(model, file.getAbsolutePath());
        }
        return model;
    }

    /**
     * Prepare delete model
     *
     */
    private Model prepareDeleteModel(Model insertModel, ReportEntry.Duration parentDuration) throws IOException {
        ReportEntry.Duration duration;
        XmlItemDao dao = XmlItemDao.instanciate(db);
        Model model = ModelFactory.createDefaultModel();

        ModelQueryExecutionService modelQueryExecutionService = new ModelQueryExecutionService(model);
        DatatourismeRepository modelRepository = new DatatourismeRepository(modelQueryExecutionService);

        // reset counts
        stats = new Stats();

        // get all published POI
        logger.debug("#1 : get all published POI ...");
        duration = parentDuration.startProcess("query_published_poi");
        Map<String, Resource> publishedPois = datasetRepository.findAllPOI(fluxIdentifier);
        duration.end();

        // list pois to insert
        logger.debug("#2 : list pois to insert/delete ...");
        duration = parentDuration.startProcess("list_insert_delete_poi");
        Set<Resource> insertPois = insertModel.listResourcesWithProperty(DatatourismeMetadata.sourceChecksum)
            .toSet();

        // list pois to delete
        Set<String> identifiers = new HashSet<>(dao.getTemplate().queryForList("SELECT id FROM items WHERE status = 1", String.class));
        Set<Resource> deletePois = publishedPois.entrySet().stream()
            .filter(es -> !identifiers.contains(es.getKey()))
            .map(Map.Entry::getValue)
            .collect(Collectors.toSet());

        // count insert/update/delete
        stats.update = (int) insertPois.stream().filter(publishedPois::containsValue).count();
        stats.insert = insertPois.size() - stats.update;
        stats.delete = deletePois.size();

        // IMPORTANT : compute insert and delete lists
        Set<Resource> insertDeletePois = new HashSet<>(insertPois);
        insertDeletePois.addAll(deletePois);
        duration.end();

        // if empty delete POI, just return empty model
        if(insertDeletePois.size() == 0) {
            return model;
        }

        // #3 : complete the model with whole hierarchy of the deleted POIs
        logger.debug("#3 : complete the model with whole hierarchy of the insert/delete POIs");
        duration = parentDuration.startProcess("complete_model_hierarchy");
        Model resultModel = datasetRepository.constructPOIHierarchy(insertDeletePois);
        model.add(resultModel);
        duration.end();

        // #4 : get list of all deletable sub-resources
        logger.debug("#4 : get list of all deletable sub-resources");
        duration = parentDuration.startProcess("list_deletable_resources");
        Set<Resource> deleteResources = model.listSubjects()
            .filterKeep(res -> !insertDeletePois.contains(res))
            .toSet();

        duration.end();

        // #5 : foreach sub-resource to delete, keep used otherwise
        Set<Resource> keepResources = Collections.synchronizedSet(new HashSet<>());

        logger.debug("#5 : foreach sub-resource to delete, keep used otherwise");
        if(deleteResources.size() > 0) {
            // get resources usages in dataset
            logger.debug("#5.1 : get resources usages in dataset");
            duration = parentDuration.startProcess("get_resources_usage");
            Map<Resource, Set<Resource>> resourcesUsages = datasetRepository.selectResourcesUsages(deleteResources);
            duration.end();
            // for each
            logger.debug("#5.2 : foreach");
            duration = parentDuration.startProcess("filter_deletable_resources");
            resourcesUsages.entrySet().stream().parallel()
                // only if contains POI outside the insert/delete scope
                .filter(e -> e.getValue().stream().anyMatch(poi -> !insertDeletePois.contains(poi)))
                // add to resources to keep
                .forEach(e -> {
                    synchronized (keepResources) {
                        keepResources.add(e.getKey());
                    }
                });
            duration.end();
        }

        // #6 : remove the whole graph of used resources
        logger.debug("#6 : remove the whole graph of used resources");
        if(keepResources.size() > 0) {
            duration = parentDuration.startProcess("remove_used_resources_graph");
            resultModel = modelRepository.constructResourceHierarchy(keepResources);
            model.remove(resultModel);
            duration.end();
        }

        return model;
    }

    /**
     * Final models processes
     */
    private UpdateModels finalizeUpdateModels(Model insertModel, Model deleteModel) {
        // prepare final models
        Model finalInsertModel = insertModel.difference(deleteModel);
        Model finalDeleteModel = deleteModel.difference(insertModel);

        // remove all lastUpdate metadata from final delete model
        finalDeleteModel.removeAll(null, Datatourisme.lastUpdateDatatourisme, (RDFNode) null);

        // pour chaque fingerprint du model insert final => update lastUpdate
        finalInsertModel.listResourcesWithProperty(DatatourismeMetadata.fingerprint)
            .forEachRemaining(resource -> {
                // ajout de la date de mise à jour
                resource.addProperty(Datatourisme.lastUpdateDatatourisme, insertModel.createTypedLiteral(Calendar.getInstance()));
                // suppression de la précèdente date de mise à jour
                deleteModel.listStatements(resource, Datatourisme.lastUpdateDatatourisme, (RDFNode) null)
                    .forEachRemaining(finalDeleteModel::add);
            });

        return new UpdateModels(finalInsertModel, finalDeleteModel);
    }

    /**
     * Store archived resources into a special namespace
     *
     * @param models
     */
    private void processArchive(UpdateModels models) throws IOException, URISyntaxException {
        // prepare update request
        UpdateRequest request = UpdateFactory.create() ;

        // add insert request
        Model insertModel = ModelFactory.createDefaultModel();
        models.getDelete().listSubjectsWithProperty(RDF.type, Datatourisme.PointOfInterest).forEachRemaining(subject -> {
            insertModel.add(models.getDelete().listStatements(new ArchiveStatementsSelector(subject)));
        });

        if(!insertModel.isEmpty()) {
            StringWriter writer = new StringWriter();
            RDFDataMgr.write(writer, insertModel, Lang.NT) ;
            // batch data to avoid StackOverflow error
            List<String> lines = Arrays.asList(writer.toString().split("\n"));
            StreamUtils.batchStream(lines, 1000).forEach(list -> {
                request.add("INSERT DATA { " + String.join("\n", list) + " }");
            });
        }

        // add delete requests as batch
        List<String> values = models.getInsert().listSubjectsWithProperty(RDF.type, Datatourisme.PointOfInterest)
            .mapWith(FmtUtils::stringForResource)
            .toList();

        StreamUtils.batchStream(values, 1000).forEach(list -> {
            request.add(String.format("DELETE { ?s ?p ?o } WHERE { ?s ?p ?o. VALUES ?s { %s } }", String.join(" ", list)));
        });

        if(request.getOperations().size() > 0) {
            archiveDatasetRestClient.update(request);
        }
    }

    /**
     * Purge the extra dataset from totaly deleted resources (aka no more triple in insert model)
     *
     * @param models
     */
    private void purgeExtraDataset(UpdateModels models) throws IOException, URISyntaxException {
        // prepare update request
        UpdateRequest request = UpdateFactory.create() ;

        List<String> values = models.getDelete().listSubjects()
            .filterDrop(resource -> models.getInsert().containsResource(resource))
            .mapWith(FmtUtils::stringForResource)
            .toList();

        StreamUtils.batchStream(values, 1000).forEach(list -> {
            request.add(String.format("DELETE { ?s ?p ?o } WHERE { GRAPH ?g { ?s ?p ?o } VALUES ?s { %s } }", String.join(" ", list)));
        });

        if(request.getOperations().size() > 0) {
            extraDatasetRestClient.update(request);
        }
    }

    /**
     * Send each file into the enrichment pipeline
     *
     * @param filesArr
     */
    private void sendEnrichmentPipelineMessages(File[] filesArr) {
        for (File file : filesArr) {
            try {
                MessageProperties properties = new MessageProperties();
                properties.setContentType("application/n-triples");
                byte[] bytes = FileUtils.readFileToByteArray(file);
                Message message = new Message(bytes, properties);
                rabbitTemplate.send(pipelineSourceExchange.getName(), "", message);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * @return
     */
    private List<ReportEntry> getStatReportEntries() {
        Map<String, Map<String, String>> reportMaps = new HashMap<>();

        List<ReportEntry> entries = new ArrayList<>();
        ResultSet rs = datasetRepository.selectPOIStats(fluxIdentifier);
        rs.forEachRemaining(qs -> {
            Map<String, String> reportMap = new HashMap<>();
            String uri = qs.getResource("uri").getURI();

            reportMap.put("uri", uri);
            reportMap.put("id", qs.getLiteral("id").getString());

            // label
            Literal label = qs.getLiteral("label");
            if(label != null) {
                reportMap.put("label", label.getString());
            }

            // type
            Resource type = qs.getResource("type");
            if(type != null) {
                OntologyClass ontClass = ontology.getClass(type.getURI());
                if(ontClass != null && ontClass.getLabel() != null) {
                    reportMap.put("type", ontClass.getLabel());
                }
            }

            // city
            Literal city = qs.getLiteral("city");
            if(city != null) {
                reportMap.put("city", city.getString());
            }

            // creator
            Literal creator = qs.getLiteral("creator");
            if(creator != null) {
                reportMap.put("creator", creator.getString());
            }

            reportMaps.putIfAbsent(uri, reportMap);
        });

        reportMaps.forEach((uri, reportMap) -> {
            ReportEntry reportEntry = new ReportEntry().setLevel(Level.INFO).data(reportMap);
            entries.add(reportEntry);
        });

        return entries;
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return ReportingTask.class;
    }

    public PersistRDFTask setDb(File db) {
        this.db = db;
        return this;
    }

    public PersistRDFTask setFluxIdentifier(int fluxIdentifier) {
        this.fluxIdentifier = fluxIdentifier;
        return this;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setDb(storage.file("poi.db"));
        setFluxIdentifier((Integer) payload.getRequired("flux"));
    }

    /**
     * UpdateModels
     */
    public static class UpdateModels  {
        private Model insert;
        private Model delete;

        public UpdateModels(Model insert, Model delete) {
            this.insert = insert;
            this.delete = delete;
        }

        public Model getInsert() {
            return insert;
        }

        public void setInsert(Model insert) {
            this.insert = insert;
        }

        public Model getDelete() {
            return delete;
        }

        public void setDelete(Model delete) {
            this.delete = delete;
        }
    }

    /**
     * Stats
     */
    public static class Stats  {
        public int insert = 0;
        public int update = 0;
        public int delete = 0;
        public int total = 0;
        public float evolution = 0;
    }
}
