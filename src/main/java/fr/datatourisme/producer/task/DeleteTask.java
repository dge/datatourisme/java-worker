/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class DeleteTask extends AbstractTask {

    @Override
    public String getMachineName() {
        return "delete";
    }

    @Override
    protected String getLocalizedName() {
        return "Suppression";
    }

    @Override
    public ReportEntry execute() throws Exception {

        // remove files
        String[] files = {"poi.db"/*, "alignment.json", "thesaurus.json"*/};
        for(String file: files) {
            File f = storage.file(file);
            if(f.exists()) {
                f.delete();
            }
        }

        // remove directories
        String[] directories = {"source", "rdf", "xpath-schemas"};
        for(String directory: directories) {
            // remove source
            File dir = storage.dir(directory).toFile();
            if(dir.exists()) {
                FileUtils.deleteDirectory(dir);
            }
        }

        return new ReportEntry();
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return PersistRDFTask.class;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException { }
}
