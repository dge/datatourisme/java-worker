/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.utils.LftpExecutor;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Request;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.jena.ext.com.google.common.collect.ImmutableList;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DownloadTask extends AbstractTask {

    private static Pattern acceptedPattern;

    private static Map<String, List<String>> mimeTypes = ImmutableMap.of(
        "xml", ImmutableList.of("application/xml", "text/xml"),
        "zip", ImmutableList.of("application/zip")
    );

    private URL url;

    private Map<String, URL> i18nUrls;

    public DownloadTask() {
        acceptedPattern = Pattern.compile("\\.(" + String.join("|", mimeTypes.keySet()) + ")$");
    }

    @Override
    public String getMachineName() {
        return "download";
    }

    @Override
    protected String getLocalizedName() {
        return "Téléchargement";
    }

    @Override
    public ReportEntry execute() throws Exception {
        ReportEntry reportEntry = new ReportEntry();
        Storage inputDir = storage.dir("source");

        // #1 : empty source folder
        logger.info("Préparation du dossier de réception");
        FileUtils.deleteDirectory(inputDir.toFile());
        inputDir.toFile().mkdirs();

        download(url, inputDir.toFile());
        if(i18nUrls != null) {
            for(Map.Entry<String, URL> entry : i18nUrls.entrySet()) {
                download(entry.getValue(), inputDir.dir(entry.getKey()).toFile());
            }
        }

        // report downloaded files
        File[] xmlFiles = inputDir.toFile().listFiles((d, name) -> name.endsWith(".xml"));
        if(xmlFiles.length == 0) {
            throw new JobException("Aucun fichier XML n'a été trouvé à l'adresse " + url);
        }

        List<Map<String, Object>> reportFiles = new ArrayList<>();
        Arrays.stream(xmlFiles).forEach(xmlFile -> {
            Map<String, Object> reportFile = new HashMap<>();
            reportFile.put("name", xmlFile.getName());
            reportFile.put("size", xmlFile.length());
            reportFiles.add(reportFile);
        });

        return reportEntry.data("files", reportFiles);
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return PreprocessXmlTask.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void initFromPayload(Payload payload) throws JobException {
        try {
            setUrl((String) payload.getRequired("source"));
            setI18nUrls((Map<String, String>) payload.get("i18nSources").orElse(null));
        } catch (MalformedURLException e) {
            throw new JobException(e);
        }
    }

    public DownloadTask setUrl(URL url) {
        this.url = url;
        return this;
    }

    public DownloadTask setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
        return this;
    }

    public DownloadTask setI18nUrls(Map<String, String> i18nUrls) {
        if(i18nUrls != null) {
            this.i18nUrls = new HashMap<>();
            i18nUrls.forEach((key, value) -> {
                try {
                    this.i18nUrls.put(key, new URL(value));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            });
        } else {
            this.i18nUrls = null;
        }

        return this;
    }

    /**
     * @param url
     * @param dir
     */
    private void download(URL url, File dir) throws Exception {

        // #1 get all links
        List<URL> urls = new ArrayList<>();
        if(acceptedPattern.matcher(url.getPath()).find()) {
            urls.add(url);
        } else {
            try {
                List<URL> all = getAllLinks(url);
                if(all.size() > 0) {
                    urls.addAll(all);
                } else {
                    urls.add(url);
                }
            } catch(JobException e) {
                urls.add(url);
            }
        }

        // #2 download each link
        for(URL _url: urls) {
            if(!_url.getProtocol().equals("file")) {
                logger.info("Téléchargement : " + _url);
            }
            downloadFile(_url, dir);
        }

        // #3 Unzip all compressed files
        File[] zipFiles = dir.listFiles((f, name) -> name.endsWith(".zip"));
        for (File zipFile : zipFiles) {
            logger.info("Décompression : " + zipFile.getName());
            unzip(zipFile);
            zipFile.delete();
        }

        // #4 remove hidden files
        File[] hiddenFiles = dir.listFiles(File::isHidden);
        for(File file: hiddenFiles) {
            file.delete();
        }
    }

    /**
     * Return all links for a base urlc
     *
     * @param url
     * @return
     * @throws IOException
     */
    private List<URL> getAllLinks(URL url) throws JobException {
        LftpExecutor executor = LftpExecutor.timeoutCommand("find -d 2 \"" + url + "\"");
        executeLftp(executor, url.toString());
        List<String> list = executor.getOutputStream().getLines();

        // filter only XML and ZIP
        return list.stream()
            .filter(acceptedPattern.asPredicate())
            .map(s -> {
                try {
                    return new URL(s);
                } catch (MalformedURLException e) {
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    /**
     * Download a specific file
     *
     * @param url
     * @param workingDir
     * @return
     * @throws IOException
     */
    private void downloadFile(URL url, File workingDir) throws JobException {

        String filename = FilenameUtils.getName(url.getPath());
        if(!acceptedPattern.matcher(filename).find()) {
            filename = FilenameUtils.getBaseName(url.getPath()) + ".tmp";
        }
        File targetFile = new File(workingDir, filename);

        // download file
        String mimeList = mimeTypes.values().stream().flatMap(Collection::stream).collect(Collectors.joining(","));
        String acceptHeader = mimeList + ";q=0.9,*/*;q=0.8";

        LftpExecutor executor = LftpExecutor.timeoutCommand("set http:accept \"" + acceptHeader + "\"; get \"" + url + "\" -o \"" + targetFile.getName() + "\"; bye")
            .setWorkingDirectory(workingDir);

        try {
            // first, try to download with lftp
            executeLftp(executor, url.toString());
        } catch(JobException e) {
            // fallback : try to download with fluent API
            if(url.toString().startsWith("http")) {
                try {
                    Request.Get(url.toString())
                        .connectTimeout(30000).socketTimeout(30000)
                        .addHeader("Accept", acceptHeader)
                        .execute()
                        .saveContent(targetFile);
                } catch (IOException e1) {
                    // else, throw initial exception
                    throw e;
                }
            } else {
                throw e;
            }
        }

        if(FilenameUtils.getExtension(targetFile.getName()).equals("tmp")) {
            // now try to find the right extension for temporary file
            try {
                String mimeType = getMimeType(targetFile);
                if(mimeType == null) {
                    throw new JobException("Impossible de déterminer le mime-type du fichier : " + url);
                }
                String extension = getExtension(mimeType);
                if(extension == null) {
                    throw new JobException("Le mime-type " + mimeType + " n'est pas pris en charge : " + url);
                }
                targetFile.renameTo(new File(workingDir, FilenameUtils.getBaseName(targetFile.getName()) + "." + extension));
            } catch (IOException e) {
                 throw new JobException(e);
            }
        }
    }

    /**
     * @param executor
     * @return
     * @throws JobException
     */
    private int executeLftp(LftpExecutor executor, String url) throws JobException {
        try {
            return executor.execute();
        } catch(HttpResponseException e) {
            throw new JobException("Impossible de récupérer " + url + " : HTTP " + e.getStatusCode());
        } catch(ConnectTimeoutException e) {
            throw new TemporaryJobException("Délai de connexion dépassé").setDelay(3600).setMaxTries(2);
        } catch(Exception e) {
            throw new JobException("Impossible de récupérer " + url + " : " + e.getLocalizedMessage());
        }
    }

    /**
     * Unzip a file
     *
     * @param zipFile
     * @return
     * @throws IOException
     */
    private int unzip(File zipFile) throws IOException {
        final CommandLine cmdLine = new CommandLine("unzip");
        cmdLine.addArgument("-jqo");
        cmdLine.addArgument(zipFile.getAbsolutePath(), false);
        // include xml
        cmdLine.addArgument("*.xml");
        DefaultExecutor exec = new DefaultExecutor();
        exec.setWorkingDirectory(zipFile.getParentFile());
        return exec.execute(cmdLine);
    }

    /**
     * @param file
     * @return
     */
    private String getMimeType(File file) throws IOException {
        String mimeType = Files.probeContentType(file.toPath());
        if(mimeType == null || mimeType.equals("application/x-zerosize")) {
            // Files.probeContentType may not work, so make an ultimate test to detect xml
            if (new BufferedReader(new FileReader(file)).readLine().startsWith("<?xml")) {
               return "text/xml";
            }
        }
        return mimeType;
    }

    /**
     * @param mimeType
     * @return
     */
    private String getExtension(String mimeType) {
        // remove derived information to get the standard mime type
        String standardMimeType = mimeType.replaceAll("\\/[a-z]+\\+", "/");
        for (Map.Entry<String, List<String>> entry : mimeTypes.entrySet()) {
            if(entry.getValue().contains(standardMimeType)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
