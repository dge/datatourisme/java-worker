/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import com.google.gson.GsonBuilder;
import com.ximpleware.NavException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;
import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.utils.VTDUtils;
import fr.datatourisme.worker.Tube;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/producer/{fluxId}/expr-raw-extract")
public class ExprRawExtractController extends AbstractController {

    @Autowired
    @Qualifier("producer")
    Tube tube;

    static class Request {
        public String expr = null;
        public Object atomic = null;
        public String context;
        public Boolean count = false;
        public Boolean distinct = false;
        public Boolean firstOnly = false;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @RequestBody Request request) throws IOException, XPathEvalException, NavException, XPathParseException, JDOMException {

        if (request.context == null) {
            return error("Il manque le paramètre \"context\"", HttpStatus.BAD_REQUEST);
        }

        File locationDir = tube.getStorage().dir(fluxId).dir("source", false).toFile();
        if (!locationDir.exists()) {
            throw new RuntimeException("Impossible de localiser le dossier source");
        }

        List<File> xmlFiles = new ArrayList<>();
        Files.newDirectoryStream(locationDir.toPath(), path -> path.toString().endsWith(".xml"))
                .forEach(f -> xmlFiles.add(f.toFile()));

        if (xmlFiles.size() == 0) {
            throw new RuntimeException("Unable to find any XML sources");
        }

        // build elements stream
        Stream<byte[]> stream = xmlFiles.stream()
            .map(file -> {
                try {
                    return VTDUtils.streamElements(file, request.context);
                } catch (Exception e) {
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .flatMap(i -> i);

        if (request.expr == null) {
            // just return context count
            return response(stream.count());
        }

        // initialize atomic chain
        AtomicChain atomicChain = new AtomicChain();
        if (request.atomic != null) {
            String json = new GsonBuilder().create().toJson(request.atomic);
            atomicChain = AtomicChain.fromJson(json);
        }
        AtomicChain finalAtomicChain = atomicChain;

        // stream process
        Stream<String> valuesStream = stream
                .map(bytes -> {
                    try {
                        return XmlContext.create(bytes);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .map(xmlContext -> {
                    try {
                        Stream<String> values = finalAtomicChain.processStream(xmlContext.evaluateValues(request.expr));
                        return request.firstOnly ? values.limit(1) : values;
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .flatMap(i -> i);

        if (request.distinct) {
            valuesStream = valuesStream.distinct();
        }

        if (request.count) {
            return response(valuesStream.count());
        }

        return response(valuesStream.collect(Collectors.toList()));
    }

}