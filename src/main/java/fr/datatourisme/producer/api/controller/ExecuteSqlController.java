/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/producer/{fluxId}/execute-sql")
public class ExecuteSqlController extends AbstractProducerController {

    static class  Request {
        public String query;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @RequestBody Request request) throws XmlDaoFileNotFoundException {

        if(request.query == null) {
            return response("Il manque le paramètre \"query\"", HttpStatus.BAD_REQUEST);
        }

        XmlItemDao dao = locateXmlItemDao(fluxId);

        List<Map<String, Object>> rows = dao.query(request.query, (rs, rowNum) -> {
            Map<String, Object> row = new HashMap<>();
            for(int i=1; i<=rs.getMetaData().getColumnCount(); i++) {
                String columnName = rs.getMetaData().getColumnName(i);
                row.put(columnName, rs.getString(i));
            }
            return row;
        });

        return response(rows);

    }

}