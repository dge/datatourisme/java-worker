/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.worker.Tube;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.File;

/**
 * fr.datatourisme.producer.api.controller
 */
abstract public class AbstractProducerController extends AbstractController {

    @Autowired
    @Qualifier("producer")
    Tube tube;

    @ExceptionHandler(XmlDaoFileNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public @ResponseBody
    String handleException(XmlDaoFileNotFoundException e) {
        return "Impossible de localiser la base de données";
    }

    /**
     * Locate a XmlItem db file based on flux id
     *
     * @param fluxId
     * @return
     */
    protected XmlItemDao locateXmlItemDao(Integer fluxId) throws XmlDaoFileNotFoundException {
        File daoFile = tube.getStorage().dir(fluxId).file("poi.db");
        if(!daoFile.exists()) {
            throw new XmlDaoFileNotFoundException();
        }
        return XmlItemDao.instanciate(daoFile);
    }

    /**
     * Exceptions
     */
    static class XmlDaoFileNotFoundException extends Exception {}
}
