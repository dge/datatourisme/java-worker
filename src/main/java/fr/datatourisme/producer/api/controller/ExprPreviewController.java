/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import com.alexkasko.springjdbc.iterable.CloseableIterator;
import com.google.gson.GsonBuilder;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.producer.xml.datasource.XmlItem;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.utils.VTDUtils;
import fr.datatourisme.worker.Tube;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/producer/{fluxId}/expr-preview")
public class ExprPreviewController extends AbstractProducerController {

    enum Source { dao, raw }

    static class Request {
        enum Selector {
            element,
            value
        }
        public Selector selector;
        public List<String> context;
        public String type;
        public String expr;
        public Object atomic;
        public Integer limit = 20;
    }

    @Autowired
    @Qualifier("producer")
    Tube tube;

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @RequestBody Request request) throws JDOMException, IOException, XmlDaoFileNotFoundException {
        return post(fluxId, Source.dao, request);
    }

    @PostMapping("/{source}")
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @PathVariable Source source, @RequestBody Request request) throws JDOMException, IOException, XmlDaoFileNotFoundException {

        // normalize limit
        request.limit = request.limit != null && request.limit <= 20 ? request.limit : 20;

        // initialize atomic chain
        AtomicChain atomicChain = new AtomicChain();
        if(request.atomic != null) {
            String json = new GsonBuilder().create().toJson(request.atomic);
            atomicChain = AtomicChain.fromJson(json);
        }
        AtomicChain finalAtomicChain = atomicChain;

        // prepare stream
        Stream<ItemContext> stream;
        CloseableIterator<XmlItem> closeableIterator = null;
        if(source == Source.dao) {
            XmlItemDao dao = locateXmlItemDao(fluxId);
            closeableIterator = dao.queryForIter("SELECT items.* FROM items ORDER BY RANDOM()");
            stream = streamDAOIterator(closeableIterator);
        } else {
            stream = streamRaw(fluxId, request);
        }

        // process stream
        Set<Result> response = stream
            .map(itemContext -> {
                Result result = new Result();
                result.identifier = itemContext.identifier;
                result.data = processContext(itemContext.xmlContext, request.context, request, finalAtomicChain)
                    .collect(Collectors.toList());
                if(result.data.size() > 0) {
                    return result;
                }
                return null;
            })
            .filter(Objects::nonNull)
            .limit(request.limit)
            .limit(1000)
            .collect(Collectors.toSet());

        if (closeableIterator != null) {
            closeableIterator.close();
        }

        return response(response);
    }

    /**
     * @param xmlContext
     * @param contextList
     * @param request
     * @param atomicChain
     * @return
     */
    private Stream<ContextResult> processContext(XmlContext xmlContext, List<String> contextList, Request request, AtomicChain atomicChain) {

        try {
            if(contextList != null && contextList.size() > 0) {
                // context list is not empty, return recursive
                List<String> localContextList = new ArrayList<>(contextList);
                String context = localContextList.remove(0);
                return xmlContext.evaluate(context)
                    .filter(obj -> obj instanceof Element)
                    .map(elmt -> xmlContext.create((Element) elmt))
                    .flatMap(xmlCtx -> processContext(xmlCtx, localContextList, request, atomicChain));
            }

            Stream<String> stream;
            if(request.selector == Request.Selector.element) {
                // request for element
                stream = xmlContext.evaluate(request.expr)
                    .filter(obj -> obj instanceof Element)
                    .map(obj -> {
                        XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat());
                        return outp.outputString((Element) obj).replaceAll("\\s+xmlns:[^=]+=\"[^\"]+\"", "");
                    });
            } else {
                // request for values = apply atomic chain
                stream = atomicChain.processStream(xmlContext.evaluateValues(request.expr));
            }

            ContextResult result = new ContextResult();
            result.context = getElementPath((Element) xmlContext.getContext());
            result.values = stream.collect(Collectors.toList());
            if(result.values.size() > 0) {
                return Stream.of(result);
            }
            return Stream.empty();
        } catch (QueryExpressionException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * @return
     */
    private Stream<ItemContext> streamDAOIterator(CloseableIterator<XmlItem> iter) throws XmlDaoFileNotFoundException {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iter, Spliterator.ORDERED), false)
            .map(xmlItem -> {
                try {
                    ItemContext itemContext = new ItemContext();
                    itemContext.identifier = xmlItem.getIdentifier();
                    itemContext.xmlContext = XmlContext.create(xmlItem.getBytes());
                    return itemContext;
                } catch (Exception e) {
                    return null;
                }
            })
            .filter(Objects::nonNull);
    }

    /**
     * @param fluxId
     * @param request
     * @return
     */
    private Stream<ItemContext> streamRaw(Integer fluxId, Request request) throws IOException {

        File locationDir = tube.getStorage().dir(fluxId).dir("source", false).toFile();
        if (!locationDir.exists()) {
            throw new RuntimeException("Impossible de localiser le dossier source");
        }

        List<File> xmlFiles = new ArrayList<>();
        Files.newDirectoryStream(locationDir.toPath(), path -> path.toString().endsWith(".xml"))
                .forEach(f -> xmlFiles.add(f.toFile()));

        if (xmlFiles.size() == 0) {
            throw new RuntimeException("Unable to find any XML sources");
        }

        String context;
        if(request.context != null && request.context.size() > 0) {
            context = request.context.remove(0);
        } else {
            context = request.expr;
            request.expr = ".";
        }

        return xmlFiles.stream()
            .map(file -> {
                try {
                    return VTDUtils.streamElements(file, context);
                } catch (Exception e) {
                    return null;
                }
            })
            .filter(Objects::nonNull)
            .flatMap(i -> i)
            .map(bytes -> {
                try {
                    ItemContext itemContext = new ItemContext();
                    itemContext.identifier = null;
                    itemContext.xmlContext = XmlContext.create(bytes);
                    return itemContext;
                } catch (Exception e) {
                    return null;
                }
            })
            .filter(Objects::nonNull);
    }

    /**
     * @param elmt
     * @return
     */
    private String getElementPath(Element elmt) {
        if(elmt.isRootElement()) {
            return null;
        }
        String path = elmt.getQualifiedName();
        if(elmt.getParentElement() != null && !elmt.getParentElement().isRootElement()) {
            Element parent = elmt.getParentElement();
            List<Element> children = parent.getChildren(elmt.getName(), elmt.getNamespace());
            if(children.size() > 1) {
                path += "[" + (children.indexOf(elmt)+1) + "]";
            }
            path = getElementPath(parent) + "/" + path;
        }
        return path;
    }

    /**
     *
     */
    static class ContextResult {
        public String context;
        public List<String> values;
    }

    static class Result {
        public String identifier;
        public List<ContextResult> data;
    }

    /**
     *
     */
    static class ItemContext {
        public String identifier;
        public XmlContext xmlContext;
    }
}