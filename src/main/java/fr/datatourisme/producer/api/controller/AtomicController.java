/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/producer/atomic")
public class AtomicController extends AbstractController {

    static class Request {
        public List<String> values;
        public Object chain;
    }

    @PostMapping("/preview")
    @ResponseBody
    public ResponseEntity<?> get(@RequestBody AtomicController.Request request) throws IOException {

        if(request.values == null) {
            return error("Il manque le paramètre \"values\"", HttpStatus.BAD_REQUEST);
        }

        if(request.chain == null) {
            return error("Il manque le paramètre \"chain\"", HttpStatus.BAD_REQUEST);
        }

        try {
            String json = new GsonBuilder().create().toJson(request.chain);
            AtomicChain chain = AtomicChain.fromJson(json);
            List<String> results = chain.process(request.values);
            return response(results);
        } catch(JsonSyntaxException e) {
            return error("Impossible de décoder le JSON.", HttpStatus.BAD_REQUEST);
        }
    }

}