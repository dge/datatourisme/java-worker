/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import com.alexkasko.springjdbc.iterable.CloseableIterator;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.producer.xml.datasource.XmlItem;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.producer.xml.xpath.XPathMapper;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping("/producer/{fluxId}/test-schema")
public class TestSchemaController extends AbstractProducerController {

    static class Request {
        public List<String> context;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @RequestBody Request request) throws JDOMException, IOException, XmlDaoFileNotFoundException {

        // initialize dao
        XmlItemDao dao = locateXmlItemDao(fluxId);

        CloseableIterator<XmlItem> iter = dao.findAll();
        XPathMapper mapper = new XPathMapper(true);

        try {
            while(iter.hasNext()) {
                XmlItem xmlItem = iter.next();
                XmlContext xmlRootContext = XmlContext.create(xmlItem.getBytes());
                contextualProcess(xmlRootContext, request.context, element -> {
                    XMLOutputter outp = new XMLOutputter();
                    try {
                        mapper.parse(outp.outputString(element).getBytes());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

            }
        } finally {
            iter.close();
        }

        return responseJson(mapper.toJson());
    }

    /**
     * @param xmlContext
     * @param contextList
     * @param process
     * @return
     */
    private void contextualProcess(XmlContext xmlContext, List<String> contextList, Consumer<Element> process) {
        if(contextList == null || contextList.size() == 0) {
            process.accept((Element) xmlContext.getContext());
        } else {
            List<String> nextContextList = new ArrayList<>(contextList);
            nextContextList.remove(0);
            try {
                xmlContext.evaluate(contextList.get(0))
                    .filter(obj -> obj instanceof Element)
                    .forEach(elmt -> contextualProcess(xmlContext.create((Element) elmt), nextContextList, process));
            } catch (QueryExpressionException e) {
                throw new RuntimeException(e);
            }
        }
    }
}