/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.Tube;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/producer/{fluxId}/xpath-schema")
public class XpathSchemaController extends AbstractController {

    @Autowired @Qualifier("producer")
    Tube tube;

    @GetMapping/*(produces = "application/json")*/
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable Integer fluxId) throws IOException {
        Storage storage = tube.getStorage().dir(fluxId).dir("xpath-schemas");
        File file = storage.file("raw.json");
        if(!file.exists()) {
            return error("Le fichier de schéma général n'existe pas.", HttpStatus.NOT_FOUND);
        }
        return responseJsonContent(file);
    }

    @GetMapping(value = "/{type}")
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable Integer fluxId, @PathVariable String type) throws IOException {
        Storage storage = tube.getStorage().dir(fluxId).dir("xpath-schemas");
        File file = storage.file(type + ".json");
        if(!file.exists()) {
            return error("Le fichier de schéma \"" + type + "\" n'existe pas.", HttpStatus.NOT_FOUND);
        }
        return responseJsonContent(file);
    }
}