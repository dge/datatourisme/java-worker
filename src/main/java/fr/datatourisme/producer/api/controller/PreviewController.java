/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.preview.ResourceReportPreview;
import fr.datatourisme.producer.processor.report.ResourceReport;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import fr.datatourisme.producer.processor.xml.XmlProcessor;
import fr.datatourisme.producer.xml.datasource.XmlItem;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.worker.Payload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/producer/{fluxId}/preview")
public class PreviewController extends AbstractProducerController {

    @Autowired
    ThesaurusAlignmentFactory thesaurusAlignmentFactory;

    @Autowired
    AlignmentFactory alignmentFactory;

    @Autowired
    private XmlProcessor xmlProcessor;

    static class Request {
        public Object thesaurus;
        public Object alignment;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable Integer fluxId, String type, Integer limit, Integer page) throws IOException, XmlDaoFileNotFoundException {

        XmlItemDao dao = locateXmlItemDao(fluxId);

        if(limit == null) {
            limit = 50;
        }

        if(page == null || page < 1) {
            page = 1;
        }
        Integer total;

        String query = "SELECT items.id, items.label, GROUP_CONCAT(attrs.value) as types " +
                "FROM items LEFT JOIN attrs ON attrs.id = items.id AND attrs.name = \"type\" ";

        List<Object> params = new ArrayList<>();

        if(type == null) {
            total = dao.count();
        } else {
            total = dao.countByAttr("type", type);
            query += " LEFT JOIN attrs AS attrs2 ON attrs2.id = items.id AND attrs2.name = \"type\" WHERE attrs2.value = ?";
            params.add(type);
        }

        query += " GROUP BY items.id ORDER BY items.label LIMIT ?,?";
        params.add((page - 1) * limit);
        params.add(limit);

        Object[] paramsArr = params.toArray(new Object[params.size()]);
        List<Map<String, Object>> items = dao.getTemplate().query(query, paramsArr,
                (rs, i) -> {
                    Map<String, Object> result = new HashMap<>();
                    result.put("id", rs.getString("id"));
                    result.put("label", rs.getString("label"));
                    result.put("types", rs.getString("types").split(","));
                    return result;
             });

        Map<String, Object> response = new HashMap<>();
        response.put("total", total);
        response.put("items", items);
        return response(response);
    }

    @PostMapping("/{id}")
    @ResponseBody
    public ResponseEntity<?> post(@PathVariable Integer fluxId, @PathVariable String id, @RequestBody Request request) throws IOException, XmlDaoFileNotFoundException {

        XmlItemDao dao = locateXmlItemDao(fluxId);

        XmlItem item = dao.findById(id);
        if(item == null) {
            return error("Impossible de trouver le POI ayant pour identifiant " + id, HttpStatus.BAD_REQUEST);
        }

        Payload payload = new Payload();

        // load thesaurus
        ThesaurusAlignment thesaurusAlignment = thesaurusAlignmentFactory.create();
        if(request.thesaurus != null) {
            payload.set("thesaurus", request.thesaurus);
            try {
                thesaurusAlignment = thesaurusAlignmentFactory.createFromJson(payload.getJsonOrStream("thesaurus"));
            } catch (Exception e) {
                return error("Impossible de charger l'alignement de thesaurus : " + e.getMessage(), HttpStatus.BAD_REQUEST);
            }
        }

        // load alignment
        Alignment alignment;
        if(request.alignment == null) {
            return response("Il manque le paramètre \"alignment\"", HttpStatus.BAD_REQUEST);
        }
        payload.set("alignment", request.alignment);
        try {
            alignment = alignmentFactory.createFromJson(thesaurusAlignment, payload.getJsonOrStream("alignment"));
        } catch (Exception e) {
            return error("Impossible de charger l'alignement : " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        try {
            ResourceReport report = xmlProcessor.process(alignment, item.getBytes());
            ResourceReportPreview preview = new ResourceReportPreview(report, alignment);
            return responseJson(preview.toJson());
        } catch (Exception e) {
            e.printStackTrace();
            return error("Impossible de générer la prévisualisation : " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}