/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.xpath;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ximpleware.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * XPathMapper
 */
public class XPathMapper {
    private Map<String, XPathMapperItem> items = new HashMap<>();

    private Boolean relativeMode;
    private VTDGen vg = new VTDGen();
    private AutoPilot ap2;
    private Map<XPathMapperItem, Integer> childrenCount;

    public XPathMapper(boolean ignoreRoot) {
        this.relativeMode = ignoreRoot;
    }

    public XPathMapper() {
        this(false);
    }

    /**
     * @param child
     */
    private void incrementChildrenCount(XPathMapperItem child) {
        childrenCount.put(child, childrenCount.getOrDefault(child, 0)+1);
    }

    /**
     * @param xmlContent
     * @throws ParseException
     * @throws NavException
     */
    public void parse(byte[] xmlContent) throws ParseException, NavException {
        childrenCount = new HashMap<>();

        vg.setDoc(xmlContent);
        vg.parse(true);
        VTDNav vn = vg.getNav();

        ap2 = new AutoPilot(vn);
        vn.toElement(VTDNav.ROOT);

        if(relativeMode) {
            visitAttributes(vn, null);
            visitChildren(vn, null);
        } else {
            visitElement(vn);
        }

        // fix min/max value of root children
        childrenCount.forEach((child, count) -> {
            child.setMinOccursIfApplicable(count);
            child.setMaxOccursIfApplicable(count);
        });
    }

    /**
     * @return String
     */
    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(getItemsAsList());
    }

    /**
     * @param file
     * @throws IOException
     */
    public void writeToFile(File file) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(toJson().getBytes());
        fos.close();
    }

    /**
     * Visit element
     */
    private void visitElement(VTDNav vn, XPathMapperItem parent) throws NavException {
        String key = vn.toString(vn.getCurrentIndex());

        // prepare an item and set in items if not exists
        XPathMapperItem item = new XPathMapperItem(key, "element", parent, relativeMode);
        item = items.getOrDefault(item.getXpath(), item);
        items.put(item.getXpath(), item);

        if(parent != null) {
            // increment children count in parent for the given key
            parent.incrementChildrenCount(item);
        } else {
            // increment children count in root for the given key
            incrementChildrenCount(item);
        }

        // reset the current item children counts
        item.getChildrenCount().replaceAll((s, i) -> 0);

        // visite attributes and children
        visitAttributes(vn, item);
        visitChildren(vn, item);

        // fix min/max value of children
        item.getChildrenCount().forEach((child, count) -> {
            child.setMinOccursIfApplicable(count);
            child.setMaxOccursIfApplicable(count);
        });

        // re-check empty if needed
        if(item.isEmpty()) {
            item.setEmpty(item.getChildrenCount().keySet().stream().allMatch(XPathMapperItem::isEmpty));
        }

        // set example value if needed
        if(vn.getText() > -1 && item.getValue() == null) {
            String text = vn.toNormalizedString(vn.getText());
            if(!text.equals("")) {
                item.setValue(text);
            }
        }
    }

    /**
     * Visit element
     */
    private void visitElement(VTDNav vn) throws NavException {
        visitElement(vn, null);
    }

    /**
     * Visit children
     */
    private void visitChildren(VTDNav vn, XPathMapperItem parent) throws NavException {
        if(vn.toElement(VTDNav.FIRST_CHILD)) {
            visitElement(vn, parent);
            while(vn.toElement(VTDNav.NEXT_SIBLING)) {
                visitElement(vn, parent);
            }
            vn.toElement(VTDNav.PARENT);
        }
    }

    /**
     * Visit attributes
     */
    private void visitAttributes(VTDNav vn, XPathMapperItem parent) throws NavException {
        int i=-1;
        ap2.selectAttr("*");
        while((i=ap2.iterateAttr())!=-1){
            String key = "@" + vn.toString(i);

            // prepare an item and set in items if not exists
            XPathMapperItem attr = new XPathMapperItem(key, "attribute", parent, relativeMode);
            attr.setMaxOccursIfApplicable(1);
            attr.setMinOccursIfApplicable(1);
            attr = items.getOrDefault(attr.getXpath(), attr);
            items.put(attr.getXpath(), attr);

            // set example value if needed
            if(attr.getValue() == null) {
                String value = vn.toString(i+1);
                if(!value.equals("")) {
                    attr.setValue(value);
                }
            }
        }
        ap2.resetXPath();
    }

    /**
     * Get items as list
     */
    public List<XPathMapperItem> getItemsAsList() {
        List<XPathMapperItem> list = new ArrayList<>(items.values());

        // set parent indexes
        list.stream().forEach(i -> {
            if(i.getParent() != null) {
                i.setParentIndex(list.indexOf(i.getParent()));
            }
        });

        return list;
    }
}