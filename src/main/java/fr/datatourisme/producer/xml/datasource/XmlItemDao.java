/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import com.alexkasko.springjdbc.iterable.CloseableIterator;
import com.alexkasko.springjdbc.iterable.IterableJdbcTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.transaction.TransactionStatus;
import org.sqlite.core.Codes;

import javax.sql.DataSource;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class XmlItemDao {

    private IterableJdbcTemplate template;
    private DataSourceTransactionManager txManager;

    /**
     * @param dataSource
     */
    private XmlItemDao(DataSource dataSource) {
        this.template = new IterableJdbcTemplate(dataSource);
        this.txManager = new DataSourceTransactionManager(dataSource);
        prepareDb();
    }

    /**
     * @param file
     * @return XmlItemDao
     */
    public static XmlItemDao instanciate(File file) {
        SingleConnectionDataSource dataSource = new SingleConnectionDataSource();
        dataSource.setDriverClassName("org.sqlite.JDBC");
        dataSource.setUrl("jdbc:sqlite:" + file.getAbsolutePath());
        return new XmlItemDao(dataSource);
    }

    /**
     * @return IterableJdbcTemplate
     */
    public IterableJdbcTemplate getTemplate() {
        return template;
    }

    /**
     * @param item
     * @return
     */
    public int insertOrReplace(XmlItem item){
        return this.template.update(
            "INSERT OR REPLACE INTO items (id, digest, label, bytes, status) VALUES (?, ?, ?, ?, ?)",
                item.getIdentifier(), item.getDigest(), item.getLabel(), item.getBytes(), item.getStatus());
    }

    /**
     * @param item
     * @return
     */
    public int update(XmlItem item){
        return this.template.update(
                "UPDATE items SET digest = ?, label = ?, bytes = ?, status = ? WHERE id = ?",
                item.getDigest(), item.getLabel(), item.getBytes(), item.getStatus(), item.getIdentifier());
    }

    /**
     * @param item
     * @return
     */
    public int delete(XmlItem item) {
        int res = 0;
        TransactionStatus txStatus = getTransactionStatus();
        try {
            this.template.update(
                    "DELETE FROM attrs WHERE id = ?",
                    item.getIdentifier());
            res = this.template.update(
                    "DELETE FROM items WHERE id = ?",
                    item.getIdentifier());
            commit(txStatus);
        } catch (DataAccessException e) {
            rollback(txStatus);
        }
        return res;
    }

    public int reset() {
        int res = 0;
        TransactionStatus txStatus = getTransactionStatus();
        try {
            this.template.update("DELETE FROM attrs");
            res = this.template.update("DELETE FROM items");
            commit(txStatus);
        } catch (DataAccessException e) {
            rollback(txStatus);
        }
        return res;
    }


    /**
     * @param name
     * @return
     */
    public int resetAttr(String name) {
        return this.template.update("DELETE FROM attrs WHERE name = ?", name);
    }

    /**
     * @param item
     * @param name
     * @param value
     * @return
     */
    public int insertAttr(XmlItem item, String name, String value) {
        try {
            return this.template.update(
                "INSERT INTO attrs (id, name, value) VALUES (?, ?, ?)",
                item.getIdentifier(), name, value);
        } catch(UncategorizedSQLException e) {
            if(e.getSQLException().getErrorCode() != Codes.SQLITE_CONSTRAINT) {
                throw e;
            }
            return 0;
        }
    }

    /**
     * @param identifier
     * @return
     */
    public XmlItem findById(String identifier) {
        try {
            return this.template.queryForObject("SELECT items.* FROM items WHERE items.id=?",
                new Object[]{identifier},
                new XmlItemMapper());
        } catch(EmptyResultDataAccessException e) {
            return null;
        }
    }

    /**
     * @return
     */
    public CloseableIterator<XmlItem> findAll() {
        return this.queryForIter("SELECT items.* FROM items");
    }

    /**
     * @param name
     * @param value
     * @return
     */
    public CloseableIterator<XmlItem> findAllByAttr(String name, String value) {
        return this.queryForIter("SELECT items.* FROM items LEFT JOIN attrs ON attrs.id = items.id WHERE attrs.name=? AND attrs.value = ? GROUP BY items.id",
            new Object[]{name, value});
    }

    /**
     * @param name
     * @return
     */
    public CloseableIterator<String> findDistinctAttrValues(String name) {
        return this.template.queryForIter("SELECT DISTINCT attrs.value FROM attrs WHERE attrs.name=?",
            new Object[]{name},
            String.class);
    }

    /**
     * @return
     */
    public Integer count() {
        return this.template.queryForObject("SELECT COUNT(*) FROM items",
            Integer.class);
    }

    public Integer countByAttr(String name, String value) {
        return this.template.queryForObject("SELECT COUNT(DISTINCT items.id) FROM items LEFT JOIN attrs ON attrs.id = items.id WHERE attrs.name=? AND attrs.value = ?",
            new Object[]{name, value},
            Integer.class);
    }

    /**
     * @param sql
     * @param rowMapper
     * @return
     */
    public <T> List<T> query(String sql, RowMapper<T> rowMapper) {
        return this.template.query(sql, rowMapper);
    }

    /**
     * @param sql
     * @return
     */
    public <T> List<XmlItem> query(String sql) {
        return this.template.query(sql, new XmlItemMapper());
    }

    /**
     * @return
     */
    public CloseableIterator<XmlItem> queryForIter(String sql) {
        return this.template.queryForIter(sql, new XmlItemMapper());
    }

    /**
     * @return
     */
    public CloseableIterator<XmlItem> queryForIter(String sql, Object[] args) {
        return this.template.queryForIter(sql, args, new XmlItemMapper());
    }

    /**
     * @return
     */
    public XmlItem queryForObject(String sql) {
        return this.template.queryForObject(sql, new XmlItemMapper());
    }

    /**
     * @return
     */
    public TransactionStatus getTransactionStatus() {
        return txManager.getTransaction(null);
    }

    /**
     * @param txStatus
     */
    public void commit(TransactionStatus txStatus) {
        txManager.commit(txStatus);
    }

    /**
     * @param txStatus
     */
    public void rollback(TransactionStatus txStatus) {
        txManager.rollback(txStatus);
    }

    /**
     * Initialise database
     */
    private void prepareDb() {
        TransactionStatus txStatus = getTransactionStatus();
        try {
            this.template.execute("CREATE TABLE IF NOT EXISTS items (\n"
                + "	id varchar(255) PRIMARY KEY,\n"
                + "	digest varchar(255) NOT NULL,\n"
                + "	label varchar(255),\n"
                + "	bytes blob,\n"
                + "	status boolean\n"
                + ");");

            this.template.execute("CREATE TABLE IF NOT EXISTS attrs (\n"
                + "	id varchar(255) NOT NULL,\n"
                + "	name varchar(255) NOT NULL,\n"
                + "	value varchar(255) NOT NULL,\n"
                + " FOREIGN KEY (id) REFERENCES items(id)\n"
                + "     ON DELETE CASCADE"
                + ");");

            this.template.execute("CREATE UNIQUE INDEX IF NOT EXISTS attrs_name_value_IDX ON attrs(id,name,value);");

            commit(txStatus);
        } catch (DataAccessException e) {
            rollback(txStatus);
            throw e;
        }
    }

    /**
     * XmlItemMapper
     */
    public class XmlItemMapper implements RowMapper<XmlItem> {
        public XmlItem mapRow(ResultSet rs, int rowNum) throws SQLException {
            XmlItem item = new XmlItem(rs.getString("id"));
            item.setDigest(rs.getString("digest"));
            item.setLabel(rs.getString("label"));
            item.setBytes(rs.getBytes("bytes"));
            item.setStatus(rs.getBoolean("status"));
            return item;
        }
    }
}
