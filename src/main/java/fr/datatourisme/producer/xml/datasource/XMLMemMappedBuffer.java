/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import com.ximpleware.extended.IByteBuffer;
import com.ximpleware.extended.ParseExceptionHuge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * XMLMemMappedBuffer maps an XML document into memory using memory map.
 * It avoids the complete loading of the document in memory thus making it
 * possible to process very large files. But the trade off is potentially
 * lower performance due to disk IO.
 */

public class XMLMemMappedBuffer implements IByteBuffer {
    MappedByteBuffer input[];
    FileChannel fc;
    RandomAccessFile raf;
    String fn;
    long length;
    public XMLMemMappedBuffer(){

    }

    public long length(){
        return length;
    }

    public byte byteAt(long index){
        return input[(int)(index>>30)].get((int)(index & 0x3fffffff));
    }

    public void readFile(String fileName) throws IOException, ParseExceptionHuge {
        File f = new File(fileName);
        fn = fileName;
        long l = f.length();
        length = l;
        if (l>= (1L<< 38)){
            throw new ParseExceptionHuge("document too big > 256 Gbyte");
        }
        raf = new RandomAccessFile(fileName, "r");
        fc  = raf.getChannel();
        int pageNumber = (int)(l>>30)+(((l & 0x3fffffffL)==0)?0:1);

        input = new MappedByteBuffer[pageNumber];
        long l2 =0;
        for (int i=0;i<pageNumber;i++){
            if (i < (pageNumber-1)){
                //bufferArray[i] = new byte[1<<30];
                input[i]= fc.map(FileChannel.MapMode.READ_ONLY, l2 ,1<<30);
                l2 = l2+(1<<30);
            }
            else{
                //bufferArray[i] = new byte[(int)l];
                input[i]= fc.map(FileChannel.MapMode.READ_ONLY, l2, l - ((long)i<<30));
            }
            //input[i] = new RandomAccessFile(fileName, "r").getChannel()
            //.map(FileChannel.MapMode.READ_ONLY, 0,(1<<32)-1);
        }
        //if (fc!=null)
        //fc.close();
        //if (raf!=null)
        //raf.close();
    }

    /**
     * NOt implemented yet
     */
    public byte[] getBytes(){
        return null;
    }

    /**
     * not implemented yet
     */
    public byte[] getBytes(int offset, int len){

        ByteBuffer bb = ByteBuffer.allocate( len );
        try {
            fc.read( bb, offset );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bb.array();
    }

    /**
     * write the segment (denoted by its offset and length) into an output file stream
     */
    public void writeToFileOutputStream(FileOutputStream ost, long os, long len)
            throws IOException{

        FileChannel ostChannel = ost.getChannel();

        fc.transferTo(os, len, ostChannel);

    }

    public void close() {

    }
}
