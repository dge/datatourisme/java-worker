/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import org.apache.commons.codec.digest.DigestUtils;

public class XmlItem {
    private String identifier;
    private String label;
    private byte[] bytes;
    private String digest;
    private Boolean status;

    public XmlItem(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
        this.setDigest(DigestUtils.md5Hex(this.bytes));
    }

    public String getDigest() {
        return digest;
    }

    public void setDigest(String digest) {
        this.digest = digest;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "XmlItem{" +
                "identifier='" + identifier + '\'' +
                //", bytes=" + Arrays.toString(bytes) +
                ", digest='" + digest + '\'' +
                '}';
    }
}
