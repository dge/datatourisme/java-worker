/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.alignment.transformer.literal.BooleanTransformer;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.utils.JDOM2Utils;
import fr.datatourisme.utils.VTDUtils;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDFS;
import org.jdom2.JDOMException;
import org.springframework.transaction.TransactionStatus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

public class XmlItemIndexer {
    private XmlItemDao dao;
    private Alignment alignment;

    public XmlItemIndexer(XmlItemDao dao, Alignment alignment) {
        this.dao = dao;
        this.alignment = alignment;
    }

    public void setDao(XmlItemDao dao) {
        this.dao = dao;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public static XmlItemIndexer instanciate(File file, Alignment alignment) {
        XmlItemDao dao = XmlItemDao.instanciate(file);
        return new XmlItemIndexer(dao, alignment);
    }

    /**
     * Number of items processed
     *
     * @param filename
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws XPathParseException
     * @throws NavException
     * @throws XPathEvalException
     * @throws JDOMException
     */
    public ProcessResult process(String filename) throws IOException, NavException, XPathParseException, XPathEvalException, JDOMException, ParseException {
        ProcessResult processResult = new ProcessResult();
        VTDUtils.streamElements(new File(filename), alignment.getContextExpr())
            .forEach(bytes -> {
                XmlContext ctx;

                try {
                    ctx = XmlContext.create(bytes);
                } catch (Exception e) {
                    return;
                }

                // ---
                // extract identifier
                // ---
                String identifier = null;
                AbstractRule identifierRule = alignment.getRule(DC.identifier);
                if(identifierRule != null) {
                    try {
                        identifier = ctx.evaluateFirstValue(identifierRule);
                    } catch (QueryExpressionException e) {}
                }
                if(StringUtils.isBlank(identifier)) {
                    processResult.stats.invalid++;
                    return;
                }
                processResult.identifiers.add(identifier);

                // ---
                // prepare item
                // ---
                XmlItem item = new XmlItem(identifier);
                item.setBytes(bytes);

                // ---
                // extract label
                // ---
                String label = null;
                AbstractRule labelRule = alignment.getRule(RDFS.label);
                if(labelRule != null) {
                    try {
                        label = ctx.evaluateFirstValue(labelRule);
                    } catch (QueryExpressionException e) { }
                }
                // create item
                if(StringUtils.isNotBlank(label)) {
                    item.setLabel(label);
                }

                // ---
                // determine deletion
                // ---
                boolean isDelete = false;
                AbstractRule isDeleteRule = alignment.getRule(DatatourismeMetadata.isObsolete);
                BooleanTransformer transformer = new BooleanTransformer();
                if(isDeleteRule != null) {
                    try {
                        isDelete = ctx.evaluateValues(isDeleteRule).anyMatch(transformer::transform);
                    } catch (QueryExpressionException e) {}
                }

                // ---
                // run dao operation
                // ---
                if(isDelete) {
                    dao.delete(item);
                    processResult.stats.delete++;
                } else {
                    dao.insertOrReplace(item);
                    processResult.stats.publishable++;
                }

                processResult.stats.total++;
            });

        return processResult;
    }

    /**
     * Handle array of files
     *
     * @param filenames
     * @throws XPathEvalException
     * @throws NavException
     * @throws XPathParseException
     * @throws ParseException
     * @throws IOException
     * @throws JDOMException
     */
    public List<ProcessResult> process(String[] filenames) throws XPathEvalException, NavException, XPathParseException, ParseException, IOException, JDOMException {
        List<ProcessResult> list = new ArrayList<>();
        for (String filename: filenames) {
            list.add(process(filename));
        }
        return list;
    }

    /**
     * @param name
     * @param expr
     */
    public void processAttr(String name, String expr) {
        dao.resetAttr(name);
        processExpr(expr, (item, list) -> {
            list.stream()
                .map(JDOM2Utils::getNodeValue)
                .filter(StringUtils::isNotEmpty)
                .forEach(s -> {
                    dao.insertAttr(item, name, s);
                });
        });
    }

    /**
     *
     * @param expr
     * @param consumer
     */
    public void processExpr(String expr, BiConsumer<XmlItem, List<Object>> consumer) {
        //XPathExpression xpathExpr = XPathFactory.instance().compile(xpath);
        dao.findAll().forEachRemaining(item -> {
            List<Object> list = new ArrayList<>();
            try {
                XmlContext ctx = XmlContext.create(item.getBytes());
                ctx.evaluate(expr).forEach(list::add);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            consumer.accept(item, list);
        });
    }

    /**
     * @return
     */
    public XmlItemDao getDao() {
        return dao;
    }

    /**
     * @return
     */
    public TransactionStatus getTransactionStatus() {
        return dao.getTransactionStatus();
    }

    /**
     * @param txStatus
     */
    public void commit(TransactionStatus txStatus) {
        dao.commit(txStatus);
    }

    /**
     * @param txStatus
     */
    public void rollback(TransactionStatus txStatus) {
        dao.rollback(txStatus);
    }

    public static class ProcessResult  {
        public Stats stats = new Stats();
        public Set<String> identifiers = new HashSet<>();
    }

    public static class Stats  {
        public int total = 0;
        public int invalid = 0;
        public int delete = 0;
        public int publishable = 0;

        public void add(Stats stats) {
            total += stats.total;
            invalid += stats.invalid;
            delete += stats.delete;
            publishable += stats.publishable;
        }
    }
}
