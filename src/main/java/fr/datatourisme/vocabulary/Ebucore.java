/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Ebucore {

    public static final Resource resource(String suffix )
    { return ResourceFactory.createResource( NS + suffix ); }

    public static final Property property(String suffix )
    { return ResourceFactory.createProperty( NS + suffix ); }

    /** <p>The namespace of the vocabulary as a string ({@value})</p> */
    public static final String NS = "http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#";

    /** <p>The namespace of the vocabulary as a string</p>
     *  @see #NS */
    public static String getURI() {return NS;}

    /** <p>The namespace of the vocabulary as a resource</p> */
    public static final Resource NAMESPACE = resource( NS );


    // Vocabulary properties
    ///////////////////////////
    public static final Property isLocatedAt = property( "isLocatedAt" );

    // Vocabulary classes
    ///////////////////////////

    // Vocabulary properties
    ///////////////////////////

    public static final Resource PlaceOfInterest = resource( "PlaceOfInterest" );
    public static final Resource EntertainmentAndEvent = resource( "EntertainmentAndEvent" );
    public static final Resource Product = resource( "Product" );
    public static final Resource Tour = resource( "Tour" );

}
