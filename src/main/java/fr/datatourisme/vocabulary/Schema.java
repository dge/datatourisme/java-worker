/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.vocabulary;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Schema {

    public static final Resource resource(String suffix )
    { return ResourceFactory.createResource( NS + suffix ); }

    public static final Property property(String suffix )
    { return ResourceFactory.createProperty( NS + suffix ); }

    /** <p>The namespace of the vocabulary as a string ({@value})</p> */
    public static final String NS = "http://schema.org/";

    /** <p>The namespace of the vocabulary as a string</p>
     *  @see #NS */
    public static String getURI() {return NS;}

    /** <p>The namespace of the vocabulary as a resource</p> */
    public static final Resource NAMESPACE = resource( NS );


    // Vocabulary classes
    ///////////////////////////

    public static final Resource GeoCoordinates = resource( "GeoCoordinates" );

    // Vocabulary properties
    ///////////////////////////

    public static final Property geo = property( "geo" );
    public static final Property longitude = property( "longitude" );
    public static final Property latitude = property( "latitude" );
    public static final Property offers = property( "offers" );
    public static final Property areaServed = property( "areaServed" );
    public static final Property startDate = property( "startDate" );
    public static final Property endDate = property( "endDate" );
    public static final Property review = property( "review" );
    public static final Property address = property( "address" );
    public static final Property email = property( "email" );
    public static final Property telephone = property( "telephone" );
    public static final Property addressLocality = property( "addressLocality" );
    public static final Property legalName = property( "legalName" );
    public static final Property postalCode = property( "postalCode" );

}
