/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import fr.datatourisme.assembler.AssemblerConfiguration;
import fr.datatourisme.bundle.BundleConfiguration;
import fr.datatourisme.dataset.DatasetConfiguration;
import fr.datatourisme.messaging.MessagingConfiguration;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.WorkerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.io.File;

@SpringBootConfiguration
@Import({
    AssemblerConfiguration.class,
    WorkerConfiguration.class,
    fr.datatourisme.producer.processor.ProcessorConfiguration.class,
    BundleConfiguration.class,
    DatasetConfiguration.class,
    MessagingConfiguration.class
})
public class Configuration {
    @Bean(name = "storage")
    public Storage createStorage(@Value( "${datatourisme.data.path}" ) String dataPath) {
        return new Storage(new File(dataPath));
    }
}
