/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import org.apache.jena.ext.com.google.common.base.Function;
import org.apache.jena.ext.com.google.common.base.Supplier;
import org.apache.jena.ext.com.google.common.base.Suppliers;
import org.apache.jena.ext.com.google.common.cache.CacheBuilder;
import org.apache.jena.ext.com.google.common.cache.CacheLoader;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.Lock;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OntologyIndividual extends OntologyResource {

    // cached suppliers/functions
    private Supplier<Map<OntologyProperty, List<Object>>> listPropertiesValues = Suppliers.memoize(this::getPropertiesValues);
    private Function<Boolean, List<OntologyClass>> listTypes = CacheBuilder.newBuilder().build(CacheLoader.from(this::getTypes));
    private Function<OntologyProperty, List<RDFNode>> listPropertyValues = CacheBuilder.newBuilder().build(CacheLoader.from(this::getPropertyValues));

    /**
     * Constructor
     *
     * @param ontology
     * @param individual
     */
    public OntologyIndividual(fr.datatourisme.ontology.Ontology ontology, Individual individual) {
        super(ontology, individual);
    }

    /**
     * List types
     *
     * @return Stream
     */
    public Stream<OntologyClass> listTypes(Boolean direct) {
        return listTypes.apply(direct).stream();
    }

    /**
     * @return
     */
    private List<OntologyClass> getTypes(Boolean direct) {
        Stream<OntologyClass> stream;
        if(direct) {
            ontResource.getOntModel().enterCriticalSection(Lock.READ);
            try {
                stream = ontResource.listRDFTypes(true).toList().stream()
                    .filter(res -> !res.isAnon())
                    .map(res -> res.as(OntClass.class))
                    .map(c -> { return ontology.createClass(c); });
            } finally {
                ontResource.getOntModel().leaveCriticalSection();
            }
        } else {
            stream = Stream.concat(
                listTypes(true),
                listTypes(true).flatMap(c -> c.listSuperClasses(false))
            );
        }
        return stream
            .filter(res -> !res.hasURI(OWL.getURI() + "NamedIndividual")) // encore utile ?
            .collect(Collectors.toList());
    }

    /**
     * List types
     *
     * @return Stream
     */
    public Stream<Map.Entry<OntologyProperty, List<Object>>> listPropertiesValues() {
        return listPropertiesValues.get().entrySet().stream();
    }

    /**
     * @return
     */
    private Map<OntologyProperty, List<Object>> getPropertiesValues() {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            Map<OntologyProperty, List<Object>> map = new HashMap<>();
            ontResource.listProperties().toList().stream()
                .filter(stmt -> !stmt.getObject().isAnon())
                .filter(stmt ->
                    !stmt.getPredicate().isAnon()
                        && !stmt.getPredicate().equals(RDF.type)
                        && !stmt.getPredicate().equals(RDFS.label)
                        && !stmt.getPredicate().equals(RDFS.comment)
                )
                .forEach(stmt -> {
                    Object value;
                    if(stmt.getObject().isResource()) {
                        value = stmt.getObject().asResource();
                    } else {
                        value = stmt.getObject().toString();
                    }
                    OntologyProperty property = ontology.createProperty(stmt.getPredicate().as(OntProperty.class));
                    map.computeIfAbsent(property, s -> new ArrayList<>()).add(value);
                });
            return map;
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }

    /**
     * Return true if the individual is an instance of uri
     *
     * @param uri
     * @return
     */
    public boolean isInstanceOf(String uri) {
        return listTypes(false)
            .anyMatch(c -> c.hasURI(uri));
    }

    public boolean isInstanceOf(OntologyClass res) {
        return isInstanceOf(res.getURI());
    }

    public boolean isInstanceOf(Resource res) {
        return isInstanceOf(res.getURI());
    }

    /**
     * List property values for a given property
     *
     * @return Stream
     */
    public Stream<RDFNode> listPropertyValues(OntologyProperty property) {
        return listPropertyValues.apply(property).stream();
    }

    /**
     * List property values for a given property
     *
     * @return
     */
    private List<RDFNode> getPropertyValues(OntologyProperty property) {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            Stream<RDFNode> stream = ontResource.listPropertyValues(property.getOntResource().asProperty()).toList().stream();
            return  stream.collect(Collectors.toList());
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }
}
