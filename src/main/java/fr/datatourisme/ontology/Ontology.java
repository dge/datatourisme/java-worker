/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.Lock;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.shared.impl.PrefixMappingImpl;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Ontology {

    private OntModel ontModel;

    public static String defaultLanguage = "fr";

    // cached maps
    private Map<String, OntologyClass> _classes = new HashMap<>();
    private Map<String, OntologyProperty> _properties = new HashMap<>();
    private Map<String, OntologyIndividual> _individuals = new HashMap<>();

    /**
     * Constructor
     *
     * @param ontModel
     */
    public Ontology(OntModel ontModel) {
        this.ontModel = ontModel;
        this.loadClasses();
        this.loadProperties();
        this.loadIndividuals();
    }

    public OntModel getOntModel() {
        return ontModel;
    }

    public String expandPrefix(String prefixed) {
        return ontModel.expandPrefix(prefixed);
    }

    public String shortForm(String uri) {
        return ontModel.shortForm(uri);
    }

    public PrefixMapping getPrefixMapping() { return new PrefixMappingImpl().setNsPrefixes(ontModel.getNsPrefixMap()); }

    /**
     * Get a resource
     *
     * @return
     */
    public OntologyResource getResource(String uri) {

        if(_classes.containsKey(uri)) {
            return _classes.get(uri);
        } else if(_properties.containsKey(uri)) {
            return _properties.get(uri);
        } else if(_individuals.containsKey(uri)) {
            return _individuals.get(uri);
        }

        ontModel.enterCriticalSection(Lock.READ);
        try {
            Resource res = ontModel.createResource(uri);
            if(ontModel.containsResource(res)) {
                if(res.canAs(OntClass.class)) {
                    return createClass(res.as(OntClass.class));
                }
                if(res.canAs(OntProperty.class)) {
                    return createProperty(res.as(OntProperty.class));
                }
                if(res.canAs(Individual.class)) {
                    return createIndividual(res.as(Individual.class));
                }
            }
            return null;
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    /*
     * ---------------------
     * =      CLASSES      =
     * ---------------------
     */

    /**
     * Create class
     *
     * @param ontClass
     * @return
     */
    public OntologyClass createClass(OntClass ontClass) {
        return this._classes.computeIfAbsent(ontClass.getURI(), uri -> {
            return new OntologyClass(this, ontClass);
        });
    }

    /**
     * loadClasses
     */
    protected void loadClasses() {
        ontModel.enterCriticalSection(Lock.READ);
        try {
            ontModel.listNamedClasses().toList().stream()
                .filter(ontClass -> !ontClass.isAnon())
                .forEach(this::createClass);
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    /**
     * @return
     */
    private List<OntologyClass> getClasses() {
        return new ArrayList<>(_classes.values());
    }

    /**
     * List classes
     *
     * @return
     */

    public Stream<OntologyClass> listClasses() {
        return _classes.values().stream();
    }

    /**
     * Get class
     *
     * @return
     */
    public OntologyClass getClass(String uri) {
        if(_classes.containsKey(uri)) {
            return _classes.get(uri);
        }
        ontModel.enterCriticalSection(Lock.READ);
        try {
            OntClass ontClass = ontModel.getOntClass(uri);
            if(ontClass != null) {
                return createClass(ontClass);
            }
            return null;
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    public OntologyClass getClass(Resource res) {
        return getClass(res.getURI());
    }

    /**
     * Has class
     *
     * @return
     */
    public boolean hasClass(String uri) {
        return _classes.containsKey(uri);
    }

    /*
     * ---------------------
     * =    PROPERTIES     =
     * ---------------------
     */

    /**
     * Create property
     *
     * @return
     */
    public OntologyProperty createProperty(OntProperty ontProperty) {
        return this._properties.computeIfAbsent(ontProperty.getURI(), uri -> {
            return new OntologyProperty(this, ontProperty);
        });
    }

    /**
     * loadProperties
     */
    protected void loadProperties() {
        ontModel.enterCriticalSection(Lock.READ);
        try {
            ontModel.listAllOntProperties().toList()
                .forEach(this::createProperty);
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    /**
     * getPropertiesList
     *
     * @return
     */
    private List<OntologyProperty> getProperties() {
        return new ArrayList<>(_properties.values());
    }

    /**
     * List properties
     *
     * @return
     */
    public Stream<OntologyProperty> listProperties() {
        return _properties.values().stream();
    }

    /**
     * Get property
     *
     * @return
     */
    public OntologyProperty getProperty(String uri) {
        if(_properties.containsKey(uri)) {
            return _properties.get(uri);
        }
        ontModel.enterCriticalSection(Lock.READ);
        try {
            OntProperty prop = ontModel.getOntProperty(uri);
            if(prop != null) {
                return createProperty(prop);
            }
            return null;
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    public OntologyProperty getProperty(Resource res) {
        return getProperty(res.getURI());
    }

    /**
     * Has propertie
     *
     * @return
     */
    public boolean hasProperty(String uri) {
        return _properties.containsKey(uri);
    }

    /*
     * ---------------------
     * =    INDIVIDUALS    =
     * ---------------------
     */

    /**
     * Create individual
     *
     * @return
     */
    public OntologyIndividual createIndividual(Individual individual) {
        return this._individuals.computeIfAbsent(individual.getURI(), uri -> {
            return new OntologyIndividual(this, individual);
        });
    }

    /**
     * loadIndividuals
     */
    protected void loadIndividuals() {
        ontModel.enterCriticalSection(Lock.READ);
        try {
            Resource owlClass = ResourceFactory.createResource( OWL.getURI() + "NamedIndividual" );
            ontModel.listIndividuals(owlClass).toList()
                .forEach(this::createIndividual);
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    /**
     * getClassesList
     * @return
     */
    private List<OntologyIndividual> getIndividuals() {
        return new ArrayList<>(_individuals.values());
    }

    /**
     * List individual
     *
     * @return
     */
    public Stream<OntologyIndividual> listIndividuals() {
        return _individuals.values().stream();
    }

    /**
     * Get individual
     *
     * @return
     */
    public OntologyIndividual getIndividual(String uri) {
        if(_individuals.containsKey(uri)) {
            return _individuals.get(uri);
        }
        ontModel.enterCriticalSection(Lock.READ);
        try {
            Resource res = ontModel.createResource(uri);
            Resource owlClass = ResourceFactory.createResource( OWL.getURI() + "NamedIndividual" );
            if(ontModel.containsResource(res) && res.canAs(Individual.class) && res.hasProperty(RDF.type, owlClass)) {
                return createIndividual(res.as(Individual.class));
            }
            return null;
        } finally {
            ontModel.leaveCriticalSection() ;
        }
    }

    public OntologyIndividual getIndividual(Resource res) {
        return getIndividual(res.getURI());
    }

    /**
     * Has individual
     *
     * @return
     */
    public boolean hasIndividual(String uri) {
        return _individuals.containsKey(uri);
    }
}
