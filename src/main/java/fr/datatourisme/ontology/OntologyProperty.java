/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import org.apache.jena.ext.com.google.common.base.Supplier;
import org.apache.jena.ext.com.google.common.base.Suppliers;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.shared.Lock;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OntologyProperty extends OntologyResource {

    // cached suppliers/functions
    private Supplier<List<OntologyClass>> listRanges = Suppliers.memoize(this::getRanges);

    boolean isFunctional;
    boolean isDatatype;
    boolean isObject;

    /**
     * Constructor
     *
     * @param ontology
     * @param ontProperty
     */
    public OntologyProperty(Ontology ontology, OntProperty ontProperty) {
        super(ontology, ontProperty);
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            isFunctional = ontResource.as(OntProperty.class).isFunctionalProperty();
            isDatatype = ontResource.isDatatypeProperty();
            isObject = ontResource.isObjectProperty();
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }

    /**
     * List super classes
     *
     * @return Stream
     */
    public Stream<OntologyClass> listRanges() {
        return listRanges.get().stream();
    }

    /**
     * @return
     */
    private List<OntologyClass> getRanges() {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            return ((OntProperty)ontResource).listRange().toList().stream()
                //.filter(OntResource::isClass)
                .map(OntResource::asClass)
                .flatMap(c -> {
                    // if the range is an union class, flat map operands
                    if(c.isUnionClass()) {
                        return c.asUnionClass().listOperands().toList().stream();
                    }
                    return Stream.of(c);
                })
                .filter(c -> !c.isAnon())
                .map(c -> ontology.createClass(c))
                .collect(Collectors.toList());
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }

    /**
     * Return arbitrary range
     *
     * @return
     */
    public OntologyResource getRange() {
        return listRanges().findFirst().orElse(null);
    }

    /**
     * is functional
     *
     * @return Boolean
     */
    public Boolean isFunctional() {
        return isFunctional;
    }

    /**
     * is datatype
     *
     * @return Boolean
     */
    public Boolean isDatatype() {
        return isDatatype;
    }

    /**
     * is object
     *
     * @return Boolean
     */
    public Boolean isObject() {
        return isObject;
    }
}


