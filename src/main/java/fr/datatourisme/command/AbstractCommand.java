/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import org.apache.log4j.Logger;
import org.nanocom.console.command.Command;
import org.nanocom.console.input.InputInterface;
import org.nanocom.console.output.OutputInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

@Import(fr.datatourisme.Configuration.class)
public abstract class AbstractCommand extends Command implements CommandLineRunner, ExitCodeGenerator {
    protected Logger logger = Logger.getLogger(getClass());

    int exitCode = 0;
    abstract int run();

    @Autowired
    OutputInterface output;

    @Autowired
    InputInterface input;

    @Override
    protected int execute(InputInterface input, OutputInterface output) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(this.getClass())
            .bannerMode(Banner.Mode.CONSOLE)
            .web(getWebEnv());

        if (getProfile() != null) {
            builder.profiles(getProfile());
        }

        builder.initializers((ApplicationContextInitializer<ConfigurableApplicationContext>) configurableApplicationContext -> {
            ConfigurableListableBeanFactory factory = configurableApplicationContext.getBeanFactory();
            factory.registerSingleton("inputInterface", input);
            factory.registerSingleton("outputInterface", output);
        });

        ConfigurableApplicationContext ctx = builder.run();
        return SpringApplication.exit(ctx);
    }

    public boolean getWebEnv() {
        return false;
    }

    public String getProfile() {
        return null;
    }

    @Override
    public void run(String... args) {
        exitCode = run();
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
