/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.bundle.ResourceStoreBuilder;
import fr.datatourisme.dataset.GraphStoreManager;
import fr.datatourisme.ontology.Ontology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.io.IOException;

@Import(fr.datatourisme.Configuration.class)
public class BuildBundleResourceStore extends AbstractCommand {

    @Autowired
    GraphStoreManager graphStoreManager;

    @Autowired
    Ontology ontologyLite;

    @Value( "${datatourisme.bundlestore.path}" )
    String storePath;

    @Override
    protected void configure() {
        setName("build-bundle-resource-store");
        setDescription("Build the bundle store");
    }

    @Override
    public int run() {
        GraphStore store = null;
        try {
            store = graphStoreManager.openReadOnly(3000);
            ResourceStoreBuilder builder = new ResourceStoreBuilder(store, ontologyLite);
            builder.build(storePath);
            store.close();
        } catch (GraphStoreException | IOException e) {
            e.printStackTrace();
            return 1;
        } finally {
            if (store != null) {
                store.close();
            }
        }
        return 0;
    }
}
