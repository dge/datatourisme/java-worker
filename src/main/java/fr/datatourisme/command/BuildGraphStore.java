/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import com.conjecto.graphstore.GraphLoader;
import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.conjecto.graphstore.exception.GraphStoreException;
import fr.datatourisme.dataset.BlazegraphRestClient;
import fr.datatourisme.dataset.GraphStoreManager;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.nanocom.console.input.InputArgument;
import org.nanocom.console.input.InputParameterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.net.URISyntaxException;

@Import(fr.datatourisme.Configuration.class)
public class BuildGraphStore extends AbstractCommand {

    @Value("${datatourisme.graphstore.path}")
    private String defaultStorePath;

    @Autowired
    BlazegraphRestClient datasetRestClient;

    @Override
    protected void configure() {
        setName("build-graphstore");
        setDescription("Reset & build the graph store");
        setDefinition(new InputParameterInterface[] {
            // arguments
            new InputArgument("path", InputArgument.OPTIONAL, "Path to the store (if absent, use the default path)")
        });
    }

    @Override
    public int run() {
        GraphStore store;
        long lockTimeout = 5 * 1000; // 5s
        String path = (String) input.getArgument("path");
        if (path == null) {
            path = defaultStorePath;
        }
        GraphStoreManager graphStoreManager = new GraphStoreManager(path);
        try (CloseableHttpResponse response = datasetRestClient.getStmts()) {

            if (graphStoreManager.exists()) {
                logger.info("Try to acquire the store lock...");
                store = openStore(graphStoreManager, lockTimeout);
                logger.info("Delete the store...");
                store.close();
                graphStoreManager.delete();
            }

            logger.info("Initialize the store...");
            store = openStore(graphStoreManager, lockTimeout);

            logger.info("Load statements into the store...");
            GraphLoader loader = new GraphLoader(store, "nt");
            loader.load(response.getEntity().getContent());
            store.compact();

        } catch (IOException | URISyntaxException | GraphStoreException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    /**
     * @param timeout
     * @return
     * @throws InterruptedException
     */
    private GraphStore openStore(GraphStoreManager graphStoreManager, long timeout) throws GraphStoreException {
        GraphStoreOptions options = (new GraphStoreOptions())
            .setDisablePOSIndex(true)
            .setCreateIfMissing(true);
        return graphStoreManager.open(options, timeout);
    }
}
