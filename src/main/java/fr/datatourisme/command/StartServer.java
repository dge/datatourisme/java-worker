/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import fr.datatourisme.api.ApiConfiguration;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

@Import({fr.datatourisme.Configuration.class, ApiConfiguration.class})
public class StartServer extends AbstractCommand {

    @Autowired
    ApplicationContext context;

    @Autowired
    SimpleMessageListenerContainer pipelineSinkListenerContainer;

    @Autowired
    SimpleMessageListenerContainer pipelineSourceListenerContainer;

    @Override
    public boolean getWebEnv() {
        return true;
    }

    @Override
    public String getProfile() {
        return "server";
    }

    @Override
    protected void configure() {
        setName("start-server");
        setDescription("Start a server to run commands");
    }

    @Override
    public int run() {
        logger.info("Start AMQP listeners...");
        pipelineSinkListenerContainer.start();
        pipelineSourceListenerContainer.start();

        logger.info("Listening for connections...");
        while(true) {
            try {
                Thread.sleep(1000 * 120);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
