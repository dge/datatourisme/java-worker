/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import org.springframework.context.annotation.Import;

@Import(fr.datatourisme.Configuration.class)
public class HelloWorld extends AbstractCommand {

    @Override
    protected void configure() {
        setName("hello-world");
        setDescription("test command");
    }

    @Override
    public int run() {
        output.writeln("hello world !");
        return 0;
    }

}
