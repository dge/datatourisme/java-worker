/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.webapp.api.message;

import fr.datatourisme.worker.Job;

import java.util.Map;

public class JobStatusMessage {
    String uuid;
    Integer flux;
    String task;
    Map<String, String> stats;
    public JobStatusMessage(Job job) {
        uuid = job.getUuid();
        flux = (Integer) job.getPayload().get("flux").orElse(0);
        task = (String) job.getPayload().get("task").orElse(null);
        stats = job.getStats();
    }
}
