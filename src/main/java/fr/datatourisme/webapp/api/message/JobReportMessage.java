/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.webapp.api.message;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import fr.datatourisme.worker.Job;
import fr.datatourisme.worker.logger.reporter.JobReporter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class JobReportMessage extends JobStatusMessage {
    JsonElement reports;
    //String report;

    public JobReportMessage(Job job) throws IOException {
        super(job);
        // get the report
        File file = JobReporter.getReporter(job).getFile();
        if(file.exists()) {
            String content = FileUtils.readFileToString(file);
            reports = new JsonParser().parse(content);
        }
    }
}
