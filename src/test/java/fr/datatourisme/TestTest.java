/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TestTest {
    @Test
    public void jenaQueryTest() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        Calendar cal = Calendar.getInstance();
        Literal l = model.createTypedLiteral(cal);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Literal l2 = model.createTypedLiteral(dateFormat.format(cal.getTime()), XSDDatatype.XSDdate);
    }
}
