/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset;

import fr.datatourisme.AbstractTest;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * fr.datatourisme.dataset
 */
public class DatatourismeRepositoryTest extends AbstractTest {

    @Autowired
    DatatourismeRepository datasetRepository;

    @Test
    public void selectResourcesUsages() throws Exception {
        long startTime = System.currentTimeMillis();

        Set<Resource> resources = new HashSet<>();
        resources.add(new ResourceImpl("http://www.datatourisme.fr/data/bd9debab-4523-3864-8296-e4de962c756b"));

        Map<Resource, Set<Resource>> resourcesUsages = datasetRepository.selectResourcesUsages(resources);
        long total = System.currentTimeMillis() - startTime;
        System.out.println(total);
    }
}