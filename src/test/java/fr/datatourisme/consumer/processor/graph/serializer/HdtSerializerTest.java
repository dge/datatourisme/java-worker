/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph.serializer;

import com.conjecto.graphstore.GraphLoader;
import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import fr.datatourisme.TestTest;
import fr.datatourisme.vocabulary.Datatourisme;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.options.HDTSpecification;
import org.rdfhdt.hdt.triples.IteratorTripleString;
import org.rdfhdt.hdt.triples.TripleString;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * fr.datatourisme.consumer.processor.graph.serializer
 */
public class HdtSerializerTest {

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    @Test
    public void serializeTest() throws Exception {

        URL ntFile = TestTest.class.getClassLoader().getResource("samples/flux-721-201803161412.nt");
        FileInputStream inputStream = new FileInputStream(ntFile.getFile());
        File dbDir = temp.newFolder();

        File hdtOriginalFile = new File(temp.getRoot(), "original.hdt");
        File hdtOutputFile = new File(temp.getRoot(), "output.hdt");

        // Create original HDT from RDF file
        HDT hdtOriginal = HDTManager.generateHDT(ntFile.getFile(), Datatourisme.getURI(), RDFNotation.parse("nt"), new HDTSpecification(), null);
        hdtOriginal.saveToHDT(hdtOriginalFile.getAbsolutePath(), null);

        // Create own hdt
        GraphStoreOptions options = (new GraphStoreOptions()).setCreateIfMissing(true);
        GraphStore store = GraphStore.open(dbDir.getAbsolutePath(), options);

        try {
            // load graph
            GraphLoader loader = new GraphLoader(store, "nt");
            loader.load(inputStream);
            store.compact();
            inputStream.close();
            // serialize graph
            HdtSerializer serializer = new HdtSerializer();
            serializer.serialize(new FileOutputStream(hdtOutputFile), store, null);
        } finally {
            store.close();
        }

        HDT hdtFinal = HDTManager.loadHDT(hdtOutputFile.getAbsolutePath(), null);

        // same size
        assertEquals(hdtOriginal.getTriples().getNumberOfElements(), hdtFinal.getTriples().getNumberOfElements());

        // each item from original is in final
        IteratorTripleString iter = hdtOriginal.search("", "", "");
        IteratorTripleString iter2;
        while(iter.hasNext()) {
            TripleString triple = iter.next();
            iter2 = hdtFinal.search(triple.getSubject(), triple.getPredicate(), "");
            assertTrue(iter2.hasNext());
        }
    }
}