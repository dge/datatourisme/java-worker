/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import fr.datatourisme.AbstractTest;
import fr.datatourisme.vocabulary.Datatourisme;
import org.junit.Before;

abstract public class AbstractTaskTest extends AbstractTest {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    public ExecuteQueryTask getConstructExecuteQueryTask() {
        ExecuteQueryTask task = createTask(ExecuteQueryTask.class);
        task.setPartial(false);
        task.setQuery("CONSTRUCT {?s a <urn:resource>} WHERE { ?s a <"+ Datatourisme.PointOfInterest.getURI() +"> } LIMIT 1000");
        return task;
    }

    public ExecuteQueryTask getSelectExecuteQueryTask() {
        ExecuteQueryTask task = createTask(ExecuteQueryTask.class);
        task.setPartial(false);
        task.setQuery("SELECT ?s ?p ?o WHERE {?s ?p ?o} LIMIT 10000");
        return task;
    }

    public ProcessGraphResultTask getProcessGraphResultTask(String format) {
        ProcessGraphResultTask task = createTask(ProcessGraphResultTask.class);
        task.setPartial(false);
        task.setFormat(format);
        return task;
    }

    public ProcessGraphResultTask getProcessGraphResultTask() {
        return getProcessGraphResultTask("nt");
    }

    public ProcessTupleResultTask getProcessTupleResultTask() {
        ProcessTupleResultTask task = createTask(ProcessTupleResultTask.class);
        task.setPartial(false);
        task.setFormat("csv");
        return task;
    }

    public PublishDatagouvTask getPublishDataGouvTask() {
        PublishDatagouvTask task = createTask(PublishDatagouvTask.class);
        task.setApiKey("eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiNTY2NmNmMTJjNzUxZGY2MjMzYzY2NGMwIiwidGltZSI6MTQ1OTI3ODk0My41NzE3NDR9.gLSEd5pj95eYA_y8Be-_TRznVeEVCiTtnjDRtDOEks8");
        task.setDataset("datatourisme");
        task.setFluxIdentifier(9999);
        return task;
    }
}
