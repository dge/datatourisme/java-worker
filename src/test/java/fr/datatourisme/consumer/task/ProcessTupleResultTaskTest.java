/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertTrue;

/**
 * fr.datatourisme.consumer.task
 */
public class ProcessTupleResultTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        getSelectExecuteQueryTask().run();
        getProcessTupleResultTask().run();
        assertTrue(storage.dir("output").file("out.complete.csv").exists());
        assertTrue(storage.dir("output").file("out.complete.csv").length() > 0);
    }

    @Test
    public void testLanguageExecute() throws Exception {
        getSelectExecuteQueryTask().run();

        Set<String> languages = new HashSet<>(Arrays.asList("en"));
        ProcessTupleResultTask task2 = getProcessTupleResultTask();
        task2.setLanguages(languages);
        task2.run();

        InputStream fileStream = new FileInputStream(storage.dir("output").file("out.complete.csv"));
        InputStream gzipStream = new GZIPInputStream(fileStream);
        BufferedReader buffered = new BufferedReader(new InputStreamReader(gzipStream));

        String line;
        while ((line = buffered.readLine()) != null) {
            if(line.contains("restauration traditionnelle")) {
                assertTrue(false);
            }
        }
        buffered.close();
        assertTrue(true);
    }
}