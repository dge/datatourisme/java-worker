/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * fr.datatourisme.consumer.task
 */
public class ExecuteQueryTaskTest extends AbstractTaskTest {

    @Test
    public void testConstructExecute() throws Exception {
        getConstructExecuteQueryTask().run();
        assertTrue(storage.dir("_graph", false).toFile().exists());
//        GraphStore store = GraphStore.open(storage.dir("_graph", false).toFile().getAbsolutePath());
//        store.querySPO().forEachRemaining(t -> {
//            System.out.println(t.toString());
//        });
    }

    @Test
    public void testSelectExecute() throws Exception {
        getSelectExecuteQueryTask().run();
        assertTrue(storage.file("_tuples.bin").exists());
    }
}