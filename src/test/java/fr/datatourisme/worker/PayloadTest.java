/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;


import fr.datatourisme.worker.exception.InvalidPayloadException;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PayloadTest {

    @Test
    @Ignore
    public void testFromJson() throws IOException, InvalidPayloadException {
        URL payloadJson = this.getClass().getClassLoader().getResource("payload.json");
        byte[] encoded = Files.readAllBytes(Paths.get(payloadJson.getFile()));
        Payload payload = Payload.fromJson(new String(encoded));
        System.out.println(payload.toJson());
    }
}
