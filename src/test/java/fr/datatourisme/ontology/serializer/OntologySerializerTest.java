/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology.serializer;

import fr.datatourisme.AbstractTest;
import fr.datatourisme.ontology.Ontology;
import junitx.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class OntologySerializerTest extends AbstractTest {

    @Autowired
    protected ApplicationContext context;

    @Test
    public void serialize() throws Exception {
        Ontology ontology = (Ontology)context.getBean("ontology");
        String json = OntologySerializer.serialize(ontology);
        Assert.assertNotNull(json);
        //System.out.println(OntologySerializer.serialize(ontology, true));
    }

    @Test
    public void serializeAlignment() throws Exception {
        Ontology ontology = (Ontology)context.getBean("ontologyAlignment");
        String json = OntologySerializer.serialize(ontology);
        Assert.assertNotNull(json);
    }
    @Test
    @Ignore
    public void test() throws Exception {
        Ontology ontology = (Ontology)context.getBean("ontologyLite");
        ontology.listClasses().forEach(System.out::println);
    }
}