/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

public class GeoLatLonBuiltinTest {
    @Test
    public void testBodyCall() {
        BuiltinRegistry.theRegistry.register(new GeoLatLon());

        String rule =
        "[latlon:  (?P http://schema.org/geo ?g), (?g http://schema.org/longitude ?lon), (?g http://schema.org/latitude ?lat) ->\n" +
        "    [(?P http://example.org/latlon ?t) <- geolatlon(?lat, ?lon, ?t)]\n" +
        "]";

        Reasoner reasoner = new GenericRuleReasoner(Rule.parseRules(rule));

        String rdf = "@prefix ex: <http://example.org/>.\n" +
                "@prefix schema: <http://schema.org/>.\n" +
                "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" +
                "ex:test schema:geo [ " +
                "   schema:latitude \"48.1119800\"^^xsd:float; " +
                "   schema:longitude \"-1.6742900\"^^xsd:float " +
                "].";

        Model model = ModelFactory.createDefaultModel();
        model.read(new ByteArrayInputStream(rdf.getBytes()), null, "TTL");

        InfModel extraInfModel = ModelFactory.createInfModel(reasoner, model);

        Resource res = extraInfModel.getResource("http://example.org/test");
        Assert.assertEquals("48.1119800#-1.6742900", res.getRequiredProperty(extraInfModel.getProperty("http://example.org/latlon")).getLiteral().getLexicalForm());
    }
}
