/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

public class BootApplicationTest extends AbstractTest {

    @Autowired
    ApplicationContext context;

    @Test
    public void testContextLoads() throws Exception {
        assertThat(context).isNotNull();
        assertThat(context.containsBean("getBeanstalkClientFactory")).isTrue();
        assertThat(context.containsBean("ontology")).isTrue();
        assertThat(context.containsBean("ontologyLite")).isTrue();
    }
}
