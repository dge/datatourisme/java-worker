/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import fr.datatourisme.Application;
import org.junit.Ignore;
import org.junit.Test;

/**
 * fr.datatourisme.command
 */
public class SerializeOntologyTest {
    @Test
    @Ignore
    public void testCommand() {
        String[] args = {"serialize-ontology", ""};
        Application.main(args);
    }
}