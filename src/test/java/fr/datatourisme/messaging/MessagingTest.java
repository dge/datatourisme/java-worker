/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.messaging;

import fr.datatourisme.AbstractTest;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;

public class MessagingTest extends AbstractTest {

    @Autowired
    SimpleMessageListenerContainer pipelineSinkListenerContainer;

    @Autowired
    SimpleMessageListenerContainer pipelineSourceListenerContainer;

    @Test
    @Ignore
    public void testMessaging() {
        pipelineSinkListenerContainer.start();
        pipelineSourceListenerContainer.start();
        while (true) {
            try {
                Thread.sleep(1000 * 120);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
