/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import com.ximpleware.ParseException;
import fr.datatourisme.producer.AbstractProducerTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.transaction.TransactionStatus;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Unit test for XmlItemDaoTest
 */
public class XmlItemIndexerTest extends AbstractProducerTest {
    private XmlItemIndexer indexer;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        indexer = XmlItemIndexer.instanciate(getTempFile("datasource.index"), getAlignement());
    }

    /**
     * testProcess
     */
    @Test
    public void testProcess() throws IOException {
        File datasource = getXmlSource();
        TransactionStatus txStatus = indexer.getTransactionStatus();
        try {
            indexer.process(datasource.getAbsolutePath());
            indexer.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            indexer.rollback(txStatus);
        }
        assertIterCount(50, indexer.getDao().findAll());
    }

    /**
     * testProcess bad file
     */
    @Test
    public void testProcessBad() throws Exception {
        URL datasource = this.getClass().getClassLoader().getResource("fixtures/xml/datasource/datasource_bad.xml");
        TransactionStatus txStatus = indexer.getTransactionStatus();
        exception.expect(ParseException.class);

        try {
            indexer.process(datasource.getFile());
            indexer.commit(txStatus);
        } catch(Exception e) {
            indexer.rollback(txStatus);
            throw e;
        }
    }

    /**
     * testProcessAttr
     */
    @Test
    public void testProcessAttr() throws IOException {
        testProcess();

        TransactionStatus txStatus = indexer.getTransactionStatus();
        try {
            indexer.processAttr("type", "@type|*[starts-with(local-name(), 'informations')]/*/elementReference/@id");
            indexer.commit(txStatus);
        } catch (Exception e) {
            e.printStackTrace();
            indexer.rollback(txStatus);
        }

        assertIterCount(13, indexer.getDao().findAllByAttr("type", "PATRIMOINE_CULTUREL"));
    }
}
