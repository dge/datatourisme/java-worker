/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.reasoner;

import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.Schema;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

public class ValidationResonerTest extends AbstractProducerTest {

    @Autowired
    InferenceReasoner inferenceReasoner;

    @Autowired
    ValidationReasoner validationReasoner;

    @Test
    public void testBugDeLEspace() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.PlaceOfInterest)
            .addProperty(DC.identifier, "test")
            .addProperty(Datatourisme.isLocatedAt, model.createResource()
                .addProperty(RDFS.comment, "test"));

        inferenceReasoner.process(model);

        ValidityReport validityReport = validationReasoner.validate(resource);

        assertIterCount(5, validityReport.getReports());
    }

    @Test
    public void testInterference() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("urn:poi")
                .addProperty(RDF.type, Datatourisme.PointOfInterest)
                .addProperty(DC.identifier, "test")
                .addProperty(RDFS.label, "POI");
        inferenceReasoner.process(model);
        ValidityReport validityReport = validationReasoner.validate(resource);
        //model.listStatements().forEachRemaining(System.out::println);
        assertIterCount(4, validityReport.getReports());

        Model model2 = ModelFactory.createDefaultModel();
        resource = model2.createResource("urn:poi")
                .addProperty(RDF.type, Datatourisme.PlaceOfInterest)
                .addProperty(DC.identifier, "test")
                .addProperty(RDFS.label, "POI");
        inferenceReasoner.process(model2);
        validityReport = validationReasoner.validate(resource);
        //model2.listStatements().forEachRemaining(System.out::println);
        assertIterCount(3, validityReport.getReports());
    }

    @Test
    public void testGeoLoc() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("urn:test")
            .addProperty(RDF.type, Datatourisme.PointOfInterest);
        ValidityReport validityReport = validationReasoner.validate(resource);
        assertIterCount(6, validityReport.getReports());

        Resource loc = model.createResource("urn:loc")
            .addProperty(RDF.type, Datatourisme.resource("Place"));
        resource.addProperty(Datatourisme.property("isLocatedAt"), loc);
        validityReport = validationReasoner.validate(resource);
        assertIterCount(7, validityReport.getReports());

        Resource geo = model.createResource("urn:geo");
        loc.addProperty(Schema.geo, geo);
        validityReport = validationReasoner.validate(resource);
        assertIterCount(8, validityReport.getReports());

        geo.addProperty(Schema.latitude, "48.1119800");
        validityReport = validationReasoner.validate(resource);
        assertIterCount(7, validityReport.getReports());

        geo.addProperty(Schema.longitude, "-1.6742900");
        validityReport = validationReasoner.validate(resource);
        assertIterCount(6, validityReport.getReports());
    }

    @Test
    public void testPeriod() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("urn:test")
                .addProperty(RDF.type, Datatourisme.EntertainmentAndEvent);
        ValidityReport validityReport = validationReasoner.validate(resource);
        assertIterCount(1, validityReport.getReports());

        Resource period = model.createResource("urn:period")
                .addProperty(RDF.type, Datatourisme.resource("Period"));
        resource.addProperty(Datatourisme.property("takesPlaceAt"), period);
        validityReport = validationReasoner.validate(resource);
        assertIterCount(1, validityReport.getReports());

        period.addProperty(Datatourisme.property("startDate"), "2001-01-01");
        validityReport = validationReasoner.validate(resource);
        assertIterCount(0, validityReport.getReports());
    }

    @Test
    public void testValidateEmail() {
        Model model = org.apache.jena.rdf.model.ModelFactory.createDefaultModel();

        // valid email
        Resource res = model.createResource("urn:test")
            .addProperty(Schema.email, "contact@test.com");
        ValidityReport validityReport = validationReasoner.validate(res);
        assertIterCount(0, validityReport.getReports());

        // valid email
        res = model.createResource("urn:test")
            .addProperty(Schema.email, "contact@104.fr");
        validityReport = validationReasoner.validate(res);
        assertIterCount(0, validityReport.getReports());

        // valid email
        res = model.createResource("urn:test")
            .addProperty(Schema.email, "H0683@accor.com");
        validityReport = validationReasoner.validate(res);
        assertIterCount(0, validityReport.getReports());

        // invalid email
        res = model.createResource("urn:test2")
            .addProperty(Schema.email, "test.com");
        validityReport = validationReasoner.validate(res);
        assertIterCount(1, validityReport.getReports());

        // invalid email
        res = model.createResource("urn:test2")
            .addProperty(Schema.email, "contact@test.something");
        validityReport = validationReasoner.validate(res);
        assertIterCount(1, validityReport.getReports());
    }

    @Test
    public void testAtLeastOneContact() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("urn:poi")
                .addProperty(RDF.type, Datatourisme.Product);
        Resource resource2 = model.createResource("urn:poi2")
                .addProperty(RDF.type, Datatourisme.Tour);
        Resource resource3 = model.createResource("urn:poi3")
                .addProperty(RDF.type, Datatourisme.resource("Accommodation"));
        Resource resource4 = model.createResource("urn:poi4")
                .addProperty(RDF.type, Datatourisme.resource("CamperVanArea"));

        inferenceReasoner.process(model);

        ValidityReport validityReport = validationReasoner.validate(resource);
        Assert.assertEquals(1, countReportByType("missing contact info", validityReport));
        validityReport = validationReasoner.validate(resource2);
//        model.listStatements().forEachRemaining(System.out::println);
        Assert.assertEquals(0, countReportByType("missing contact info", validityReport));
        validityReport = validationReasoner.validate(resource3);
        Assert.assertEquals(1, countReportByType("missing contact info", validityReport));
        validityReport = validationReasoner.validate(resource4);
        Assert.assertEquals(0, countReportByType("missing contact info", validityReport));
//
        resource.addProperty(Datatourisme.property("hasBookingContact"), model.createResource()
                .addProperty(Schema.telephone, "0255441144"));
        validityReport = validationReasoner.validate(resource);
//        validityReport.getReports().forEachRemaining(System.out::println);
//        model.listStatements().forEachRemaining(System.out::println);
        Assert.assertEquals(0, countReportByType("missing contact info", validityReport));
    }

    /**
     * @param type
     * @param report
     * @return
     */
    private int countReportByType(String type, ValidityReport report) {
        int i = 0;
        Iterator<ValidityReport.Report> iter = report.getReports();
        while(iter.hasNext()) {
            ValidityReport.Report r = iter.next();
            if(r.getType().equals("\"" + type + "\"")) {
                i++;
            }
        }
        return i;
    }
}
