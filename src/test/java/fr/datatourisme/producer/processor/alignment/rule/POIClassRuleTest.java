/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.commons.io.IOUtils;
import org.apache.jena.vocabulary.RDF;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * 
 */
public class POIClassRuleTest extends AbstractProducerTest {

    @Autowired
    Ontology ontologyAlignment;

    @Test
    public void evaluate() throws Exception {
        POIClassRule rule = new POIClassRule();
        rule.setAlignment(getAlignement());
        rule.setProperty(ontologyAlignment.getProperty(RDF.type));
        rule.setType(AbstractRule.Type.xpath);
        rule.setExpr("//elementReference/@id");

        XmlContext ctx = XmlContext.create(IOUtils.toByteArray(new FileInputStream(getXmlSource())));
        Set<String> set = rule.evaluate(ctx).collect(Collectors.toSet());

        assertEquals(3, set.size());
        assertTrue(set.stream().anyMatch(e -> Datatourisme.resource("Tour").hasURI(e)));
        assertTrue(set.stream().anyMatch(e -> Datatourisme.resource("EntertainmentAndEvent").hasURI(e)));
        assertTrue(set.stream().anyMatch(e -> Datatourisme.resource("Practice").hasURI(e)));

        rule.setExpr("//elementReference/@elementReferenceType");
        set = rule.evaluate(ctx).collect(Collectors.toSet());
        assertEquals(1, set.size());
    }
}