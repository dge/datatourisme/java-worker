/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * 
 */
public class DateTransformerTest {
    @Test
    public void transform() throws Exception {
        DateTransformer transformer = new DateTransformer();
        int year = Calendar.getInstance().get(Calendar.YEAR);

        assertEquals("2018-07-21", transformer.transform("21/07/2018"));
        assertEquals("2018-08-02", transformer.transform("02/08/2018"));
        assertEquals("2018-07-21", transformer.transform("21 juillet 2018"));
        assertEquals(year + "-07-21", transformer.transform("21 juillet"));
    }

}