/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.Schema;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static org.junit.Assert.assertEquals;

/**
 * 
 */
public class LiteralTransformerTest extends AbstractProducerTest {

    @Autowired
    @Qualifier("ontologyLite")
    Ontology ontology;

    @Test
    public void transform() throws Exception {
        // range : date
        OntologyProperty property = ontology.getProperty(Datatourisme.property("creationDate"));
        Literal l = LiteralTransformer.transform(property, "21 juillet 2017");
        assertEquals("2017-07-21", l.getString());

        // range : boolean
        property = ontology.getProperty(Datatourisme.property("reducedMobilityAccess"));
        l = LiteralTransformer.transform(property, "vrai");
        assertEquals(true, l.getBoolean());

        // range : positiveInteger
        property = ontology.getProperty(Datatourisme.property("requiredMaxPersonCount"));
        l = LiteralTransformer.transform(property, "14");
        assertEquals(14, l.getInt());

        // property : schema:telephone
        property = ontology.getProperty(Schema.telephone);
        l = LiteralTransformer.transform(property, "0299000000");
        assertEquals("+33 2 99 00 00 00", l.getString());

        // property : rdfs:label
        property = ontology.getProperty(RDFS.label);
        l = LiteralTransformer.transform(property, "Salut !");
        assertEquals("Salut !", l.getString());
        assertEquals("fr", l.getLanguage());
    }
}