/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * 
 */
public class ThesaurusRuleTest extends AbstractProducerTest {

    @Autowired
    Ontology ontologyAlignment;

    XmlContext ctx;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        ctx = XmlContext.create(IOUtils.toByteArray(new FileInputStream(getXmlSource())));
    }

    @Test
    public void evaluateIndividual() throws Exception {
        ThesaurusRule rule = new ThesaurusRule();
        rule.setAlignment(getAlignement());
        rule.setProperty(ontologyAlignment.getProperty(Datatourisme.property("hasTheme")));
        rule.setType(AbstractRule.Type.xpath);
        rule.setExpr("//elementReference/@id");

        Set<String> set = rule.evaluate(ctx).collect(Collectors.toSet());

        assertEquals(2, set.size());
        assertTrue(set.stream().anyMatch(e -> DatatourismeThesaurus.resource("AfricanCuisine").hasURI(e)));
        assertTrue(set.stream().anyMatch(e -> DatatourismeThesaurus.resource("AsianCuisine").hasURI(e)));

        rule.setExpr("//elementReference/@elementReferenceType");
        set = rule.evaluate(ctx).collect(Collectors.toSet());
        assertEquals(0, set.size());
    }

    @Test
    public void evaluateFunctionalIndividual() throws Exception {
        Alignment alignment = new Alignment(ontologyAlignment, getThesaurusAlignment(), null);
        OntologyProperty property = ontologyAlignment.getProperty(Datatourisme.property("hasEligiblePolicy"));
        assertTrue(property.isFunctional());

        ThesaurusRule rule = new ThesaurusRule();
        rule.setAlignment(alignment);
        rule.setProperty(property);
        rule.setType(AbstractRule.Type.xpath);
        rule.setExpr("//elementReference/@id");

        Set<String> set = rule.evaluate(ctx).collect(Collectors.toSet());
        assertEquals(1, set.size());
        assertTrue(set.stream().anyMatch(e -> DatatourismeThesaurus.resource("ReducedRate").hasURI(e)));
    }
}