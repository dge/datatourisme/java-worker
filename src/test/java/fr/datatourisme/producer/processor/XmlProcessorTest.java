/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor;

import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.preview.ResourceReportPreview;
import fr.datatourisme.producer.processor.report.ResourceReport;
import fr.datatourisme.producer.processor.xml.XmlProcessor;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class XmlProcessorTest extends AbstractProducerTest {

    @Autowired
    XmlProcessor xmlProcessor;

    @Test
    public void testProcess() throws Exception {

        Model model = ModelFactory.createDefaultModel();
        Alignment alignment = getAlignement();

        long startTime = System.nanoTime();
        XmlItemDao dao = XmlItemDao.instanciate(getDb());
        dao.findAll().forEachRemaining(i -> {
            try {
                ResourceReport report = xmlProcessor.process(alignment, i.getBytes());
                model.add(report.getResource().getModel());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

//        ResourceValidityReport report = processor.process(dao.findById("sitraLOI303084").getBytes());
//        model.add(report.getResource().getModel());
//        report.getReports().forEachRemaining(r -> System.out.println(r));
//        System.exit(1);

        long endTime = System.nanoTime();
        model.write(System.out, "TTL") ;
        System.out.println((endTime - startTime) / 1000000 );;
    }

    @Test
    public void testPreview() throws Exception {
        Alignment alignment = getAlignement();

        XmlItemDao dao = XmlItemDao.instanciate(getDb());
        ResourceReport report = xmlProcessor.process(alignment, dao.findById("sitraLOI303084").getBytes());
        ResourceReportPreview preview = new ResourceReportPreview(report, alignment);

        System.out.println(preview.toJson(true));
    }
}
