/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.expand;

import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import fr.datatourisme.producer.processor.atomic.operation.reduce.Index;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SplitTest {
    @Test
    public void process() throws Exception {
        List<String> test = new ArrayList<>();
        test.add("test1#test2");
        test.add("test3# test4");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
            new Split(new Object[]{"#"})
        }).process(test);

        Assert.assertArrayEquals(new String[] {"test1", "test2", "test3", "test4"}, results.toArray());
    }

    @Test
    public void bugCRTNA() throws Exception {
        List<String> test = new ArrayList<>();
        test.add("http://www.chateau-lalande-stjean.blogspot.com|https://www.gites-de-france-gironde.com/Gite-location-Saint-loubes-Manaud-Pres-bordeaux-33G3041.html");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
            new Split(new Object[]{"#"})
        }).process(test);

        Assert.assertArrayEquals(new String[] {"http://www.chateau-lalande-stjean.blogspot.com|https://www.gites-de-france-gironde.com/Gite-location-Saint-loubes-Manaud-Pres-bordeaux-33G3041.html"}, results.toArray());
    }

    @Test
    public void testNotRemoveEmpty() {
        List<String> test = new ArrayList<>();
        test.add("|67 rue Nationale |||41400|MONTRICHARD VAL DE CHER|41151");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
                new Split(new Object[]{"|"}), new Index(new Object[] {6})
        }).process(test);

        Assert.assertArrayEquals(new String[] {"41151"}, results.toArray());
    }
}