/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic;

import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import fr.datatourisme.producer.processor.atomic.operation.expand.Split;
import fr.datatourisme.producer.processor.atomic.operation.reduce.Count;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AtomicChainTest {
    @Test
    public void testNull() {
        List<String> test = new ArrayList<>();
        test.add("test1");
        test.add(null);
        test.add("test3");

        List<String> results = AtomicChain.create(new AbstractOperation[] {}).process(test);

        Assert.assertArrayEquals(new String[] {"test1", "test3"}, results.toArray());
    }

    @Test
    public void testFromJson() {
        String json = "[\n" +
        "    {\"split\": [\"#\"]},\n" +
        "    {\"count\": []}\n" +
        "]";

        AtomicChain chain = AtomicChain.fromJson(json);
        List<AbstractOperation> ops = chain.getOperations();
        Assert.assertTrue(ops.get(0) instanceof Split);
        Assert.assertTrue(ops.get(1) instanceof Count);
    }
}
