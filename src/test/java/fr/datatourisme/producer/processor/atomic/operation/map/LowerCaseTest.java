/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class LowerCaseTest {
    @Test
    public void process() throws Exception {
        List<String> test = new ArrayList<>();
        test.add("TEST1");
        test.add("test2");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
            new LowerCase()
        }).process(test);

        Assert.assertArrayEquals(new String[] {"test1", "test2"}, results.toArray());
    }

}