/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import org.apache.jena.ext.com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MapTest {
    @Test
    public void process() throws Exception {

        List<String> test = new ArrayList<>();
        test.add("test1");
        test.add("test2");
        test.add("test3");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
            new Map(new Object[]{
                ImmutableMap.of("test1", "foo1", "test3", "foo3")
            })
        }).process(test);
        Assert.assertArrayEquals(new String[] {"foo1", "foo3"}, results.toArray());

        results = AtomicChain.create(new AbstractOperation[] {
                new Map(new Object[]{
                    ImmutableMap.of("test1", "foo1", "test3", "foo3"),
                    true
                })
        }).process(test);
        Assert.assertArrayEquals(new String[] {"foo1", "test2", "foo3"}, results.toArray());
    }

}