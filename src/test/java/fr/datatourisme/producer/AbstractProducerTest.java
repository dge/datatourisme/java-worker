/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer;

import fr.datatourisme.AbstractTest;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;

abstract public class AbstractProducerTest extends AbstractTest {

    @Autowired
    private ThesaurusAlignmentFactory thesaurusAlignmentFactory;

    @Autowired
    private AlignmentFactory alignmentFactory;

    private String fixturesDir = null;

    public void setFixturesDir(String fixturesDir) {
        this.fixturesDir = fixturesDir;
    }

    protected File getFixture(String name) {
        URL url = this.getClass().getClassLoader().getResource("fixtures/producer" + (fixturesDir != null ? "/" + fixturesDir : "") + "/" + name);
        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            return new File(url.getPath());
        }
    }

    public File getDb() {
        return getFixture("poi.db");
    }

    public File getXmlSource() {
        return getFixture("datasource.xml");
    }

    public File getXmlSource(String lang) {
        return getFixture("datasource." + lang + ".xml");
    }

    public ThesaurusAlignment getThesaurusAlignment() {
        try {
            File file = getFixture("thesaurus.json");
            return thesaurusAlignmentFactory.createFromJson(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Alignment getAlignement() {
        try {
            File file = getFixture("alignment.json");
            return alignmentFactory.createFromJson(getThesaurusAlignment(), file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
