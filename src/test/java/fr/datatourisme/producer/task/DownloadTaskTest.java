/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class DownloadTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        ReportEntry reportEntry = getDownloadTask().run();
        System.out.println(reportEntry.toJsonTree().getAsJsonObject().get("duration"));
        Assert.assertTrue(storage.dir("source").file(getXmlSource().getName()).exists());
    }

    @Test
    public void testDownload() throws Exception {
        URL zipFile = this.getClass().getClassLoader().getResource("zip/data source.zip");
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl(zipFile);
        task.run();
        Assert.assertTrue(storage.dir("source").file("datasource.xml").exists());
    }

    @Test(expected=JobException.class)
    public void test404Execute() throws Throwable {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("http://www.google.com/test.xml");
        task.run();
    }

    @Test(expected=MalformedURLException.class)
    public void testMalformedExecute() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("htp://www.google.com/test.xml");
        task.run();
    }

    @Ignore
    @Test(expected=TemporaryJobException.class)
    public void testTimeoutExecute() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("http://10.255.255.1");
        task.run();
    }

    @Test
    public void testMimeTypeGuesser() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("https://www.sitlor.fr/xml/exploitation/listeproduits.asp?rfrom=1&rto=20&user=2000675&pwkey=82ff62a5e0147491c797f1b34dab7d03&urlnames=tous&PVALUES=4000045,845,@,+7J&PNAMES=eltypo,lentidad,horariodu,horarioau&georeftype=2&clause=2000675000001");
        task.run();
        Assert.assertTrue(storage.dir("source").file("listeproduits.xml").exists());
    }

    @Test
    @Ignore
    public void testAtomMimeTypeGuesser() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("https://wcf.tourinsoft.com/Syndication/cdt31/7f05d541-ba8a-4d82-ae55-1b48623d9f2f/Objects");
        task.run();
        Assert.assertTrue(storage.dir("source").file("Objects.xml").exists());
    }

    @Test
    public void test415() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("https://wcf.tourinsoft.com/Syndication/centre/0622a872-45ba-4751-abde-71ab070f7b0b/Objects");
        task.run();
        Assert.assertTrue(storage.dir("source").file("Objects.xml").exists());
    }

    @Test
    public void testFluentFallback() throws Exception {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl("https://normandie.tourinsoft.com/soft/RechercheDynamique/Syndication/controle/syndication2.asmx/getDetailBordereau?idModule=d116dcbf-13f4-4bfa-adf6-07c4262d0ec1&OBJETTOUR_CODE=FMA");
        task.run();
        Assert.assertTrue(storage.dir("source").file("getDetailBordereau.xml").exists());
    }

    @Test
    public void testI18nUrls() throws Exception {
        setFixturesDir("alsace");
        getDownloadTask().run();
        Assert.assertTrue(storage.dir("source").file("datasource.xml").exists());
        Assert.assertTrue(storage.dir("source").dir("en").file("datasource.en.xml").exists());
    }
}
