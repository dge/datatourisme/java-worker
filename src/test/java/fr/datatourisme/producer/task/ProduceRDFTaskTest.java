/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.junit.Test;

public class ProduceRDFTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        getDownloadTask().run();
        getProcessXmlTask().run();
        ReportEntry reportEntry = getProduceRDFTask().run();
        System.out.println(reportEntry.toJson(true));
        System.out.println(reportEntry.toJsonTree().getAsJsonObject().get("duration"));
    }

    @Test
    public void testExecuteAlsace() throws Exception {
        setFixturesDir("alsace");
        getDownloadTask().run();
        getProcessXmlTask().run();
        ReportEntry reportEntry = getProduceRDFTask().run();
        System.out.println(reportEntry.toJson(true));
    }
}
