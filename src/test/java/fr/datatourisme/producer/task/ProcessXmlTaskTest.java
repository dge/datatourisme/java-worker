/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.producer.xml.datasource.XmlItemIndexerTest;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

public class ProcessXmlTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        getDownloadTask().run();
        ReportEntry reportEntry = getProcessXmlTask().run();
        System.out.println(reportEntry.toJsonTree().getAsJsonObject().get("duration"));

        Storage outputDir = storage.dir("xpath-schemas");

        Assertions.assertThat(getDb()).exists();
        XmlItemDao dao = XmlItemDao.instanciate(getDb());
        XmlItemIndexerTest.assertIterCount(50, dao.findAll());
        XmlItemIndexerTest.assertIterCount(50, dao.findAllByAttr("type", "PointOfInterest"));
//        XmlItemIndexerTest.assertIterCount(28, dao.findAllByAttr("type", "PlaceOfInterest"));

        Assertions.assertThat(outputDir.file("PointOfInterest.json")).exists();
        Assertions.assertThat(outputDir.file("CulturalSite.json")).exists();
        Assertions.assertThat(outputDir.file("FoodEstablishment.json")).exists();
        Assertions.assertThat(outputDir.file("NaturalHeritage.json")).exists();
        Assertions.assertThat(outputDir.file("PlaceOfInterest.json")).exists();
        Assertions.assertThat(outputDir.file("Restaurant.json")).exists();
    }

    @Test
    public void testExecuteAlsace() throws Exception {
        setFixturesDir("alsace");
        getDownloadTask().run();

        getProcessXmlTask().run();
        Assert.assertTrue(storage.file("poi.db").exists());
        Assert.assertTrue(storage.file("poi.en.db").exists());
    }
}
