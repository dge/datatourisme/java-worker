/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.producer.AbstractProducerTest;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.junit.Before;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@ActiveProfiles("worker")
abstract public class AbstractTaskTest extends AbstractProducerTest {
    protected File db;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        db = storage.file("poi.db");
    }

    public DownloadTask getDownloadTask() throws MalformedURLException {
        DownloadTask task = createTask(DownloadTask.class);
        task.setUrl(getXmlSource().toURI().toString());

        // find linguistic feeds
        FileFilter fileFilter = new WildcardFileFilter("datasource.*.xml");
        File[] files = getXmlSource().getParentFile().listFiles(fileFilter);
        if(files.length > 0) {
            Map<String, String> i18nUrls = new HashMap<>();
            for(File langFile: files) {
                String lang = langFile.getName().split("\\.")[1];
                i18nUrls.put(lang, langFile.toURI().toString());
            }
            task.setI18nUrls(i18nUrls);
        }

        return task;
    }

    public PreprocessXmlTask getPreprocessXmlTask() {
        return createTask(PreprocessXmlTask.class);
    }

    public ProcessXmlTask getProcessXmlTask() {
        ProcessXmlTask task = createTask(ProcessXmlTask.class);
        task.setDb(db);
        task.setThesaurusAlignment(getThesaurusAlignment());
        task.setAlignment(getAlignement());
        return task;
    }

    public ProduceRDFTask getProduceRDFTask() {
        ProduceRDFTask task = createTask(ProduceRDFTask.class);
        task.setThesaurusAlignment(getThesaurusAlignment());
        task.setAlignment(getAlignement());
        task.setDb(db);
        task.setChecksum(DigestUtils.md5Hex(String.valueOf(Math.random())));
        task.setFluxIdentifier(1);
        task.setOrganizationIdentifier(1);
        return task;
    }

    public PersistRDFTask getPersistRDFTask() {
        PersistRDFTask task = createTask(PersistRDFTask.class);
        task.setFluxIdentifier(1);
        task.setDb(db);
        return task;
    }
}
