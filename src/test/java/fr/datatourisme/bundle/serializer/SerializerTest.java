/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.serializer;

import com.conjecto.graphstore.GraphLoader;
import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.github.jsonldjava.utils.JsonUtils;
import fr.datatourisme.AbstractTest;
import fr.datatourisme.bundle.ResourceStoreBuilder;
import fr.datatourisme.ontology.Ontology;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.RIOT;
import org.apache.jena.shared.impl.JenaParameters;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExternalResource;
import org.junit.rules.RuleChain;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;
import org.semanticweb.yars.nx.Resource;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SerializerTest extends AbstractTest {

    private GraphStore store;
    private Context context;

    @Autowired
    Ontology ontologyLite;

    /**
     * TemporaryFolder : rule is done as RuleChain
     */
    public TemporaryFolder temp = new TemporaryFolder();

    /**
     * ExternalResource : rule is done as RuleChain
     */
    public final ExternalResource resource = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            // prepare graphstore
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("fixtures/bundle/sample.nt");
            GraphStoreOptions options = (new GraphStoreOptions())
                .setDisablePOSIndex(true)
                .setCreateIfMissing(true);

            store = GraphStore.open(temp.getRoot().getAbsolutePath(), options);
            GraphLoader loader = new GraphLoader(store, "nt");
            loader.load(inputStream);
            store.compact();

            // prepare context
            context = ResourceStoreBuilder.buildContext(ontologyLite);
        };

        @Override
        protected void after() {
            store.close();
        };
    };

    @Rule
    public TestRule ruleChain = RuleChain.outerRule(temp).around(resource);

    @BeforeClass
    public static void beforeClass() {
        // force literal validation
        JenaParameters.enableEagerLiteralValidation = true;
    }

    @Test
    public void testJsonSerialize() throws IOException {
        // serialize to json
        JsonLdSerializer jsonSerializer = new JsonLdSerializer(store, context);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        jsonSerializer.serialize(new Resource(getUri()), bos);
        bos.close();

        System.out.println(bos.toString());

        // set context
        org.apache.jena.sparql.util.Context ctx = new org.apache.jena.sparql.util.Context();
        Object jsonLdContextAsObject = JsonUtils.fromString(context.toJson());
        ctx.set(RIOT.JSONLD_CONTEXT, jsonLdContextAsObject);

        // parse to model
        Model model = ModelFactory.createDefaultModel();
        InputStream in = new ByteArrayInputStream(bos.toByteArray());
        RDFParser.source(in).lang(Lang.JSONLD).context(ctx).parse(model.getGraph());

        // assertions
        assertModelValidity(model);
    }

    @Test
    public void testXmlSerialize() throws IOException {
        // serialize to json
        XmlSerializer xmlSerializer = new XmlSerializer(store, context);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        xmlSerializer.serialize(new Resource(getUri()), bos);
        bos.close();

        System.out.println(bos.toString());

        // parse to model
        Model model = ModelFactory.createDefaultModel();
        InputStream in = new ByteArrayInputStream(bos.toByteArray());
        RDFDataMgr.read(model, in, Lang.RDFXML) ;

        // assertions
        assertModelValidity(model);
    }

    /**
     * @return
     */
    private String getUri() {
        return "https://data.datatourisme.gouv.fr/13/c8f96a13-4c1a-33eb-a734-d0e8223dbaa8";
    }

    /**
     * @param model
     */
    private void assertModelValidity(Model model) {
        org.apache.jena.rdf.model.Resource res = model.getResource(getUri());
        // literal
        Assert.assertTrue(res.getProperty(RDFS.comment).getObject() instanceof Literal);
        // size
        Assert.assertEquals(20, res.listProperties().toList().size());
    }
}