#!/bin/bash

#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier:  GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

: ${JAVA_WORKER_PATH?"Need to set JAVA_WORKER_PATH"}

# Disable exit on non 0
set +e

# stop workers
supervisorctl stop all

# go to java worker path
cd $JAVA_WORKER_PATH

# build graphstore
java -jar java-worker.jar build-graphstore || error=true

# build bundle store
if [ ! $error ]
then
  java -jar java-worker.jar build-bundle-resource-store
fi

# restart workers
supervisorctl start all
