#!/bin/bash

#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier:  GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

# check variables
: ${DATATOURISME_DATA_PATH?"Need to set DATATOURISME_DATA_PATH"}
: ${DATATOURISME_WEBAPP_PRODUCER_PATH?"Need to set DATATOURISME_WEBAPP_PRODUCER_PATH"}
: ${DATATOURISME_WEBAPP_CONSUMER_PATH?"Need to set DATATOURISME_WEBAPP_CONSUMER_PATH"}

# see https://www.artificialworlds.net/blog/2012/10/17/bash-associative-array-examples/
declare -A REPLACE
REPLACE["http://www.datatourisme.fr/ontology/core/1.0#"]="https://www.datatourisme.gouv.fr/ontology/core#"
REPLACE["http://www.datatourisme.fr/resource/core/1.0#"]="https://www.datatourisme.gouv.fr/resource/core#"
REPLACE["http://www.datatourisme.fr/ontology/alignment/1.0#"]="https://www.datatourisme.gouv.fr/ontology/alignment#"
REPLACE["http://www.datatourisme.fr/resource/metadata/1.0#"]="https://www.datatourisme.gouv.fr/resource/metadata#"

function escape {
    echo ${1//\//\\/} | sed -e 's/\\/\\\\/g; s/\//\\\//g; s/&/\\\&/g'
}

now=$(date +"%Y%m%d")

for filename in ${DATATOURISME_DATA_PATH}/producer/*/alignment.json ${DATATOURISME_DATA_PATH}/producer/*/thesaurus.json
do
    echo ${filename}

    if [ ! -f ${filename}.${now}.bak ]; then
        cp ${filename} ${filename}.${now}.bak
    else
        cp ${filename}.${now}.bak ${filename}
    fi

    for k in "${!REPLACE[@]}"; do
        sed -i "s,\"$(escape ${k}),\"$(escape ${REPLACE[$k]}),g" "$filename"
    done
done

# webapp producer / consumer
for k in "${!REPLACE[@]}"; do
    php ${DATATOURISME_WEBAPP_PRODUCER_PATH}/bin/console datatourisme:update-ontology-uri ${k} ${REPLACE[$k]}
    php ${DATATOURISME_WEBAPP_CONSUMER_PATH}/bin/console datatourisme:update-ontology-uri ${k} ${REPLACE[$k]}
done

php ${DATATOURISME_WEBAPP_PRODUCER_PATH}/bin/console datatourisme:thesaurus:substitute "https://www.datatourisme.gouv.fr/ontology/core#Meeting" "https://www.datatourisme.gouv.fr/ontology/core#BusinessEvent"
php ${DATATOURISME_WEBAPP_PRODUCER_PATH}/bin/console datatourisme:thesaurus:substitute "https://www.datatourisme.gouv.fr/ontology/core#MusicalEvent" "https://www.datatourisme.gouv.fr/ontology/core#CulturalEvent"
